class Membership < ApplicationRecord
  belongs_to :user
  belongs_to :learning_management_system
end
