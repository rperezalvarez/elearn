class Goal < ApplicationRecord

  belongs_to :enrollment
  has_many :goal_details, dependent: :destroy
  has_one :goal_kpi_group, dependent: :destroy

  accepts_nested_attributes_for :goal_kpi_group

  validates_numericality_of :goal_hours
  validates_numericality_of :goal_evaluations
  validates_numericality_of :goal_videos

  attr_accessor :details
  attr_accessor :working_days

  include PerformanceCalculations::ResourcesActivities

  def build_goal_details
    self.details = []
    kind_of_goal = GoalDetail::KINDS.keys[0]
    7.times do |index|
      new_date = self.start + index.days
      details.push(GoalDetail.new(kind:kind_of_goal.to_s,schedule_date:new_date))
    end

  end

  def create_goal_details(picked_dates)
    kind_of_goal = GoalDetail::KINDS.keys[0]

    7.times do |index|
      new_date = self.start + index.day
      picked = picked_dates.include?(new_date.strftime("%Y-%m-%d"))
      
      GoalDetail.create(
                    goal_id:self.id,
                    kind:kind_of_goal.to_s,
                    schedule_date:new_date,
                    picked:picked
                    )
    end
  end

  def create_kpi_group
    enrollment = self.enrollment
    week = self.week

    avg_completers_kpis = enrollment.avg_completers(week-1)

    GoalKpiGroup.create(
      goal: self,

      videos_last_week: enrollment.videos_watched(week),
      evals_last_week: enrollment.exams_done(week),
      
      most_effective_day: enrollment.most_effective_day,
      
      avg_weekly_videos: enrollment.avg_videos_watched.round(1),
      avg_weekly_exams: enrollment.avg_exams_done.round(1),
      avg_weekly_time: enrollment.avg_time_invested_per_week.round(1),
      avg_weekly_time_videos: enrollment.avg_time_invested_per_week(Resource::CATEGORIES[:lectures]).round(1),
      avg_weekly_time_exams: enrollment.avg_time_invested_per_week(Resource::CATEGORIES[:evaluations]).round(1),
      
      total_videos_this_week: enrollment.edition.course.resources.where(category: Resource::CATEGORIES[:lectures], module: week-1).distinct.count,
      total_evals_this_week: enrollment.edition.course.resources.where(category: Resource::CATEGORIES[:evaluations], module: week-1).distinct.count,
      total_time_videos_week: enrollment.edition.course.resources.where(category: Resource::CATEGORIES[:lectures], module: week-1).distinct.sum(:duration),
      total_time_evals_week: enrollment.edition.course.resources.where(category: Resource::CATEGORIES[:evaluations], module: week-1).distinct.sum(:duration),
      
      avg_videos_completers: avg_completers_kpis[0].round(1),
      avg_evals_completers: avg_completers_kpis[1].round(1),
      avg_time_completers: avg_completers_kpis[2].round(1)
    )
  end

  def self.create_or_initialize_by_params(user, enrollment,week)
    start_goal_date = enrollment.date_enrolled.beginning_of_day + (week-1).week
    end_goal_date = start_goal_date + 6.day
    goal = Goal.find_or_initialize_by(
      user_id: user.id,
      enrollment_id: enrollment.id,
      week: week,
      start: start_goal_date,
      finish:end_goal_date
    )

    goal
  end

  def self.create_by_params(params)
    enrollment = Enrollment.find(params[:enrollment_id])

    start_date = enrollment.date_enrolled.beginning_of_day + (params[:week].to_i - 1).week
    end_date = start_date + 6.days
    @goal = Goal.new(
      user_id:params[:user_id],
      start:start_date,
      finish:end_date,
      goal_hours:params[:goal_hours],
      goal_videos:params[:goal_videos],
      goal_evaluations:params[:goal_evaluations],
      week:params[:week],
      enrollment_id:enrollment.id
    )

    result = @goal.save
    @goal.create_goal_details(params[:working_days].keys) if result
    @goal.create_kpi_group if result

    @goal
  end

end
