class LearningManagementSystem < ApplicationRecord
  has_many :memberships, dependent: :destroy

  has_many :courses, dependent: :destroy
end
