class Note < ApplicationRecord
  before_save :set_title

  paginates_per 5

  belongs_to :user
  belongs_to :course, optional: true


  before_create :generate_token

  scope :from_user, -> (user_id) { where(user_id: user_id) }

  def self.search(search)
    if search
      where('title like ? OR body like ?', "%#{search}%", "%#{search}%")
    else
      all
    end
  end

  protected

  def set_title
    self.title = "No title" if self.title.blank?
  end

  def generate_token
    self.code = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless Note.exists?(code: random_token)
    end
  end
end
