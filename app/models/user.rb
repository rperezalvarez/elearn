class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validate :check_agreement_accepted, on: :create

  has_many :sessions, dependent: :destroy
  has_many :notes, dependent: :destroy
  has_many :memberships, dependent: :destroy
  has_many :enrollments, dependent: :destroy

  # Analytics from user
  has_many :visits
  

  GENDER_TYPES = {male: 'male', female: 'female'}
  EDUCATION_LEVELS = {
    primary: 'primary',
    secondary: 'secondary',
    bachelor: 'bachelor',
    master: 'master',
    doctoral: 'doctoral'
  }

  def study_frequency(course)
    user_enrollment = Enrollment.from_user(self).from_course(course).last
    user_sessions = Session.from_user(self).from_course(course).where("? <= sessions.start", user_enrollment.date_enrolled).order(start: :asc)
    sessions_limits = user_sessions.where("sessions.duration IS NOT NULL").distinct.map{|s| [s.start.to_time.to_i, s.start.to_time.to_i + s.duration] }
    if user_sessions.last.duration.nil?
      sessions_limits << [user_sessions.last.start.to_time.to_i, user_sessions.last.start.to_time.to_i]
    end

    user_study_frequency_sum = 0

    sessions_limits.each.with_index do |s,i|
      if(i < sessions_limits.length - 1)
        user_study_frequency_sum += sessions_limits[i+1][0] - s[1]
      end
    end

    sessions_limits.length > 1 ? user_study_frequency_sum/(sessions_limits.length-1) : 0
  end

  def courses
    Course.joins(editions: :enrollments).where(enrollments:{user_id:self.id}).group('courses.id').order('courses.full_name')
  end

  def close_active_study_sessions
    active_sessions = Session.from_user(self).where(duration: nil)
    active_sessions.each do |session|
      session.close
    end
  end   




  private

    def check_agreement_accepted
      errors.add(:base, 'You must accept the agreement to be able to use My Note Progress') unless self.agreement_accepted
    end

end
