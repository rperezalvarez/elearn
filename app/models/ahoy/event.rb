module Ahoy
  class Event < ActiveRecord::Base
    include Ahoy::Properties

    self.table_name = "ahoy_events"

    belongs_to :visit
    belongs_to :user, optional: true

    def self.to_csv
      require 'csv'
      
      Dir.mkdir("analytics") unless File.exists?("analytics")

      CSV.open("analytics/analytics-#{Rails.env}-#{DateTime.now}.csv", 'w') do |csv|
        csv << Ahoy::Event.column_names
        Ahoy::Event.all.each do |e|
          csv << e.attributes.values
        end
      end
      
    end

  end
end
