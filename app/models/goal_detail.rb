class GoalDetail < ApplicationRecord
  belongs_to :goal

  KINDS = {schedule_day: "Agendar un día"}

  def title
    date = self.schedule_date
    title = nil
    if date.present?
      week_day = date.wday # Returns the day of week (0-6, Sunday is zero)
      day = I18n.t('date.day_names')[week_day]
    end

    title = "#{day.capitalize} (#{date.strftime("%d-%m-%Y")})"
  end

  def formated_schedule_date
    self.schedule_date.strftime("%Y-%m-%d")
  end
end
