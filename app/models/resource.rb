class Resource < ApplicationRecord
  # Relations
  belongs_to :course, optional: true

  has_many :session_resources, dependent: :destroy
  has_many :sessions, through: :session_resources
  has_many :resource_statuses, dependent: :destroy

  # Scopes
  scope :from_course, -> (id) { where(course_id: id) }
  scope :of_type, -> (type) { where(resource_type: type) }
  scope :of_category, -> (category) { where(category: category) }
  scope :from_course_or_of_category, -> (id, category) { where(course_id: id).or(where(category: category)) }

  CATEGORIES = {
    complements:  'complements',
    lectures:     'lectures',
    evaluations:  'evaluations',
    discussions:  'discussions',
    others:       'others'
  }

  RESOURCE_TYPES = {
    complements: {
      info:           'info',
      supplement:     'supplement',
      help:           'help'
    },
    lectures: {
      lecture:        'lecture'
    },
    evaluations: {
      peer:           'peer',
      feedback:       'feedback',
      exam:           'exam',
      programming:    'programming'
    },
    discussions: {
      discussions:    'discussions'
    },
    others: {
      procrastination:'procrastination'
    }
  }

  MATCH_RESOURCE_TYPE = {
    'info'                => RESOURCE_TYPES[:complements][:info],
    'infos'               => RESOURCE_TYPES[:complements][:info],
    'supplement'          => RESOURCE_TYPES[:complements][:supplement],
    'supplements'         => RESOURCE_TYPES[:complements][:supplement],
    'help'                => RESOURCE_TYPES[:complements][:help],
    'helps'               => RESOURCE_TYPES[:complements][:help],

    'lecture'             => RESOURCE_TYPES[:lectures][:lecture],
    'lectures'            => RESOURCE_TYPES[:lectures][:lecture],
    
    'peer'                => RESOURCE_TYPES[:evaluations][:peer],
    'peers'               => RESOURCE_TYPES[:evaluations][:peer],
    'phased peer'         => RESOURCE_TYPES[:evaluations][:peer],
    'phased peers'        => RESOURCE_TYPES[:evaluations][:peer],
    'peer review'         => RESOURCE_TYPES[:evaluations][:peer],
    'peer reviews'        => RESOURCE_TYPES[:evaluations][:peer],
    'feedback'            => RESOURCE_TYPES[:evaluations][:feedback],
    'feedbacks'           => RESOURCE_TYPES[:evaluations][:feedback],
    'review'              => RESOURCE_TYPES[:evaluations][:feedback],
    'reviews'             => RESOURCE_TYPES[:evaluations][:feedback],
    'feedback and review' => RESOURCE_TYPES[:evaluations][:feedback],
    'feedback and reviews'=> RESOURCE_TYPES[:evaluations][:feedback],
    'exam'                => RESOURCE_TYPES[:evaluations][:exam],
    'exams'               => RESOURCE_TYPES[:evaluations][:exam],
    'quiz'                => RESOURCE_TYPES[:evaluations][:exam],

    'discussion'          => RESOURCE_TYPES[:discussions][:discussions],
    'discussions'         => RESOURCE_TYPES[:discussions][:discussions],
    'forum'               => RESOURCE_TYPES[:discussions][:discussions],
    'forums'              => RESOURCE_TYPES[:discussions][:discussions],

    'procrastination'     => RESOURCE_TYPES[:others][:procrastination]
  }

  # Inverse of RESOURCE_TYPES
  def self.CATEGORY_OF_RESOURCE_TYPE(resource_type)
    resource_type = MATCH_RESOURCE_TYPE[resource_type]
    Resource::RESOURCE_TYPES.select{|key, hash| hash.values.include?(resource_type.downcase) }.keys.first.to_s
  end

end
