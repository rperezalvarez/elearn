class AppConfiguration < ApplicationRecord
  validate :there_can_only_be_one, :on => :create

  def self.min_session_effective_time_for_save
    AppConfiguration.create! if AppConfiguration.all.empty?
    AppConfiguration.first.min_session_effective_time_for_save
  end

  def self.min_session_effective_time_for_save=(new_value)
    AppConfiguration.create! if AppConfiguration.all.empty?
    c = AppConfiguration.first
    c.min_session_effective_time_for_save = new_value
    c.save!
  end

  def self.max_inactive_time
    AppConfiguration.create! if AppConfiguration.all.empty?
    AppConfiguration.first.max_inactive_time
  end

  def self.max_inactive_time=(new_value)
    AppConfiguration.create! if AppConfiguration.all.empty?
    c = AppConfiguration.first
    c.max_inactive_time = new_value
    c.save!
  end

  private

    def there_can_only_be_one
      errors.add(:base, 'There can only be one configuration') if AppConfiguration.count > 0
    end

end
