class Enrollment < ApplicationRecord
  after_create :create_enrollment_resources_statuses, :create_membership
  before_create :check_valid_enrolled_date

  belongs_to :user
  belongs_to :edition

  has_many :goals, dependent: :destroy
  has_many :resource_statuses, dependent: :destroy

  scope :from_user, -> (id) { where(user_id: id) }
  scope :from_course, -> (id) { joins(edition: :course).where("courses.id = ?", id) }

  def create_enrollment_resources_statuses
    self.edition.course.resources.each do |resource|
      ResourceStatus.create!(enrollment: self, resource: resource)
    end
  end

  def create_membership
    Membership.find_or_create_by(user: self.user, learning_management_system: self.edition.course.learning_management_system)
  end

  def day_of_course(date = nil)
    # user_enrollment = Enrollment.from_user(self).from_course(course).last
    date_start = [self.date_enrolled, self.edition.start.to_date].max

    if date.nil?
      date = Date.today
    end

    day = (date - date_start).to_i + 1
    #if day < course.duration
    return day
  end

  def current_week
    (self.day_of_course - 1)/7 + 1
  end


  def time_invested(week=nil, category=nil)
    if week.present?
      if 0 < week
        start_date = self.date_enrolled.to_datetime + (week-1).weeks
        end_date = start_date + 1.week

      else
        0.0
      end
    else
      start_date = self.date_enrolled.to_datetime
      end_date = DateTime.now
    end

    user_sessions = Session.from_user(self.user).from_course(self.edition.course).where(start: start_date..end_date)
    time_invested = SessionResource.effective.where(session: user_sessions)

    if category.present?
      time_invested = time_invested.joins(:resource).where("resources.category = ?", category)
    end

    time_invested.distinct.sum(:active_time)
  end

  
  def avg_videos_watched
    weeks_in_course = (Date.today - self.date_enrolled).to_i/7 + 1
    videos_watched = ResourceStatus.where(enrollment: self, resource: Resource.where(category: Resource::CATEGORIES[:lectures]), status: ResourceStatus::STATUS[:completed]).count/weeks_in_course.to_f
  end
  
  def avg_exams_done
    weeks_in_course = (Date.today - self.date_enrolled).to_i/7 + 1
    exams_done = ResourceStatus.where(enrollment: self, resource: Resource.where(category: Resource::CATEGORIES[:evaluations]), status: ResourceStatus::STATUS[:completed]).count/weeks_in_course.to_f
  end

  def videos_watched(week)
    resources_done(Resource::CATEGORIES[:lectures], week, self.edition.course)
  end
  
  def exams_done(week)
    resources_done(Resource::CATEGORIES[:evaluations], week, self.edition.course)
  end
  
  def resources_to_watch(category, week, course)
    period = self.edition.week_period(week)
    completed = ResourceStatus.joins(:enrollment).joins(:resource).where(status: "Not started", date_completed: period[:start_date]..period[:finish_date]).where(enrollments: {user_id: self.user.id, id: self.id}).where(resources: {category: category})
    completed.length
  end  
  
  def avg_time_invested_per_week(category=nil)
    time_invested = self.time_invested(nil, category)
    weeks_on_course = (Date.today - self.date_enrolled)/7 + 1
    
    time_invested.to_f/weeks_on_course
  end

  # kpi 11-12-13
  def avg_completers(week)
    completers_data = ActivityDetailView.where("course_id_lms = ? AND week = ? AND student_course_state != 'not_completed'", self.edition.course.id_lms, week)
    number_of_completers = completers_data.pluck(:id_user_lms).uniq.count
    
    completers_completed_activities = completers_data.where(state: 'completed', course_item_type_desc: ['lecture', 'quiz']).group(:course_item_type_desc).count
    
    @completers_completed_lectures = if completers_completed_activities['lecture'].present? && number_of_completers.present?
                                        completers_completed_activities['lecture'] / number_of_completers.to_f
                                     else
                                        0
                                     end 

                                      
    @completers_completed_evaluations = if completers_completed_activities['quiz'].present? && number_of_completers.present?
                                          completers_completed_activities['quiz'] / number_of_completers.to_f
                                        else
                                          0
                                        end
                                          
    @completers_time_invested = if completers_data.sum(:total_time_spent).present? && number_of_completers.present?
                                  completers_data.sum(:total_time_spent).to_f / number_of_completers / 3600
                                else
                                  0
                                end
    
    [@completers_completed_lectures, @completers_completed_evaluations, @completers_time_invested]
  end

  def most_effective_day
    completed_by_wday = self.resource_statuses.where("date_completed IS NOT NULL").group_by {|rs| rs.date_completed.wday }
    best_day = completed_by_wday.max_by{|k,v| v.count}

    best_day.present? ? (I18n.t('date.day_names')[best_day[0]]).capitalize : 'NA'
  end
  

  private
  
  def check_valid_enrolled_date
    if self.edition
      if self.date_enrolled < self.edition.start
        self.date_enrolled = self.edition.start
      end
    end
  end
  
  def resources_done(category, week, course)
    if 0 < week
      start_date = self.date_enrolled.to_datetime + (week-1).weeks
      end_date = start_date + 1.week
  
      videos_watched = ResourceStatus.where(enrollment: self, resource: Resource.where(category: category), date_completed: start_date..end_date).count
      videos_watched
    else
      0
    end
  end
  
end
