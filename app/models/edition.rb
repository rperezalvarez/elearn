class Edition < ApplicationRecord
  belongs_to :course
  has_many :enrollments

  scope :active, -> { where("start <= ? AND (? <= finish OR finish IS NULL)", DateTime.now, DateTime.now)}
  scope :enrollable, -> { where("start <= ? AND (? <= dead_line_enrollment_date OR dead_line_enrollment_date IS NULL)", DateTime.now, DateTime.now)}
  scope :real, -> { where('real = true') }
  scope :not_real, -> { where('real = false') }

  def week_period(week_number)
    startdate = (self.start).beginning_of_day
    finishdate = (startdate + week_number.week).end_of_day
    {start_date: startdate, finish_date: finishdate}
  end
    


end

