class Course < ApplicationRecord
  after_create :create_base_edition

  belongs_to :learning_management_system
  has_many :resources, dependent: :destroy

  has_many :editions, dependent: :destroy
  has_many :notes, dependent: :nullify

  scope :from_user, ->(id) { joins(editions: :enrollments).where("enrollments.user_id = ?", id).group("courses.id") }

  OFF_SET = 2

  def last_edition
    self.editions.order(:start).last
  end

  def total_duration
    self.duration + OFF_SET
  end

  def weeks
    weeks = []
    total_duration.times do |index|
      week = index + 1
      weeks.push( OpenStruct.new(week:week, title: "#{I18n.t('goals.week')} #{week}"))
    end

    weeks
  end

  private

    def create_base_edition
      Edition.create!(course: self, start: DateTime.now - 100.years, finish: DateTime.now + 1000.years, dead_line_enrollment_date: DateTime.now + 999.years, real: false)
    end

end
