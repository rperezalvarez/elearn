class SessionResource < ApplicationRecord
  after_create :check_if_resource_status_exists

  belongs_to :session
  belongs_to :resource

  # Scopes
  scope :effective, -> { joins(:resource).where('resources.category != ?', Resource::CATEGORIES[:others]) }
  scope :not_effective, -> { joins(:resource).where('resources.category = ?', Resource::CATEGORIES[:others]) }
  # scope :from_user, -> (id) { joins(session: :user).where("users.id = ?", id) }
  # scope :from_course, -> (id) { joins(resource: :course).where("courses.id = ?", id) }

  # Para actividades/urls que si poseen
  def self.get_url_element(url, i)
    require 'uri'
    uri = URI.parse(url)
    # self.resource = Resource.find_by_id_lms(uri.path.split('/')[i])
    return uri.path.split('/')[i]
  end

  private
    def check_if_resource_status_exists
      if self.resource.course.present? and self.resource.category != Resource::CATEGORIES[:others]
        course = self.resource.course
        enrollment = Enrollment.from_user(self.session.user).from_course(course).last
        if !ResourceStatus.find_by(enrollment: enrollment, resource: self.resource).present?
          ResourceStatus.create!(enrollment: enrollment, resource: self.resource, date_started: DateTime.now)
        end
      end
    end
end
