 class ActivityDetailView < ActiveRecord::Base
    self.table_name = 'activity_detail_view'

    protected

    # The report_state_popularities relation is a SQL view,
    # so there is no need to try to edit its records ever.
    # Doing otherwise, will result in an exception being thrown
    # by the database connection.
    def readonly?
      true
    end


    def self.refresh
      ActiveRecord::Base.connection.execute('REFRESH MATERIALIZED VIEW activity_detail_view')
    end
end # class