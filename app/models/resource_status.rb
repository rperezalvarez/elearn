class ResourceStatus < ApplicationRecord
  after_save :check_started_or_completed

  belongs_to :resource
  belongs_to :enrollment

  scope :from_user, -> (id) { joins(:enrollment).where("enrollments.user_id = ?", id) }
  scope :from_resource, -> (id) { where(resource_id: id) }
  scope :from_edition, ->(id) { joins(:enrollment).where("enrollments.edition_id = ?", id) }

  STATUS = {completed: 'Completed',started: 'Started', not_started: 'Not started'}

  def check_started_or_completed
    if self.status == STATUS[:not_started] and self.date_started != nil
      self.status = STATUS[:started]
      self.save!
    end

    if self.status != STATUS[:completed] and self.date_completed != nil
      self.status = STATUS[:completed]
      self.save!
    end
  end

  def status_at_end_of(session)
    session_duration = session.duration.present? ? session.duration : ((DateTime.now - session.start.to_datetime)*24*60*60).to_i

    if self.status == STATUS[:not_started]
      return self.status
    elsif self.status == STATUS[:started]
      if session.start <= self.date_started and self.date_started <= session.start + session_duration
        return self.status
      else
        return STATUS[:not_started]
      end
    elsif self.status == STATUS[:completed]
      started = session.start <= self.date_started and self.date_started <= session.start + session_duration
      completed = session.start <= self.date_completed and self.date_completed <= session.start + session_duration

      if completed
        return self.status
      elsif started
        return STATUS[:started]
      else
        return STATUS[:not_started]
      end

    end

  end


end
