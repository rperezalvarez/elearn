class Session < ApplicationRecord
  validate :check_if_exists_active_session, :on => :create
  before_destroy :remove_empty_user_enrollments, :remove_empty_memberships

  belongs_to :user
  has_many :session_resources, dependent: :destroy
  has_many :resources, through: :session_resources

  # Scopes
  scope :from_user, -> (id) { where(user_id: id) }
  scope :from_course, -> (id) { joins(resources: :course).where('courses.id = ?', id) }

  scope :start_between, -> (start, finish) { where('sessions.start >= ? AND sessions.start <= ?', start, finish) }

  def effective_time
    return self.session_resources.effective.sum('session_resources.active_time')
  end

  def procrastination_time
    return self.session_resources.not_effective.sum('session_resources.active_time')
  end

  # def close
  #   if self.duration.nil?
  #     start = self.start.to_datetime
  #     finish = self.session_resources.present? ? self.session_resources.order(:finish).last.finish.to_datetime : start
  #     self.duration = ((finish - start)*24*60*60).round
  #
  #     if self.save!
  #       return true
  #     else
  #       return false
  #     end
  #   else
  #     return true
  #   end
  #
  #   return false
  # end

  def close
    if self.duration.nil?
      if self.effective_time < AppConfiguration.min_session_effective_time_for_save
        self.destroy!
        return true
      else
        self.duration = (Time.now - self.start).round
        if self.save!
          return true
        else
          return false
        end
      end
    else
      return true
    end
  end

  private

  def check_if_exists_active_session
    errors.add(:base, 'There can only be one active session') if Session.where(user: self.user, duration: nil).present?
  end

  def remove_empty_user_enrollments
    session_courses = Course.joins(resources: :sessions).where("sessions.id = ?", self).distinct

    session_courses.each do |course|
      enrollments = Enrollment.from_user(self.user).from_course(course).distinct

      if enrollments.present? and enrollments.count == 1
        course_effective_time = Session.joins(:resources).where("resources.course_id = ? AND sessions.user_id = ? AND resources.category != ? ", course, self.user, Resource::CATEGORIES[:others]).distinct.sum("session_resources.active_time")

        if course_effective_time < AppConfiguration.min_session_effective_time_for_save
          enrollments.last.destroy
        end
      end

    end
  end

  def remove_empty_memberships
    # SI un usuario tiene una membership de la cual no posee ningun enrollment, entonces se borra
    user_courses_lmss = self.user.enrollments.collect{|e| e.edition.course.learning_management_system.id }.uniq.to_a
    empty_user_memberships = self.user.memberships.where.not(learning_management_system_id: user_courses_lmss)

    empty_user_memberships.distinct.destroy_all
  end

end
