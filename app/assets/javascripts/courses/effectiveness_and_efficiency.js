// Reset buttons
$(document).on('click', '.EE-remove-selection', function(e) {
  var chartID = $(this).attr('chart');
  var selChart = _.find(EECharts, function(chart) {
    return chart.ID.includes(chartID);
  });

  if(selChart) {
    selChart.dimension = selChart.dimension.filterAll();
    selChart.chart.selection(null);
    selChart.selection = null;
    updateEEChartsData();
    updateEESequence();
  }
});


function updateEEData(week) {
  $.ajax({
    type: "GET",
    contentType: "application/json; charset=utf-8",
    url: 'effectiveness_efficiency_data',
    dataType: 'json',
    success: function (data) {
      EEData = data;
      initEECrossfilter();
      drawEffectivenessCharts();
    },
    error: function(xhr, ajaxOptions, thrownError) {

    }
  });
}

function initEECrossfilter() {
  var xf = crossfilter(EEData);

  /* DIMENSIONS */
  EECharts.startedActivities.dimension = xf.dimension(function(d) { return d.resource_type + '-' + d.status; });
  EECharts.efficiencyByDay.dimension = xf.dimension(function(d) { return d.day_of_week + '/' + d.day_of_week_humanize + '-' + d.status; });
  EECharts.efficiencyByHour.dimension = xf.dimension(function(d) { return d.hour + '-' + d.status; });


  /* GROUPS */
  EECharts.startedActivities.group = EECharts.startedActivities.dimension.group().reduceCount();
  EECharts.efficiencyByDay.group = EECharts.efficiencyByDay.dimension.group().reduceCount();
  EECharts.efficiencyByHour.group = EECharts.efficiencyByHour.dimension.group().reduceCount();

}


function drawEffectivenessCharts(){
  // Set width for all charts
  for(var chart in EECharts) {
    EECharts[chart].width = $(EECharts[chart].ID).parent().width();
  }

  if(EEData.length){
    EECharts.startedActivities.chart = d3.cloudshapes.multiLevelDonutChart()
    .width(EECharts.startedActivities.width)
    .height(400)
    .labels(getStartedActivitiesLabels())
    .data(getStartedActivitiesData())
    .innRadius(60)
    .singleDonutWidth(50)
    .gap(2)
    .showLabels(true)
    .customColors(true)
    .colors(colorPalette)
    .selection(EECharts.startedActivities.selection)
    .showLabels(false)
    .analyticsClass('analytics-click-effectivenessByActivityTypeChart');

    EECharts.efficiencyByDay.chart = d3.cloudshapes.horizontalRowChart()
    .width(EECharts.efficiencyByDay.width)
    .height(300)
    .data(getEfficiencyByDayData())
    .dataUnit(I18n.t("graphs.dataUnit.activities"))
    .colors({ internal: colorPalette[I18n.t('graphs.colors.Completed')], external: colorPalette[I18n.t('graphs.colors.Started')]})
    .seriesNames(["Completed", "Started"].map(function(item) {
      return I18n.t(`graphs.seriesNames.${item}`)
    }))
    .selection(EECharts.efficiencyByDay.selection)
    .axisXTitle(I18n.t('graphs.axisTitles.Number_of_activities'))
    .analyticsClass("analytics-click-effectivenessByDafOfWeekChart");

    EECharts.efficiencyByHour.chart = d3.cloudshapes.verticalRowChart()
    .width(EECharts.efficiencyByHour.width)
    .height(300)
    .data(getEfficiencyByHourData())
    .colors({ internal: colorPalette[I18n.t('graphs.colors.Completed')], external: colorPalette[I18n.t('graphs.colors.Started')] })
    .dataUnit(I18n.t("graphs.dataUnit.activities"))
    .margin({top: 30, right: 30, bottom: 30, left: 30})
    .seriesNames(["Completed", "Started"].map(function(item) {
      return I18n.t(`graphs.seriesNames.${item}`)
    }))
    .selection(EECharts.efficiencyByHour.selection)
    .analyticsClass("analytics-click-effectivenessByHourOfDayChart");

    d3.select(EECharts.startedActivities.ID).call(EECharts.startedActivities.chart);
    d3.select(EECharts.efficiencyByDay.ID).call(EECharts.efficiencyByDay.chart);
    d3.select(EECharts.efficiencyByHour.ID).call(EECharts.efficiencyByHour.chart);


    /* CLICK FUNCTIONS */

    EECharts.startedActivities.chart.onClickFunction(function (elem, lvl, index, key) {
      var lab = EECharts.startedActivities.chart.labels();

      EECharts.startedActivities.selection = EECharts.startedActivities.chart.selection();

      if(lvl == 0) {
        EECharts.startedActivities.dimension = EECharts.startedActivities.dimension.filterFunction(function(d, i) {
          return d.toUpperCase().includes(key.toUpperCase());
        });

      } else if (lvl == 1) {
        EECharts.startedActivities.dimension = EECharts.startedActivities.dimension.filterFunction(function(d, i) {
          return d.toUpperCase() == (lab[0][parseInt(index/2)] + '-' + key).toUpperCase();
        });
      }

      updateEEChartsData();
    });

    EECharts.efficiencyByDay.chart.onClickFunction(function (elem, label, serie) {
      EECharts.efficiencyByDay.selection = EECharts.efficiencyByDay.chart.selection();

      if(serie.toUpperCase() == I18n.t('graphs.seriesNames.Completed').toUpperCase()) {
        EECharts.efficiencyByDay.dimension = EECharts.efficiencyByDay.dimension.filterFunction(function(d, i) {
          return d.toUpperCase().includes((label + '-' + serie).toUpperCase());
        });
      } else {
        EECharts.efficiencyByDay.dimension = EECharts.efficiencyByDay.dimension.filterFunction(function(d, i) {
          return d.toUpperCase().includes(label.toUpperCase());
        });
      }

      updateEEChartsData();
    });

    EECharts.efficiencyByHour.chart.onClickFunction(function (elem, label, serie) {
      EECharts.efficiencyByHour.selection = EECharts.efficiencyByHour.chart.selection();

      if(serie.toUpperCase() == I18n.t('graphs.seriesNames.Completed').toUpperCase()) {
        EECharts.efficiencyByHour.dimension = EECharts.efficiencyByHour.dimension.filterFunction(function(d, i) {
          return d.toUpperCase() == (label + '-' + serie).toUpperCase();
        });
      } else {
        EECharts.efficiencyByHour.dimension = EECharts.efficiencyByHour.dimension.filterFunction(function(d, i) {
          return d.split('-')[0] == label;
        });
      }

      updateEEChartsData();
    });


    updateEESequence();
  }
}

function updateEEChartsData() {

  EECharts.startedActivities.group = EECharts.startedActivities.dimension.group().reduceCount();
  EECharts.efficiencyByDay.group = EECharts.efficiencyByDay.dimension.group().reduceCount();
  EECharts.efficiencyByHour.group = EECharts.efficiencyByHour.dimension.group().reduceCount();

  EECharts.startedActivities.chart
  .labels(getStartedActivitiesLabels())
  .data(getStartedActivitiesData());

  EECharts.efficiencyByDay.chart
  .data(getEfficiencyByDayData());

  EECharts.efficiencyByHour.chart
  .data(getEfficiencyByHourData());

  updateEESequence();
}


function updateEESequence() {
  var sequenceSel = $('#EE-sequence');
  sequenceSel.empty();

  var dynamicCharts = _.filter(EECharts, function(c) {
    return c.type == "dynamic";
  });


  _.forEach(dynamicCharts, function(elem) {
    if(elem.chart.selection()) {
      var btnText = elem.chart.selection().label + "";
      if(elem.chart.selection().subCategory != null) {
        btnText = btnText.concat(" " + elem.chart.selection().subCategory);
      }
      sequenceSel.append("<button class='btn btn-default EE-remove-selection' title='Remove' chart='" + elem.ID.substring(1) + "'>" + btnText + " <i class='fa fa-times'></i></button>")
    }
  });
}

$(document).on('turbolinks:load', function() {
  if($(EECharts.startedActivities.ID).length) {
    updateEEData();
  }
});

$(document).on("turbolinks:before-cache", function() {
  for(var chart in EECharts) {
    EECharts[chart].chart = null;
    $(EECharts[chart].ID).empty();
    EECharts[chart].selection = null;
  }
});
