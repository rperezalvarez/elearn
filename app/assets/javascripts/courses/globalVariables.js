/* #####################################################
   #### ENGAGEMENT AND PERFORMANCE GLOBAL VARIABLES ####
   ##################################################### */

var EPData = null;
var staticData = null;
var requiredActivitiesData = null;
var OFF_SET = 2;
var colorPalette = null;

var EPCharts = {
  courseProgress: {
    ID: '#course-progress',
    chart: null,
    width: null,
    dimension: null,
    group: null,
    type: 'static'
  },
  courseTime: {
    ID: '#course-time',
    chart: null,
    width: null,
    dimension: null,
    group: null,
    type: 'static'
  },
  sessions: {
    ID: '#sessions-chart',
    chart: null,
    width: null,
    dimension: null,
    group: null,
    type: 'dynamic',
    selection: null
  },
  totalTime: {
    ID: '#total-time',
    chart: null,
    width: null,
    dimension: null,
    group: null,
    type: 'dynamic',
    selection: null
  },
  timeUse: {
    ID: '#time-use-chart',
    chart: null,
    width: null,
    dimension: null,
    group: null,
    type: 'dynamic',
    selection: null
  },
  engagementByActivities: {
    ID: '#engagement-by-number-of-activities',
    chart: null,
    width: null,
    dimension: null,
    group: null,
    type: 'dynamic',
    selection: null
  },
  engagementByActivityType: {
    ID: '#engagement-by-activity-type',
    chart: null,
    stackedChart: null,
    width: null,
    dimension: null,
    dimensionCount:null,
    groupCompletersCount:null,
    groupNoCompletersCount:null,
    groupLeft: null,
    groupLeftCompleters: null,
    groupLeftNoCompleters: null,
    groupLeftRequired: null,
    groupRight: null,
    groupRightCompleters: null,
    groupRightNoCompleters: null,
    groupRightRequired: null,
    type: 'dynamic',
    selection: null
  }
};



/* #####################################################
   ### EFFICIENCY AND EFFECTIVENESS GLOBAL VARIABLES ###
   ##################################################### */

   var EEData = null;
   var EECharts = {
     startedActivities: {
       ID: '#started-activities',
       chart: null,
       width: null,
       dimension: null,
       group: null,
       type: 'dynamic',
       selection: null
     },
     efficiencyByDay: {
       ID: '#efficiency-by-day',
       chart: null,
       width: null,
       dimension: null,
       group: null,
       type: 'dynamic',
       selection: null
     },
     efficiencyByHour: {
       ID: '#efficiency-by-hour',
       chart: null,
       width: null,
       dimension: null,
       group: null,
       type: 'dynamic',
       selection: null
     }
   };
