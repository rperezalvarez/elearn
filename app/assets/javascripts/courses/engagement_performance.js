// Week selector behaviour
$(document).on('click', '.week-btn', function(e) {
  if(!$(this).hasClass("disabled")) {
    $('.week-btn').removeClass('active');
    $(this).addClass('active');

    for(var chart in EPCharts) {
      if(EPCharts[chart].type == 'dynamic') {
        EPCharts[chart].selection = null;
      }
    }

    $('#EP-sequence').empty();
    updateEPData($(this).val());
  }
});

// Reset buttons to both dashboards
$(document).on('click', '.EP-remove-selection', function(e) {
  var chartID = $(this).attr('chart');
  var selChart = _.find(EPCharts, function(chart) {
    return chart.ID.includes(chartID);
  });

  if(selChart) {
    selChart.dimension = selChart.dimension.filterAll();
    selChart.chart.selection(null);
    selChart.selection = null;
    updateChartsData();
  }
});


// Temporal filter time use over the course buttons
$(document).on('click', '.time-use-btn', function(e) {
  if(!$(this).hasClass('active')) {
    var selectedFilter = $(this).attr('data-temporal-filter');
    $('.time-use-btn').removeClass('active');
    $(this).addClass('active');


    if(selectedFilter == 'all') {
      staticData.timeUseOverCourse = staticData.originalTimeUse;

    } else if (selectedFilter == 'last month') {
      var startDate = new Date();
      startDate.setDate(startDate.getDate() - 29);
      startDate = [startDate.getFullYear(), ('0' + (startDate.getMonth()+1)).slice(-2), ('0' + startDate.getDate()).slice(-2)].join('-');

      staticData.timeUseOverCourse = _.pickBy(staticData.originalTimeUse, function(d, key) {
        return key >= startDate;
      });
    } else if (selectedFilter == 'last week') {
      var startDate = new Date();
      startDate.setDate(startDate.getDate() - 6);
      startDate = [startDate.getFullYear(), ('0' + (startDate.getMonth()+1)).slice(-2), ('0' + startDate.getDate()).slice(-2)].join('-');

      staticData.timeUseOverCourse = _.pickBy(staticData.originalTimeUse, function(d, key) {
        return key >= startDate;
      });
    }

    updateStaticCharts();
  }
});


var ready = function() {
  // Set up toggle

  var comparationToggle = $('input#comparation').bootstrapToggle();

  $('input#comparation').change(function() {
    toggleStackedTornadoChart($(this).prop('checked'));
  });


  // Set width for all charts
  for(var chart in EPCharts) {
    EPCharts[chart].width = $(EPCharts[chart].ID).parent().width();
  }

  // Get active week and load initial data
  var activeWeek = $('.week-btn.active');
  if(activeWeek.length > 0) {
    updateEPData($('.week-btn.active').val());
    $.ajax({
      type: "GET",
      contentType: "application/json; charset=utf-8",
      url: 'progress_and_course_time_use',
      dataType: 'json',
      success: function (data) {
        staticData = data;
        //console.log(data);
        drawStaticCharts();
      },
      error: function (result) {
        staticData = null;
      }
    });
  }
};



function updateEPData(week) {

  $.ajax({
    type: "GET",
    contentType: "application/json; charset=utf-8",
    url: 'required_activities',
    dataType: 'json',
    data: {week: week },
    success: function (newData) {
      requiredActivitiesData = newData;
    },
    error: function (result) {
      requiredActivitiesData = null;
    }
  });

  $.ajax({
    type: "GET",
    contentType: "application/json; charset=utf-8",
    url: 'engagement_performance_data',
    dataType: 'json',
    data: {week: week },
    success: function (newData) {
      EPData = newData;
      // console.log(newData);      //console.log('Time 7: ' + d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() + '.' + d.getMilliseconds());
      destroyCharts('dynamic');
      initCrossfilter();
      drawCharts();
    },
    error: function (result) {
      EPData = null;
    }
  });
}

function initCrossfilter() {
  var xf = crossfilter(EPData.data);
  var uniqueUsers = _.uniqBy(_.filter(EPData.data, function(obj) {return obj.is_comparation}), 'user_id');
  var uniqueUsersXf = crossfilter(uniqueUsers);

  /* DIMENSIONS */
  EPCharts.sessions.dimension = xf.dimension(function (d) { return d.day_of_week + '/' + d.day_of_week_humanize + '-' + d.session_id});
  EPCharts.totalTime.dimension = xf.groupAll();
  EPCharts.timeUse.dimension = xf.dimension(function (d) {
    if(d.procrastination) {
      return I18n.t('graphs.labels.Procrastination');
    } else {
      return I18n.t('graphs.labels.Effective');
    }
  });
  EPCharts.engagementByActivities.dimension = xf.dimension(function (d) { return d.status; });
  EPCharts.engagementByActivityType.dimension = xf.dimension(function (d) { return d.resource_type + "-" + d.status; });
  EPCharts.engagementByActivityType.dimensionCount = uniqueUsersXf.dimension(function (d) {return d.is_completer;}).group().reduceCount();

  /* GROUPS */
  EPCharts.sessions.group = EPCharts.sessions.dimension.group().reduceSum(reduceTime_notComparation);
  EPCharts.totalTime.group = xf.groupAll().reduceSum(reduceTime_notComparation);
  EPCharts.timeUse.group = EPCharts.timeUse.dimension.group().reduceSum(reduceTime_notComparation);
  EPCharts.engagementByActivities.group = EPCharts.engagementByActivities.dimension.group().reduce(
    reduceAddCountActivities_notComparation,
    reduceRemoveCountActivities_notComparation,
    reduceInitCountActivities_notComparation
  );

  EPCharts.engagementByActivityType.groupNoCompletersCount = EPData.number_of_other_students.no_completers;
  EPCharts.engagementByActivityType.groupCompletersCount = EPData.number_of_other_students.completers;

  EPCharts.engagementByActivityType.groupLeft = EPCharts.engagementByActivityType.dimension.group().reduce(
    reduceAddCountActivities_notComparation,
    reduceRemoveCountActivities_notComparation,
    reduceInitCountActivities_notComparation
  );

  EPCharts.engagementByActivityType.groupLeftCompleters = _.map(_.filter(EPData.data, function(elem) {
    return elem.is_comparation && elem.is_completer;
  }), function(obj) {
    return {key: obj.resource_type + '-' + obj.status, value: obj.number_of_activities};
  });

  EPCharts.engagementByActivityType.groupLeftNoCompleters = _.map(_.filter(EPData.data, function(elem) {
    return elem.is_comparation && !elem.is_completer;
  }), function(obj) {
    return {key: obj.resource_type + '-' + obj.status, value: obj.number_of_activities};
  });

  EPCharts.engagementByActivityType.groupRight = EPCharts.engagementByActivityType.dimension.group().reduce(
    reduceAddTime_notComparation,
    reduceRemoveTime_notComparation,
    reduceInitTime_notComparation
  );
  EPCharts.engagementByActivityType.groupRightCompleters = EPCharts.engagementByActivityType.dimension.group().reduce(
    reduceAddTime_comparation_completers,
    reduceRemoveTime_comparation_completers,
    reduceInitTime_comparation_completers
  );
  EPCharts.engagementByActivityType.groupRightNoCompleters = EPCharts.engagementByActivityType.dimension.group().reduce(
    reduceAddTime_comparation_noCompleters,
    reduceRemoveTime_comparation_noCompleters,
    reduceInitTime_comparation_noCompleters
  );
}


function drawCharts() {

  if(EPCharts.sessions.group.all().length) {
    EPCharts.sessions.chart = d3.cloudshapes.groupedBarChart()
      .width(EPCharts.sessions.width)
      .height(200)
      .data(getSessionsData())
      .dataTitle("Sessions this week")
      .dataUnit(I18n.t(`graphs.dataUnit.min`))
      .labels(EPData.sessions_labels)
      .labelRotation(-20)
      .margin({top: 20, right: 30, bottom: 40, left: 70})
      .selectable(true)
      .legendTitle(false)
      .barsColor(colorPalette[I18n.t('graphs.colors.Session')])
      .lineMeanColor(colorPalette[I18n.t('graphs.colors.MeanTime')])
      .lineTotalColor(colorPalette[I18n.t('graphs.colors.TotalTime')])
      .gapBar(2)
      .dataLineTotal(EPData.avg_day_time)
      .dataLineAvg(EPData.avg_session_time)
      .selection(EPCharts.sessions.selection)
      .axisYTitle(I18n.t('graphs.axisTitles.Minutes'))
      .analyticsClass("analytics-click-sessionsChart");

    timeUseData = EPCharts.timeUse.group.all().map(function (d) { return round(d.value, 1); });
    timeUseLabels = EPCharts.timeUse.group.all().map(function (d) { return d.key; });

    if(timeUseLabels.length == 1) {
      if(timeUseLabels[0] == I18n.t('legends.effective_time')) {
        timeUseData = timeUseData.concat([0]);
        timeUseLabels = timeUseLabels.concat([I18n.t('legends.procrastination')]);
      } else {
        timeUseData = [0].concat(timeUseData);
        timeUseLabels = [I18n.t('legends.procrastination')].concat(timeUseLabels);
      }
    }
    
    EPCharts.timeUse.chart = d3.cloudshapes.donutChartElearn()
      .width(EPCharts.timeUse.width)
      .height(200)
      .data(timeUseData)
      .labels(timeUseLabels)
      .dataUnit(I18n.t(`graphs.dataUnit.min`))
      .innRadius(40)
      .outRadius(80)
      .margin({ top: 0, right: 10, bottom: 0, left: 30})
      .showLegend(true)
      .showLabel(true)
      .colors(_.map(timeUseLabels, function(lab) { return colorPalette[lab]; }))
      .selection(EPCharts.timeUse.selection)
      .legendY(15)
      .analyticsClass("analytics-click-timeUseChart");

    EPCharts.engagementByActivities.chart = d3.cloudshapes.donutChartElearn()
      .width(EPCharts.engagementByActivities.width)
      .height(200)
      // .data(_.without(EPCharts.engagementByActivities.group.all().map(function (d) { if(d.key.toUpperCase() != "NOT STARTED") { return d.value; }}), null, undefined))
      .data(_.without(EPCharts.engagementByActivities.group.all().map(function (d) { if(notStartedStrings.indexOf(d.key.toUpperCase()) < 0){ return d.value; }}), null, undefined))      
      .labels(_.compact(EPCharts.engagementByActivities.group.all().map(function (d) { if(notStartedStrings.indexOf(d.key.toUpperCase()) < 0) { return d.key; }})))
      .dataUnit(I18n.t(`graphs.dataUnit.activities`))
      .innRadius(40)
      .outRadius(80)
      .margin({ top: 0, right: 10, bottom: 0, left: 30})
      .showLegend(true)
      .showLabel(true)
      .colors([colorPalette[I18n.t('graphs.colors.Started')], colorPalette[I18n.t('graphs.colors.Completed')]])
      .selection(EPCharts.engagementByActivities.selection)
      .legendY(15)
      .analyticsClass("analytics-click-engagementByNumberByActivitiesChart");

    // Hide if no data available
    if(0 < _.sum(_.without(EPCharts.engagementByActivities.group.all().map(function (d) { if(notStartedStrings.indexOf(d.key.toUpperCase()) < 0) { return d.value; }}), null, undefined))) {
      $(EPCharts.engagementByActivities.ID).show();
    } else {
      $(EPCharts.engagementByActivities.ID).hide();
    }

    EPCharts.engagementByActivityType.chart = d3.cloudshapes.groupedTornadoChart()
      .width(EPCharts.engagementByActivityType.width)
      .height(300)
      .seriesNames(getEngagementByActivityTypeSeriesNames().map(function(item) {
        return I18n.t(`graphs.seriesNames.${item}`)
      }))
      .gapBar(2)
      .colors([colorPalette[I18n.t('graphs.colors.Completed')], colorPalette[I18n.t('graphs.colors.Started')], colorPalette[I18n.t('graphs.colors.Required')]])
      .dataUnit(I18n.t("graphs.dataUnit.activities"))
      .middleGap(0.1)
      .showAxisX(false)
      .showAxisY(false)
      .selectable(false)
      .xAxisLeftTitle(I18n.t(`graphs.axisTitles.${"Number of activities".split(" ").join("_")}`))
      .xAxisRightTitle(I18n.t(`graphs.axisTitles.${"Minutes".split(" ").join("_")}`))
      .leftDataUnit(I18n.t("graphs.dataUnit.activities"))
      .rightDataUnit(I18n.t("graphs.dataUnit.min"))
      .selection(EPCharts.engagementByActivityType.selection)
      .analyticEventName(hoverEventNames["engagementByActivityTypeChart"]);

    EPCharts.engagementByActivityType.stackedChart = d3.cloudshapes.stackedTornadoChart()
      .width(EPCharts.engagementByActivityType.width)
      .height(300)
      .seriesNames(getEngagementByActivityTypeSeriesNames(false))
      .gapBar(2)
      .colors([colorPalette[I18n.t('graphs.colors.Completed')], colorPalette[I18n.t('graphs.colors.Started')], colorPalette[I18n.t('graphs.colors.Required')]])
      .dataUnit(I18n.t("graphs.dataUnit.activities"))
      .middleGap(0.1)
      .showAxisX(false)
      .showAxisY(false)
      .selectable(false)
      .xAxisLeftTitle(I18n.t(`graphs.axisTitles.${"Number of activities".split(" ").join("_")}`))
      .xAxisRightTitle(I18n.t(`graphs.axisTitles.${"Minutes".split(" ").join("_")}`))
      .leftDataUnit(I18n.t("graphs.dataUnit.activities"))
      .rightDataUnit(I18n.t("graphs.dataUnit.min"))
      .analyticEventName(hoverEventNames["engagementByActivityTypeChart"]);

    EPCharts.engagementByActivityType.chart.labels(getEngagementByActivityTypeLabels().map(function(item) {
      return I18n.t(`graphs.resourceType.${item}`);
    }))
      .data(getEngagementByActivityTypeData('you'));

    EPCharts.engagementByActivityType.stackedChart.labels(getEngagementByActivityTypeLabels().map(function(item) {
      return I18n.t(`graphs.resourceType.${item}`);
    }))
      .labelsText(['You', 'Completers', 'No completers'].map(function(item) {
        return I18n.t(`graphs.labels.${item}`)
      }))
      .data([getEngagementByActivityTypeData('you'), getEngagementByActivityTypeData('completer'), getEngagementByActivityTypeData('no completer')]);

    d3.select(EPCharts.sessions.ID).call(EPCharts.sessions.chart);
    ebaData = _.without(EPCharts.engagementByActivities.group.all().map(function (d) { if(notStartedStrings.indexOf(d.key.toUpperCase()) < 0) { return d.value; }}), null, undefined);

    if(0 < _.sum(ebaData)) {
      d3.select(EPCharts.timeUse.ID).call(EPCharts.timeUse.chart);
    }

    d3.select(EPCharts.engagementByActivities.ID).call(EPCharts.engagementByActivities.chart);
    // Default nos stacked
    d3.select(EPCharts.engagementByActivityType.ID).call(EPCharts.engagementByActivityType.chart);
    d3.select(EPCharts.engagementByActivityType.ID).call(EPCharts.engagementByActivityType.stackedChart);
    $(EPCharts.engagementByActivityType.ID + ' .elearn-stackedTornadoChart').hide();

    /* CLICK FUNCTIONS */

    EPCharts.sessions.chart.onClickFunction(function (elem, val, key, sub_i) {
      EPCharts.sessions.selection = { label: key, subIndex: sub_i };

      var sessionGroups = _.groupBy(EPCharts.sessions.group.all(), function (d) {
        return d.key.split('-')[0].split('/')[1];
      });
      var sessionId = sessionGroups[key][sub_i];

      EPCharts.sessions.dimension = EPCharts.sessions.dimension.filterFunction(function (d,i) {
        return d.split('-')[1] == sessionId.key.split('-')[1];
      });

      updateChartsData();
    });

    EPCharts.timeUse.chart.onClickFunction(function (elem, chart, key) {
      if ($(elem).parent().hasClass("_selected_")) {
        EPCharts.timeUse.selection = { label: key };
        EPCharts.timeUse.dimension = EPCharts.timeUse.dimension.filterFunction(function (d, i) {
          return d == key;
        });
      } else {
        console.log('ERROR');
      }
      updateChartsData();
    });

    EPCharts.engagementByActivities.chart.onClickFunction(function (elem, chart, key) {
      if ($(elem).parent().hasClass("_selected_")) {
        EPCharts.engagementByActivities.selection = { label: key };

        EPCharts.engagementByActivities.dimension = EPCharts.engagementByActivities.dimension.filterFunction(function (d, i) {
          return d == key;
        });
      } else {
        console.log('ERROR');
      }

      updateChartsData();
    });

    EPCharts.engagementByActivityType.chart.onClickFunction(function (elem, chart, entry) {
      var activityType = I18n.t(`graphs.resourceStatus.${$(elem).parent().attr('name-label')}`)
      EPCharts.engagementByActivityType.dimension = EPCharts.engagementByActivityType.dimension.filterFunction(function (d, i) {
        return d.toUpperCase() == (entry + '-' + activityType).toUpperCase();
      });
      updateChartsData();
    });

  } else {
    destroyCharts('dynamic');
  }

  // Always draw KPI chart
  // var screenWidth = $(document).width();
  // var kpiHeight = 120;
  // if(screenWidth > ) kpiHeight = 320;
  var totalTimeVal =  EPCharts.totalTime.group.value() ? EPCharts.totalTime.group.value() : 0.0;

  EPCharts.totalTime.chart = d3.cloudshapes.kpiChart()
    .width(EPCharts.totalTime.width)
    .height(120)
    .title(I18n.t("section_titles.time_invested.title"))
    .dataUnit(I18n.t("graphs.dataUnit.min"))
    .data(totalTimeVal)
    .backgroundColor(colorPalette[I18n.t('graphs.colors.EffectiveTime')]);

  d3.select(EPCharts.totalTime.ID).call(EPCharts.totalTime.chart);

  toggleNoDataMessage();

}

function updateStaticCharts() {
  var dates = Object.keys(staticData.timeUseOverCourse);

  var effective_values = _.map(staticData.timeUseOverCourse, function(d) {
    return d.effective;
  });
  var procrastination_values = _.map(staticData.timeUseOverCourse, function(d) {
    return d.procrastination;
  });

  for(var chart in EPCharts) {
    EPCharts[chart].width = $(EPCharts[chart].ID).parent().width();
  }

  EPCharts.courseTime.chart
  .labels(dates)
  .data([effective_values, procrastination_values]);
}

function drawStaticCharts() {

  // Course progress

  EPCharts.courseProgress.chart = d3.cloudshapes.horizontalRowChart()
  .width(EPCharts.courseProgress.width)
  .height(130)
  .data([
    { label: I18n.t(`graphs.resourceType.Videos`), values: [staticData.progress.completed_lectures, staticData.progress.required_lectures] },
    { label: I18n.t(`graphs.resourceType.Evaluations`), values: [staticData.progress.completed_evaluations, staticData.progress.required_evaluations] }
  ])
  .selectable(false)
  .margin({top: 30, right: 0, bottom: 20, left: 80})
  .colors({internal: colorPalette[I18n.t('graphs.colors.Completed')], external: colorPalette[I18n.t(`graphs.colors.Required`)]})
  .seriesNames(["Completed", "Required"].map(function(elem) {
    return I18n.t(`graphs.seriesNames.${elem}`)
  }))
  .axisXTitle(I18n.t('graphs.axisTitles.Number_of_activities'));

  // Course time

  var dates = Object.keys(staticData.timeUseOverCourse);

  var effective_values = _.map(staticData.timeUseOverCourse, function(d) {
    return d.effective;
  });
  var procrastination_values = _.map(staticData.timeUseOverCourse, function(d) {
    return d.procrastination;
  });

  for(var chart in EPCharts) {
    EPCharts[chart].width = $(EPCharts[chart].ID).parent().width();
  }

  EPCharts.courseTime.chart = d3.cloudshapes.lineChartElearn()
  .width(EPCharts.courseTime.width)
  .height(200)
  .data([effective_values, procrastination_values])
  .labels(dates)
  .seriesNames(["Effective", "Procrastination"].map(function(elem) {
    return I18n.t(`graphs.seriesNames.${elem}`)
  }))
  .colors([colorPalette[I18n.t('graphs.colors.EffectiveTime')], colorPalette[I18n.t('graphs.colors.Procrastination')]])
  .margin({top: 20, right: 30, bottom: 20, left: 50})
  .dataUnit(I18n.t("graphs.dataUnit.min"))
  .analyticEventName(hoverEventNames['courseTimeUse']);

  d3.select(EPCharts.courseProgress.ID).call(EPCharts.courseProgress.chart);
  d3.select(EPCharts.courseTime.ID).call(EPCharts.courseTime.chart);

  toggleNoDataMessage();
}

function destroyCharts(type=null) {
  for(var chart in EPCharts) {
    if(type) {
      if(EPCharts[chart].type == 'dynamic') {
        $(EPCharts[chart].ID).empty();
      }
    } else {
      $(EPCharts[chart].ID).empty();
    }
  }
}

function updateChartsData() {

  // Update groups
  EPCharts.sessions.group = EPCharts.sessions.dimension.group().reduceSum(reduceTime_notComparation);
  EPCharts.totalTime.group = EPCharts.totalTime.dimension.reduceSum(reduceTime_notComparation);
  EPCharts.timeUse.group = EPCharts.timeUse.dimension.group().reduceSum(reduceTime_notComparation);

  EPCharts.engagementByActivities.group = EPCharts.engagementByActivities.dimension.group().reduce(
    reduceAddCountActivities_notComparation,
    reduceRemoveCountActivities_notComparation,
    reduceInitCountActivities_notComparation
  );
  EPCharts.engagementByActivityType.groupLeft = EPCharts.engagementByActivityType.dimension.group().reduce(
    reduceAddCountActivities_notComparation,
    reduceRemoveCountActivities_notComparation,
    reduceInitCountActivities_notComparation
  );
  EPCharts.engagementByActivityType.groupRight = EPCharts.engagementByActivityType.dimension.group().reduce(
    reduceAddTime_notComparation,
    reduceRemoveTime_notComparation,
    reduceInitTime_notComparation
  );

  // Update labels and data
  EPCharts.sessions.chart.labels(EPData.sessions_labels).data(getSessionsData());
  EPCharts.totalTime.chart.data(EPCharts.totalTime.group.value());

  EPCharts.timeUse.chart.labels(EPCharts.timeUse.group.all().map(function (d) {
    return d.key;
  }))
  .data(EPCharts.timeUse.group.all().map(function (d) {
    return round(d.value, 1).toFixed(1);
  }));

  ebaData = _.without(EPCharts.engagementByActivities.group.all().map(function (d) { if(notStartedStrings.indexOf(d.key.toUpperCase()) < 0) { return d.value; }}), null, undefined);
  // Hide if no data available
  if(0 < _.sum(ebaData)) {
    $(EPCharts.engagementByActivities.ID).show();
    EPCharts.engagementByActivities.chart
    .data(ebaData);
  } else {
    $(EPCharts.engagementByActivities.ID).hide();
  }

  EPCharts.engagementByActivityType.chart
  .seriesNames(getEngagementByActivityTypeSeriesNames().map(function(item){
    return I18n.t(`graphs.seriesNames.${item}`)
  }))
  .data(getEngagementByActivityTypeData('you'))
  .labels(getEngagementByActivityTypeLabels().map(function(item){
    return I18n.t(`graphs.resourceType.${item}`);
  }));


  toggleNoDataMessage();
  updateEPSequence();
}


/* AUXILIAR FUNCTIONS */
function capitalize(string) {
  if(string.length > 0) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  } else {
    return string;
  }
}

function updateEPSequence() {
  var sequenceSel = $('#EP-sequence');
  sequenceSel.empty();

  var dynamicCharts = _.filter(EPCharts, function(c) {
    return c.type == "dynamic";
  });

  _.forEach(dynamicCharts, function(elem) {
    if(elem.chart.selection()) {
      var btnText = elem.chart.selection().label
      if(elem.chart.selection().subIndex != null) {
        btnText = btnText.concat(" Session " + (elem.chart.selection().subIndex + 1));
      }
      sequenceSel.append("<button class='btn btn-sm btn-default EP-remove-selection' style='margin-left:5px;' title='Remove' chart='" + elem.ID.substring(1) + "'>" + btnText + " <i class='fa fa-times'></i></button>")
    }
  });
}

function toggleNoDataMessage() {
  $('.chart-container .no-data').remove();

  $('.chart-container span').each(function(i) {
    if($(this).is(':empty') || !$(this).is(':visible')) {
      $(this).parent().append("<div class='no-data'><i class='fa fa-ban fa-5x'></i><h3>No data available</h3></div>")
    }
  })
}

function toggleStackedTornadoChart(enable) {  
  if(enable) {
    ahoy.track(clickEventNames['comparationToggleEnabled'], {course_id: getCourseIdFromUrl()});
    $(EPCharts.engagementByActivityType.ID + ' .elearn-stackedTornadoChart').show();
    $(EPCharts.engagementByActivityType.ID + ' .elearn-groupedTornadoChart').hide();
  } else {
    ahoy.track(clickEventNames['comparationToggleDisabled'], {course_id: getCourseIdFromUrl()});
    $(EPCharts.engagementByActivityType.ID + ' .elearn-stackedTornadoChart').hide();
    $(EPCharts.engagementByActivityType.ID + ' .elearn-groupedTornadoChart').show();
  }
}


$(document).on('turbolinks:load', ready);

$(window).resize(function() {
  for(var chart in EPCharts) {
    EPCharts[chart].width = $(EPCharts[chart].ID).parent().width();
  }

  if($('.week-btn.active').length) {
    destroyCharts();
    drawStaticCharts();
    drawCharts();
  }
});

$(document).on("turbolinks:before-cache", function() {
  for(var chart in EPCharts) {
    EPCharts[chart].chart = null;
    $(EPCharts[chart].ID).empty();
    EPCharts[chart].selection = null;
  }
});
