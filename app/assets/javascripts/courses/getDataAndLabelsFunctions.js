/* ####################################################
   ####### ENGAGEMENT AND PERFORMANCE FUNCTIONS #######
   #################################################### */

function getSessionsData() {
  var sessionsData = [];

  var sessionGroups = _.groupBy(EPCharts.sessions.group.all(), function (d) {
    return d.key.split('-')[0].split('/')[0];
  });
  var maxNumberSeries = _.max(_.map(sessionGroups, function (d) {
    return d.length;
  }));

  var chartLab = EPData.sessions_labels;
  var startIndex = 0;
  var endIndex = chartLab.length;

  if($('.week-btn.active').val() == "view all") {
    startIndex = 1;
    endIndex = chartLab.length + 1;
  }
  for(var i=0; i < maxNumberSeries; i++) {
    var newDataGroup = [];
    for(day = startIndex; day < endIndex; day++) {
      if(sessionGroups[day]) {
        if(sessionGroups[day][i]) {
          newDataGroup.push(sessionGroups[day][i].value);
        } else {
          newDataGroup.push(0);
        }
      } else {
        newDataGroup.push(0);
      }
    }
    sessionsData.push(newDataGroup);
  }

  return sessionsData;
}

function getEngagementByActivityTypeSeriesNames(withRequired=true) {
  if(EPCharts.sessions.chart) {
    if(EPCharts.sessions.chart.selection()) {
      return ['Completed', 'Started'];
    } else if ($('.week-btn')) {
      var weeksCount = $('.week-btn').length-1;
      if(weeksCount - OFF_SET < parseInt($('.week-btn.active').val())) {
        return ['Completed', 'Started'].map(function(item) {
          return I18n.t(`graphs.resourceStatus.${item}`);
        });

      }
      if(!withRequired) {
        return ['Completed', 'Started'].map(function(item) {
          return I18n.t(`graphs.resourceStatus.${item}`);
        });
      }
    }

    return ['Completed', 'Started', 'Required'].map(function(item) {
      return I18n.t(`graphs.resourceStatus.${item}`);
    });
  }
}

function getEngagementByActivityTypeLabels() {
  var requiredLabels = _.uniq(_.map(requiredActivitiesData, function(d) { return d.resource_type.charAt(0).toUpperCase() + d.resource_type.slice(1); }));
  return _.without(requiredLabels, I18n.t("graphs.resourceType.Procrastination"), I18n.t("graphs.resourceType.Info"))
}

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

function getEngagementByActivityTypeData(userType) {
  var labels = getEngagementByActivityTypeLabels().map(function(item) {
    return I18n.t(`graphs.resourceType.${item}`);
  });
    
  EPCharts.engagementByActivityType.chart.seriesNames(getEngagementByActivityTypeSeriesNames());
  var groupLeft = [];
  var groupRight = [];
  var seriesNames = "";

  if(userType == 'you') {
    groupLeft = EPCharts.engagementByActivityType.groupLeft.all();
    groupRight = EPCharts.engagementByActivityType.groupRight.all();
    seriesNames = EPCharts.engagementByActivityType.chart.seriesNames();
    
  } else if (userType == 'completer') {
    groupLeft = EPCharts.engagementByActivityType.groupLeftCompleters;
    groupLeft.forEach(function(obj) {
      obj.value = (obj.value / EPCharts.engagementByActivityType.groupCompletersCount);
    })
    groupRight = EPCharts.engagementByActivityType.groupRightCompleters.all();
    groupRight.forEach(function(obj) {
      obj.value = obj.value / EPCharts.engagementByActivityType.groupCompletersCount;

    })
    seriesNames = EPCharts.engagementByActivityType.stackedChart.seriesNames();

  } else if (userType == 'no completer') {
    groupLeft = EPCharts.engagementByActivityType.groupLeftNoCompleters;
    groupLeft.forEach(function(obj) {
      obj.value = (obj.value / EPCharts.engagementByActivityType.groupNoCompletersCount);
    })

    groupRight = EPCharts.engagementByActivityType.groupRightNoCompleters.all();
    groupRight.forEach(function(obj) {
      obj.value = obj.value / EPCharts.engagementByActivityType.groupNoCompletersCount;
    })

    seriesNames = EPCharts.engagementByActivityType.stackedChart.seriesNames();
  }

  var leftData = [];
  var rightData = [];
  seriesNames.forEach(function(seriesName) {
    var serieDataLeft = [];
    var serieDataRight = [];

    labels.forEach(function(label) {
      if(seriesName.toUpperCase() == I18n.t(`graphs.resourceStatus.Required`).toUpperCase()) {
        var resourceTypeData = _.filter(requiredActivitiesData, function(d) {
          return I18n.t(`graphs.resourceType.${d.resource_type.capitalize()}`).toUpperCase() == label.toUpperCase();
        });

        if(resourceTypeData.length > 0) {
          var requiredTime = _.sumBy(resourceTypeData, function(d) {
            if(d.duration) {
              return d.duration;
            } else {
              return 0;
            }
          });
          var requiredActivities = resourceTypeData.length;

          serieDataLeft.push(requiredActivities);
          serieDataRight.push(requiredTime);

        } else {
          serieDataLeft.push(0);
          serieDataRight.push(0);
        }
      
      } else {
        var key = label + '-' + seriesName;
        var groupEntryLeft = _.find(groupLeft, function(d) {
          return key.toUpperCase() == d.key.toUpperCase();
        });

        if(groupEntryLeft) {
          serieDataLeft.push(parseFloat(groupEntryLeft.value.toFixed(2)));
        } else {
          serieDataLeft.push(0);
        }

        var groupEntryRight = _.find(groupRight, function(d) {
          return key.toUpperCase() == d.key.toUpperCase();
        });

        if(groupEntryRight) {
          serieDataRight.push(parseFloat(groupEntryRight.value.toFixed(1)));
        } else {
          serieDataRight.push(0);
        }
      }
    });

    leftData.push(serieDataLeft);
    rightData.push(serieDataRight);
  });
  return [leftData, rightData];
}



/* ####################################################
   ###### EFFICIENCY AND EFFECTIVENESS FUNCTIONS ######
   #################################################### */

function getStartedActivitiesLabels() {
  var firstLvl = _.uniq(_.map(EECharts.startedActivities.group.all(), function(d) { return d.key.split('-')[0]; }));
  var secondLvl = _.flatten(_.map(firstLvl, function() { return ["Completed", "Not completed"].map(function(item){
    return I18n.t(`graphs.resourceStatus.${item.split(" ").join("_")}`); })}));
  return [firstLvl, secondLvl];
}

function getStartedActivitiesData() {
  var resourceTypes = getStartedActivitiesLabels()[0];

  var firstLvl = _.map(resourceTypes, function(r) {
    var resourceTypeData = _.filter(EECharts.startedActivities.group.all(), function(d) {
      return d.key.toUpperCase().includes(r.toUpperCase());
    });

    if(resourceTypeData) {
      return _.sumBy(resourceTypeData, function(d) { return d.value; })
    } else {
      return 0;
    }
  });

  var secondLvl = _.flatten(_.map(resourceTypes, function(r) {
    var completed = _.find(EECharts.startedActivities.group.all(), function(d) {
      return d.key.toUpperCase() == (r.toUpperCase() + '-' + I18n.t('graphs.resourceStatus.Completed').toUpperCase());
    });

    var notCompleted = _.find(EECharts.startedActivities.group.all(), function(d) {
      return d.key.toUpperCase() == (r.toUpperCase() + '-' + I18n.t('graphs.resourceStatus.Not completed').toUpperCase());
    });

    return [
      (completed) ? completed.value : 0,
      (notCompleted) ? notCompleted.value : 0
    ];
  }));
  // var firstLvl = _.map(EECharts.startedActivities.group.all(), function(d) { return d.value; });
  // var secondLvl = _.flatten(_.map(EECharts.startedActivities.group.all(), function(d) { return [d.value, d.value - d.value.completed]; }));
  return [firstLvl, secondLvl];
}

function getEfficiencyByDayData() {
  var labels = _.uniq(_.map(EECharts.efficiencyByDay.group.all(), function(d) { return d.key.split('-')[0].split('/')[1]; }));

  var output = _.map(labels, function(label) {
    var completed = _.find(EECharts.efficiencyByDay.group.all(), function(d) {
      return d.key.toUpperCase().includes(label.toUpperCase() + '-' + I18n.t('graphs.resourceStatus.Completed').toUpperCase());
    });
    var notCompleted = _.find(EECharts.efficiencyByDay.group.all(), function(d) {
      return d.key.toUpperCase().includes(label.toUpperCase() + '-' + I18n.t('graphs.resourceStatus.Not completed').toUpperCase());
    });

    var outputEntry = [
      (completed) ? completed.value : 0,
      (notCompleted) ? notCompleted.value : 0
    ];

    return {
      label: label,
      values: [
        outputEntry[0],
        _.sum(outputEntry)
      ]
    };
  });
  return output;
}

function getEfficiencyByHourData() {
  var output = _.map(Array.from(Array(24).keys()), function(label) {
    var completed = _.find(EECharts.efficiencyByHour.group.all(), function(d) {
      return d.key.toUpperCase() == (label + '-' + I18n.t('graphs.resourceStatus.Completed').toUpperCase());
    });
    var notCompleted = _.find(EECharts.efficiencyByHour.group.all(), function(d) {
      return d.key.toUpperCase() == (label + '-' + I18n.t('graphs.resourceStatus.Not completed').toUpperCase());
    });

    var outputEntry = [
      (completed) ? completed.value : 0,
      (notCompleted) ? notCompleted.value : 0
    ];

    return {
      label: label,
      values: [
        outputEntry[0],
        _.sum(outputEntry)
      ]
    };
  });
  return output;
}

function round(value, precision) {
  var multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
}