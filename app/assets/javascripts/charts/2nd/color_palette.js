
var notStartedStrings = [];
var completedStrings = [];

$( document ).ready(function() {
  var colors = {};  
  var availableLanguages = Object.keys(I18n.translations);

  availableLanguages.forEach(function(lang) {
    colors[I18n.t("graphs.colors.Procrastination", {locale: lang})] = "#E57373";
    colors[I18n.t("graphs.colors.EffectiveTime", {locale: lang})]= "#4FC3F7";
    colors[I18n.t("graphs.colors.Session", {locale: lang})]= "#FFB74D";
    colors[I18n.t("graphs.colors.Completed", {locale: lang})]= "#4DD0E1";
    colors[I18n.t("graphs.colors.Not_completed", {locale: lang})]= "#EEDDCC";
    colors[I18n.t("graphs.colors.Started", {locale: lang})]= "#90A4AE";
    colors[I18n.t("graphs.colors.Required", {locale: lang})]= "#FF8A65";
    colors[I18n.t("graphs.colors.Lectures", {locale: lang})]= "#4DB6AC";
    colors[I18n.t("graphs.colors.Discussions", {locale: lang})]= "#BA68C8";
    colors[I18n.t("graphs.colors.Exams", {locale: lang})]= "#A1887F";
    colors[I18n.t("graphs.colors.Supplements", {locale: lang})]= "#CADCAD";
    colors[I18n.t("graphs.colors.Infos", {locale: lang})]= "#FFD54F";
    colors[I18n.t("graphs.colors.Helps", {locale: lang})]= "#1273E1";
    colors[I18n.t("graphs.colors.MeanTime", {locale: lang})]= "#01579B";
    colors[I18n.t("graphs.colors.TotalTime", {locale: lang})]= "#0288D1";
    colors[I18n.t("graphs.colors.Goal", {locale: lang})] = "#FF8A65";
    colors[I18n.t("graphs.colors.Effective time", {locale: lang})]= "#4FC3F7";
  });

  colorPalette = colors;

  notStartedStrings     = _.map(availableLanguages, function(lang) { return I18n.t("graphs.resourceStatus.Not started", {locale: lang}).toUpperCase(); });
  completedStrings      = _.map(availableLanguages, function(lang) { return I18n.t("graphs.resourceStatus.Completed"  , {locale: lang}).toUpperCase(); });
  effectiveTimeStrings  = _.map(availableLanguages, function(lang) { return I18n.t("legends.effective_time"  ,          {locale: lang}).toUpperCase(); });
});





//sessionsScale: ["#FFF3E0"; "#FFE0B2"; "#FFCC80"; "#FFB74D"; "#FFA726"; "#FF9800"; "#FB8C00"; "#F57C00"; "#EF6C00"; "#E65100"];
// ['#E1F5FE';'#B3E5FC'; '#81D4FA'; '#4FC3F7'; '#29B6F6';'#03A9F4'; '#039BE5'; '#0288D1'; '#0277BD'; '#01579B'];
