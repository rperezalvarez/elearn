
/* #####################################################
  #### ENGAGEMENT AND PERFORMANCE REDUCE FUNCTIONS ####
  ##################################################### */


// #####################################################
function reduceTime_notComparation(p, v) {
  if(!p.is_comparation) {
    return p.time_invested;
  }
  return 0;
}
// #####################################################
function reduceAddCountActivities_notComparation(v, p) {
  if(!p.is_comparation && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v + 1;
  }
  return v;
}

function reduceRemoveCountActivities_notComparation(v, p) {
  if(!p.is_comparation && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v - 1;
  }
  return v;
}

function reduceInitCountActivities_notComparation() {
  return 0;
}
// #####################################################
function reduceAddCountActivities_comparation_completers(v, p) {
  if(p.is_comparation && p.is_completer && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v + 1;
  }
  return v;
}

function reduceRemoveCountActivities_comparation_completers(v, p) {
  if(p.is_comparation && p.is_completer && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v - 1;
  }
  return v;
}

function reduceInitCountActivities_comparation_completers() {
  return 0;
}
// #####################################################
function reduceAddCountActivities_comparation_noCompleters(v, p) {
  if(p.is_comparation && !p.is_completer && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v + 1;
  }
  return v;
}

function reduceRemoveCountActivities_comparation_noCompleters(v, p) {
  if(p.is_comparation && !p.is_completer && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v - 1;
  }
  return v;
}

function reduceInitCountActivities_comparation_noCompleters() {
  return 0;
}
// #####################################################
function reduceAddTime_notComparation(v, p) {
  if(!p.is_comparation && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v + p.time_invested;
  }
  return v;
}

function reduceRemoveTime_notComparation(v, p) {
  if(!p.is_comparation && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v - p.time_invested;
  }
  return v;
}

function reduceInitTime_notComparation() {
  return 0;
}
// #####################################################
function reduceAddTime_comparation_completers(v, p) {
  if(p.is_comparation && p.is_completer && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v + p.time_invested;
  }
  return v;
}

function reduceRemoveTime_comparation_completers(v, p) {
  if(p.is_comparation && p.is_completer && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v - p.time_invested;
  }
  return v;
}

function reduceInitTime_comparation_completers() {
  return 0;
}
// #####################################################
function reduceAddTime_comparation_noCompleters(v, p) {
  if(p.is_comparation && !p.is_completer && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v + p.time_invested;
  }
  return v;
}

function reduceRemoveTime_comparation_noCompleters(v, p) {
  if(p.is_comparation && !p.is_completer && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v - p.time_invested;
  }
  return v;
}

function reduceInitTime_comparation_noCompleters() {
  return 0;
}
// #####################################################
function reduceAddCountActivities_notComparation(v, p) {
  if(!p.is_comparation && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v + 1;
  }
  return v;
}

function reduceRemoveCountActivities_notComparation(v, p) {
  if(!p.is_comparation && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v - 1;
  }
  return v;
}

function reduceInitCountActivities_notComparation() {
  return 0;
}
// #####################################################
function reduceAddTime_notComparation(v, p) {
  if(!p.is_comparation && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v + p.time_invested;
  }
  return v;
}

function reduceRemoveTime_notComparation(v, p) {
  if(!p.is_comparation && notStartedStrings.indexOf(p.status.toString().toUpperCase()) < 0) {
    return v - p.time_invested;
  }
  return v;
}

function reduceInitTime_notComparation() {
  return 0;
}
// #####################################################


/* #####################################################
  ### EFFICIENCY AND EFFECTIVENESS REDUCE FUNCTIONS ###
  ##################################################### */

// #####################################################
function reduceAddCountStartedCompleted(v,p) {
  if(completedStrings.indexOf(p.status.toUpperCase()) >= 0) {
    ++v.completed;
  }
  ++v.started;
  return v;
}

function reduceRemoveCountStartedCompleted(v,p) {
  if(completedStrings.indexOf(p.status.toUpperCase()) >= 0) {
    --v.completed;
  }
  --v.started;
  return v;
}

function reduceInitCountStartedCompleted() {
  return {completed: 0, started: 0};
}
// #####################################################
function reduceAddCountStartedCompleted(v,p) {
  if(completedStrings.indexOf(p.status.toUpperCase()) >= 0) {
    ++v.completed;
  }
  ++v.started;
  return v;
}

function reduceRemoveCountStartedCompleted(v,p) {
  if(completedStrings.indexOf(p.status.toUpperCase()) >= 0) {
    --v.completed;
  }
  --v.started;
  return v;
}

function reduceInitCountStartedCompleted() {
  return {completed: 0, started: 0};
}
// #####################################################

