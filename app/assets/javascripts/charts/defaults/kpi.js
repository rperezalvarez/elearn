d3.cloudshapes.kpiChart = function module() {

  var margin = { top: 0, right: 0, bottom: 20, left: 0 };

  var kpiTypes = { time: "Time", number: "Number" };
  var kpiType = kpiTypes.number;

  var oldData = 0;
  var data = 10;
  var dataUnit = "";
  var updateData;

  var title = "";

  var width = 300;
  var height = 200;
  var backgroundColor = "#ccc";
  var color = "#fff";
  var ease = "exp";
  var selection;

  var svg;
  var kpi;

  function exports(_selection) {
    _selection.each(function() {
      // If no SVG exists, create one - and add key groups:
      if (!svg) {
        svg = d3.select(this)
        .append("svg")
        .classed("elearn-kpi", true)
        .attr("width", width)
        .attr("height", height);
      }

      var chartH = height - margin.top - margin.bottom;
      var chartW = width - margin.left - margin.right;

      svg.append("rect")
      .attr({
        x: margin.left,
        y: 0,
        height: height,
        width: width
      })
      .style("fill", backgroundColor);

      svg.append("text")
      .attr("x", chartW/2 + margin.left )
      .attr("y", chartH/2 + margin.top)
      .attr("class", "kpi-title")
      .attr("text-anchor", "middle")
      .attr("dominant-baseline", "text-after-edge")
      .style("fill", color)
      .text(title);

      kpi = svg.selectAll(".kpi")
      .data([data])
      .enter()
      .append("text")
      .attr("x", chartW/2 + margin.left)
      .attr("y", chartH/2 + margin.top)
      .attr("class", "kpi")
      .attr("text-anchor", "middle")
      .attr("dominant-baseline", "text-before-edge")
      .style("fill", color)
      .text(function(d) { return d.toFixed(1) + " " + dataUnit; });

      updateData = function() {
        kpi.data([data])
        .transition()
        .ease(ease)
        .duration(1000)
        .tween("text", function(d) {
          var trans = d3.interpolate(this.textContent.replace(dataUnit, ""), d);
          var prec = (d.toFixed(1) + "").split(".");
          var round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
          return function(t) { this.textContent = parseFloat(Math.round(trans(t) * round) / round).toFixed(1) + " " + dataUnit ; };
        });
      }
    });
  }

  exports.kpiType = function(_x) {
    if (!arguments.length) return kpiType;
    kpiType = _x;
    return this;
  };

  exports.data = function(_x) {
    if (!arguments.length) return data;
    data = _x;
    if (typeof updateData === 'function') updateData();
    return this;
  };

  exports.dataUnit = function(_x) {
    if (!arguments.length) return dataUnit;
    dataUnit = _x;
    return this;
  };

  exports.width = function(_x) {
    if (!arguments.length) return width;
    width = parseInt(_x);
    return this;
  };

  exports.height = function(_x) {
    if (!arguments.length) return height;
    height = parseInt(_x);
    return this;
  };

  exports.backgroundColor = function(_x) {
    if (!arguments.length) return backgroundColor;
    backgroundColor = _x;
    return this;
  };

  exports.color = function(_x) {
    if (!arguments.length) return color;
    color = _x;
    return this;
  };

  exports.title = function(_x) {
    if (!arguments.length) return title;
    title = _x;
    return this;
  };

  exports.ease = function(_x) {
    if (!arguments.length) return ease;
    ease = _x;
    return this;
  };

  exports.selection = function(_x){
    if (!arguments.length) return selection;
    selection = _x;
    return this;
  };

  return exports;

}
