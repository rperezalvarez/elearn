// Bar chart Module
/////////////////////////////////

// Declare namespace

// Declare component: (this outer function acts as the closure):
d3.cloudshapes.groupedBarChart = function module() {
  var margin = {top: 30, right: 20, bottom: 40, left: 40},
  width = 500,
  height = 500,
  gap = 0,
  ease = "exp";

  var barsColor = "orange";
  var lineMeanColor = 'blue';
  var lineTotalColor = 'green';

  var analyticsClass = '';

  var data = [];

  var dataLineTotal = [0, 0, 0, 0, 0, 0, 0];
  var dataLineAvg = [0, 0, 0, 0, 0, 0, 0];

  var labels = [];
  var dataTitle = "";
  var dataUnit = "";
  var onClickFunction = function(){};
  var svg;

  var selectable = true;
  var showAxisY = true;
  var labelSize = 12;
  var labelRotation = 0;
  var orderData = [];

  var legendX = 10;
  var legendY = 20;
  var gapBar = 0;
  var axisYTitle = "";

  var labelsShort = [];

  var updateData;

  var selection;
  var tip = d3.select("body").append("div").attr("class", "toolTipRowChart");


  function wrap() {
    textWidth = width-legendX-20;
    var self = d3.select(this),
    textLength = self.node().getComputedTextLength(),
    text = self.text();
    while (textLength > (textWidth - 2 * 0) && text.length > 0) {
      text = text.slice(0, -1);
      self.text(text + '...');
      textLength = self.node().getComputedTextLength();
    }
  }

  // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
  // the above variables.
  function exports(_selection) {
    _selection.each(function() {
      _data = data;

      if (orderData.length != 0){
        _data.forEach( (d,k) => {

          var data_ordened = [];
          for (var i = 0; i< orderData.length; i++) {
            for (var j = 0; j< d.length; j++){
              if (labels[j] == orderData[i]){
                data_ordened[i] = d[j];
              }
            }
          }
          _data[k] = data_ordened;
        })
        if (labelsShort.length == 0){
          labels = orderData;
        } else{
          labels = labelsShort;
        }
      }

      var chartW = width - margin.left - margin.right,
      chartH = height - margin.top - margin.bottom;

      // Define x and y scale variables.
      var x1 = d3.scale.ordinal()
      .domain(labels.map(function(d) { return d; }))
      .rangeRoundBands([0, chartW], 0.2);

      var y1 = d3.scale.linear()
      .domain([0, d3.max([..._.flatten(_data), ...dataLineTotal, ...dataLineAvg], function(d, i) { return d*1.1; })]).nice(10)
      .range([chartH, 0]);

      var xAxis = d3.svg.axis()
      .scale(x1)
      .orient("bottom")
      .innerTickSize(0)
      .outerTickSize(10)
      .tickPadding(10)
      .tickFormat(function(d){return d.toLocaleString();});

      // If no SVG exists, create one - and add key groups:

      if (!svg) {
        svg = d3.select(this)
        .append("svg")
        .classed("elearn-groupedbarchart", true);
        var container = svg.append("g").classed("container-group", true);

        var legendContainer  = svg.append("g")
        .classed("legend-column", true);
      }
      // AXIS DEFINITION
      if(showAxisY){
        var yAxis = d3.svg.axis()
        .scale(y1)
        .orient("left")
        .innerTickSize(-chartW)
        .ticks(5)
        .outerTickSize(20)
        .tickPadding(40)
        .tickFormat(function (d){ return ""});

        var yAxis1 = d3.svg.axis()
        .scale(y1)
        .orient('left')
        .ticks(5)
        .outerTickSize(0)
        .tickPadding(20)
        .tickSize(20)
        .tickFormat(function(d,i){
          return d.toLocaleString();
        });

        var yAxis2 = d3.svg.axis()
        .scale(y1)
        .orient("left")
        .innerTickSize(0)
        .ticks(3)
        .tickSize(60)
        .tickPadding(60)
        .tickFormat(function (d){ return ""});

        // AXIS CALL
        container.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate("+ 0 +"," + 0 + ")")
        .call(yAxis);

        container.append("g")
        .attr("class", "y axis1")
        .attr("transform", "translate("+ 0 +"," + 0 + ")")
        .call(yAxis1);
        //
        // container.append("g")
        // .attr("class", "y axis2")
        // .call(yAxis2);
      }

      container.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + chartH + ")")
      .call(xAxis);


      // Transition the width and height of the main SVG and the key 'g' group:
      svg.transition().attr({width: width, height: height});
      svg.select(".container-group")
      .attr({transform: "translate(" + margin.left + "," + margin.top + ")"});

      // Define gap between bars:
      var groupGapSize = x1.rangeBand() / 100 * gap;
      var barGapSize = 0;
      var barW = (x1.rangeBand() - barGapSize)/_data.length;

      container.select('.x').selectAll("text")
               .style("text-anchor", function() {
                 if (labelRotation == 0) {
                   return "middle"
                 } else {
                   return "end"
                 }
               })
               .style("font-size", labelSize)
               .attr("transform", function(d) {
                 return "translate(0,0), rotate(" + labelRotation + ")"
               });

      // Select all bars and bind data:
      var groups = container.selectAll(".group-bars")
                            .data(_data)
                            .enter().append("g")
                            .attr('class', 'group-bars')
                            .attr("name-label",function(d,i){ return legendTitle[i]})
                            .style("fill", barsColor )
                            .attr("transform", function(d,i) {
                              return "translate(" + i*(gapBar+barW) + "," + 0 + ")";
                            });

      var bars = groups.selectAll(".bar")
                       .data(function(d,i) {return d; });


      // ENTER, UPDATE and EXIT CODE:
      // D3 ENTER code for bars!
      bars.enter().append("rect")
        .attr("class", function(d,i,i2) {
          if(selection) {
            if(selection.label == labels[i] && selection.subIndex == i2) {
              return "bar _selected_";
            } else {
              return "bar _notSelected_";
            }
          } else {
            return "bar";
          }
        })
        .classed(analyticsClass, true)
        .attr({
          width: barW,
          x: function(d, i) { return x1(labels[i]) + barGapSize / 2; },
          y: function(d, i) { return y1(d); },
          height: function(d, i) {
            return chartH - y1(d); }
          })
        .on('mouseover', function(d,i){
          tip.style("left", d3.event.pageX+10+"px");
          tip.style("top", d3.event.pageY-25+"px");
          tip.style("display", "inline-block");
          tip.html("<b>"+labels[i]+"</b><br>"+d.toFixed(1).toLocaleString()+" "+ dataUnit.toLocaleString())
        })
        .on("mousemove", function(){
          tip.style("left", d3.event.pageX+10+"px");
          tip.style("top", d3.event.pageY-25+"px");
          tip.style("display", "inline-block");
        })
        .on('mouseout', function(d,i){
          tip.style("display", "none");
        })
        .on("click", function(d,i,sub_i){
          if (selectable) {
            selection = {label: labels[i], subIndex: sub_i};
            bars.classed('_selected_', false);
            bars.classed('_notSelected_', true);
            d3.select(this).classed('_notSelected_', false);
            d3.select(this).classed('_selected_', true);

            onClickFunction(this, d, labels[i], sub_i);
          }
        });

          if (legendTitle.length >0 ){
            createLegend();
          }

          // D3 EXIT code for bars
          bars.exit().transition().style({opacity: 0}).remove();

      // COMPARISION LINES
      if(0 < _.max(dataLineTotal)) {
        var lineTotalTime = d3.svg.line()
            .x(function(d, i) { return x1(labels[i]); })
            .y(function(d, i) { return y1(d); })
            .interpolate("cardinal");
  
        container.append("path")
        .attr("class", "line total-time")
        .attr("d", lineTotalTime(dataLineTotal))
        .attr("transform", "translate("+ data.length/2 * (barW + gapBar)  +"," + 0 + ")")
        .style({"stroke": lineTotalColor, "stroke-width":"2px", "fill":"none"})
        .on("mouseover", function() {
          d3.select(this).style("stroke-width", "5px");
          tip.style("left", d3.event.pageX+10+"px");
          tip.style("top", d3.event.pageY-25+"px");
          tip.style("display", "inline-block");
          tip.html("<b>Average time per day</b><br/>" + dataLineTotal[0].toFixed(1) + " min");
        })
        .on("mousemove", function(){
          tip.style("left", d3.event.pageX+10+"px");
          tip.style("top", d3.event.pageY-25+"px");
          tip.style("display", "inline-block");
        })
        .on("mouseout", function() {
          d3.select(this).style("stroke-width", "3px");
          tip.style("display", "none");
        });
      }

      if(0 < _.max(dataLineAvg)) {
        var lineMeanTime = d3.svg.line()
            .x(function(d, i) {return x1(labels[i]);})
            .y(function(d, i) {return y1(d);})
            .interpolate("cardinal");
  
        container.append("path")
        .attr("class", "line mean time")
        .attr("d", lineMeanTime(dataLineAvg))
        .attr("transform", "translate("+ data.length/2 * (barW + gapBar) +"," + 0 + ")")
        .style({"stroke": lineMeanColor, "stroke-width":"3px", "fill":"none"})
        .on("mouseover", function() {
          d3.select(this).style("stroke-width", "5px");
          tip.style("left", d3.event.pageX+10+"px");
          tip.style("top", d3.event.pageY-25+"px");
          tip.style("display", "inline-block");
          tip.html("<b>" + I18n.t("graphs.tooltips.week_detail_line.text") + "</b><br/>" + dataLineAvg[0].toFixed(1) + " min");
        })
        .on("mousemove", function(){
          tip.style("left", d3.event.pageX+10+"px");
          tip.style("top", d3.event.pageY-25+"px");
          tip.style("display", "inline-block");
        })
        .on("mouseout", function() {
          d3.select(this).style("stroke-width", "3px");
          tip.style("display", "none");
        });
      }

      // Put axis Y title
      container.append("text")
      .text(axisYTitle)
      .classed("axis-title", true)
      .attr("x", -chartH/2)
      .attr("y", -margin.left)
      .attr("text-anchor", "middle")
      .attr("dominant-baseline", "ideographic")
      .attr("transform", "rotate(-90)")
      .attr("dy", "1em");


      function createLegend(){
        items = legendContainer.selectAll(".legend-item")
        .data(legendTitle);

        items.enter().append("g")
        .classed("legend-item", true)
        .classed("_selected_", true)
        .attr({transform: "translate(" + (chartW-legendX) + "," + (legendY) + ")"})


        items.append("text").append('tspan').text(function(d, i) {
          return d.toLocaleString();
        })
        .attr("x", function(d) {
          return 10;
        })
        .attr("y", function(d,i) {
          return 20*i;
        })
        .attr('dx', '20').each(wrap);
        // .append('tspan').classed("data-values", true).text(function(d, i) {
        //     return "(" +d.toLocaleString() + ")";
        // });

        items.append("rect")
        .attr("x", function(d) {
          return 10;
        })
        .attr("y", function(d,i) {
          return 20*i-10;
        })
        .attr('width', '10')
        .attr('height', '10')
        .attr("fill", barsColor);
      }

      updateData = function() {
        _data = data;

        if (orderData.length != 0){
          _data.forEach( (d,k) => {

            var data_ordened = [];
            for (var i = 0; i< orderData.length; i++) {
              for (var j = 0; j< d.length; j++){
                if (labels[j] == orderData[i]){
                  data_ordened[i] = d[j];
                }
              }
            }
            _data[k] = data_ordened;
          })
          if (labelsShort.length == 0){
            labels = orderData;
          } else{
            labels = labelsShort;
          }

        }

        y1 = d3.scale.linear()
        .domain([0, d3.max([_.flatten(_data), ...dataLineTotal, ...dataLineAvg], function(d, i) { return d*1.1; })]).nice(10)
        .range([chartH, 0]);

        if(showAxisY){
          yAxis = d3.svg.axis()
          .scale(y1)
          .orient("left")
          .innerTickSize(-chartW)
          .ticks(3)
          .outerTickSize(20)
          .tickPadding(40)
          .tickFormat(function (d){ return ""});

          yAxis1 = d3.svg.axis()
          .scale(y1)
          .orient('left')
          .ticks(3)
          .outerTickSize(0)
          .tickPadding(20)
          .tickSize(20)
          .tickFormat(function(d,i){
            return d.toLocaleString();
          });

          yAxis2 = d3.svg.axis()
          .scale(y1)
          .orient("left")
          .innerTickSize(0)
          .ticks(3)
          .tickSize(60)
          .tickPadding(60)
          .tickFormat(function (d){ return ""});

          container.selectAll("g .y.axis").call(yAxis);
          container.selectAll("g .y.axis1").call(yAxis1);
          container.selectAll("g .y.axis2").call(yAxis2);
        }

        groupGapSize = x1.rangeBand() / 100 * gap;
        barGapSize = 0;
        barW = (x1.rangeBand() - barGapSize)/_data.length;

        groups = container.selectAll(".group-bars").data(_data);


        //
        //ACTUALMENTE NO PERMITE AÑADIR NUEVOS GRUPOS DINAMICAMENTE
        bars = bars.data(function(d,i){return _data[i]});


        bars.enter().append("rect")
        .attr("class", "bar")
        .attr("y", y1(0))
        .attr("height", height - y1(0));


        // D3 UPDATE code for bars
        bars.transition().ease(ease).duration(1000)
        .attr({
          width: barW,
          x: function(d, i) { return x1(labels[i]) + barGapSize / 2; },
          y: function(d, i) { return y1(d); },
          height: function(d, i) { return chartH - y1(d); }
        });

        bars.exit()
        .transition()
        .duration(300)
        .attr("y", y1(0))
        .attr("height", chartH - y1(0))
        .style('fill-opacity', 1e-6)
        .remove();

        if(!selection) {
          bars.classed("_selected_", false);
          bars.classed("_notSelected_", false);
        }

        // D3 EXIT code for bars
        bars.exit().transition().style({opacity: 0}).remove();

        groups.transition().ease(ease).duration(1000).style('fill', barsColor )

        items = legendContainer.selectAll("rect")
        items.transition().ease(ease).duration(1000).style('fill', barsColor )


        // // COMPARISION LINES
        // if(lineTotalTime) {
        //   lineTotalTime
        //   .x(function(d, i) { console.log(labels); return x1(labels[i]); })
        //   .y(function(d, i) { return y1(d); });
        // }

        // container.selectAll(".total-time")
        // .attr("d", lineTotalTime(dataLineTotal));

      }

      infoTooltip = function() {
        var tip = d3.select("body").append("div").attr("class", "toolTipRowChart");

      }
      infoTooltip();

        });
      }

      exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
      };

      exports.onClickFunction = function(value) {
        if (!arguments.length) return onClickFunction;
        onClickFunction = value;
        return this;
      };

      exports.selectable = function(_x){
        if (!arguments.length) return selectable;
        selectable = _x;
        return this;
      }
      exports.labels = function(_x) {
        if (!arguments.length) return labels;
        labels = _x;
        return this;
      };
      exports.orderData = function(_x) {
        if (!arguments.length) return orderData;
        orderData = _x;
        return this;
      };

      exports.showAxisY = function(_x){
        if (!arguments.length) return showAxisY;
        showAxisY = _x;
        return this;
      }
      exports.legendTitle = function(_x){
        if (!arguments.length) return legendTitle;
        legendTitle = _x;
        return this;
      }

      exports.labelRotation = function(_x){
        if (!arguments.length) return labelRotation;
        labelRotation = _x
        return this;
      }

      exports.labelSize = function(_x){
        if (!arguments.length) return labelSize;
        labelSize = _x;
        return this
      }
      exports.legendX = function(_x){
        if (!arguments.length) return legendX;
        legendX = _x;
        return this
      }

      exports.barsColor = function(_x){
        if(!arguments.length) return barsColor;
        barsColor = _x;
        // if (typeof updateData === 'function') updateData();
        return this;
      }


      exports.dataTitle = function(_x) {
        if (!arguments.length) return dataTitle;
        dataTitle = _x;
        return this;
      };

      exports.dataUnit = function(_x) {
        if (!arguments.length) return dataUnit;
        dataUnit = _x;
        return this;
      };


      exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
      };
      exports.gapBar = function(_x) {
        if (!arguments.length) return gapBar;
        gapBar = _x;
        return this;
      };

      exports.labelsShort = function(_x){
        if (!arguments.length) return labelsShort;
        labelsShort = _x;
        return this;
      }

      // GETTERS AND SETTERS:
      exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        return this;
      };

      exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
      };
      exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
      };
      exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
      };

      exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
      };

      exports.selection = function(_x){
        if (!arguments.length) return selection;
        selection = _x;
        return this;
      };

      exports.dataLineAvg = function(_x){
        if (!arguments.length) return dataLineAvg;
        dataLineAvg = _x;
        return this;
      };

      exports.dataLineTotal = function(_x){
        if (!arguments.length) return dataLineTotal;
        dataLineTotal = _x;
        return this;
      };

      exports.lineTotalColor = function(_x){
        if (!arguments.length) return lineTotalColor;
        lineTotalColor = _x;
        return this;
      };

      exports.lineMeanColor = function(_x){
        if (!arguments.length) return lineMeanColor;
        lineMeanColor = _x;
        return this;
      };

      exports.axisYTitle = function(_x){
        if (!arguments.length) return axisYTitle;
        axisYTitle = _x;
        return this;
      };

      exports.analyticsClass = function(_x){
        if (!arguments.length) return analyticsClass;
        analyticsClass = _x;
        return this;
      };

      return exports;
    };
