d3.cloudshapes.horizontalRowChart = function module() {
  var margin = {top: 30, right: 20, bottom: 20, left: 80};
  var width = 450;
  var height = 200;
  var margin_bars = 0.08;
  var data = [];
  var dataUnit = "";
  var seriesNames = [];
  var max_value;
  var colors = { internal:'#4169e1',external:'#87cefa' }

  var showLegend = true;
  var legendX = 10;
  var legendY = 0;
  var axisXTitle = "";
  var analyticsClass = "";

  var gapExternalBars = 0.5;
  var gapInternalBars = 0.3;

  // h_ext_bar = (chartH/data.length)*0.35;
  // h_int_bar = h_ext_bar*0.5;

  var ease = "exp";
  var svg;
  var updateData = null;
  var selectable = true;
  var selection;

  var onClickFunction;

  //Returns a functions of given index , return a color ?
  //var colors = d3.scale.category10();

  //Returns arrays of colors?
  //var colors = d3.scale.category10().range();

  function exports(_selection) {
    _selection.each(function() {

      var chartW = width - margin.left - margin.right,
          chartH = height - margin.top - margin.bottom;

      // If no SVG exists, create one - and add key groups:
      if (!svg) {
        svg = d3.select(this)
        .append("svg")
        .attr("width", width)
        .attr("height", height);

        var container = svg.append("g")
        .classed("container-group", true)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var legendContainer = svg.append("g")
            .classed("legend-group", true);

        max_value = d3.max(data, function(d){ return d3.max(d.values); });
      }

      //Tooltip
      var tip = d3.select("body").append("div").attr("class", "toolTipRowChart");

      var x0 = d3.scale.linear()
      .domain([0, max_value])
      .range([0, chartW]);

      var y0 = d3.scale.ordinal()
      .domain(data.map(function(d) {
        return d.label;
      }))
      .rangeRoundBands([0, chartH], gapExternalBars);

      var xAxis = d3.svg.axis()
      .scale(x0)
      .tickSize(-chartH)
      .orient("bottom")
      .ticks(d3.min([max_value, 5]));

      var yAxis = d3.svg.axis()
      .scale(y0)
      .orient("left");


      var extBarH = y0.rangeBand() - gapExternalBars;
      var intBarH = extBarH * (1 - gapInternalBars * 2);

      var externalBars = container.selectAll(".external-bar")
      .data(data)
      .enter()
      .append("rect")
      .classed("external-bar", true)
      .classed(analyticsClass, true)
      .attr({
        x: function(d) { return x0(0); },
        y: function(d) { return y0(d.label); },
        height: extBarH,
        width: function(d) { return x0(d.values[1]); },
        fill: colors.external
      })
      .on("mousemove", function(d){
        tip.style("left", d3.event.pageX+10+"px");
        tip.style("top", d3.event.pageY-25+"px");
        tip.style("display", "inline-block");
        tip.html("<b>" + (seriesNames[1]) + "</b>: "+(round(d.values[1], 1).toFixed(1)) + " " + dataUnit);
      })
      .on("mouseout", function(d){
        tip.style("display", "none");
      })
      .on("click", function(d, i) {
        if(selectable) {
          svg.selectAll("rect").classed("_selected_", false);
          svg.selectAll("rect").classed("_notSelected_", true);

          d3.select(this).classed("_selected_", true);
          d3.select(this).classed("_notSelected_", false);

          selection = {label: d.label, subCategory: seriesNames[1]};
          if(onClickFunction) { onClickFunction(this, d.label, seriesNames[1]); }
        }
      });

      var internalBars = container.selectAll(".internal-bar")
      .data(data)
      .enter()
      .append("rect")
      .classed("internal-bar", true)
      .classed(analyticsClass, true)
      .attr({
        x: function(d) { return x0(0); },
        y: function(d) { return y0(d.label) + (extBarH - intBarH)/2; },
        height: intBarH,
        width: function(d) { return x0(d.values[0]); },
        fill: colors.internal
      })
      .on("mousemove", function(d){
        tip.style("left", d3.event.pageX+10+"px");
        tip.style("top", d3.event.pageY-25+"px");
        tip.style("display", "inline-block");
        tip.html("<b>" + (seriesNames[0]) + "</b>: "+ (round(d.values[0], 1).toFixed(1)) + " " + dataUnit);
      })
      .on("mouseout", function(d){
        tip.style("display", "none");
      })
      .on("click", function(d, i) {
        if(selectable) {
          svg.selectAll("rect").classed("_selected_", false);
          svg.selectAll("rect").classed("_notSelected_", true);

          d3.select(this).classed("_selected_", true);
          d3.select(this).classed("_notSelected_", false);

          selection = {label: d.label, subCategory: seriesNames[0]};
          if(onClickFunction) { onClickFunction(this, d.label, seriesNames[0]); }
        }
      });

      container.insert("g",":first-child")
      .attr("class", "axisVertical")
      .call(yAxis);

      container.selectAll(".tick text")
      .classed("label", true);

      container.insert("g",":first-child")
      .attr("class", "axisHorizontal")
      .attr("transform", "translate(" + 0 + ","+ (chartH)+")")
      .call(xAxis);

      // Append x axis title
      container.append("text")
      .text(axisXTitle)
      .classed("axis-title", true)
      .attr("x", chartW)
      .attr("y", 0)
      .attr("text-anchor", "end")
      .attr("dominant-baseline", "text-after-edge");


      updateData = function() {

        externalBars
        .data(data)
        .transition()
        .ease(ease)
        .duration(1000)
        .attr({
          width: function(d){ return x0(d.values[1]); }
        });

        internalBars
        .data(data)
        .transition()
        .ease(ease)
        .duration(1000)
        .attr({
          width: function(d){ return x0(d.values[0]); }
        });
      }
      if (showLegend) {createLegend()};

      function createLegend() {
          items = legendContainer.selectAll(".legend-item")
              .data(seriesNames);
          items.enter().append("g")
              .classed("legend-item", true)
              .classed("_selected_", true)
              .attr({ transform: "translate(" + (legendX) + "," + (legendY) + ")" })

          items.append("text").append('tspan').text(function(d, i) {
              return seriesNames[i]
              })
              .attr("x", function(d, i) {
                  return 90 * i + 15;
              })
              .attr("y", function(d, i) {
                  return 10;
              })

          items.append("rect")
              .attr("x", function(d, i) {
                  return 90 * i ;
              })
              .attr("y", function(d, i) {
                  return 0;
              })
              .attr('width', '10')
              .attr('height', '10')
              .attr("fill", function(d, i) {
                  return (i == 0 ? colors.internal : colors.external);
              });
            };
        });
  }

  exports.width = function(_x) {
    if (!arguments.length) return width;
    width = parseInt(_x);
    return this;
  };

  exports.height = function(_x) {
    if (!arguments.length) return height;
    height = parseInt(_x);
    return this;
  };


  exports.data = function(_x) {
    if (!arguments.length) return data;
    data = _x;

    //Understand this function
    if (typeof updateData === 'function') updateData();

    //Return myself as object ?
    return this;
  };

  exports.max_value = function(){
    //There is another way to define a internal variable based on others variables??
    if(max_value){
      max_value = d3.max(this.data, function(d){ return d.values[1]; });
    }

    return max_value;
  };

  exports.colors = function(_x){
    if(!arguments.length) return colors;
    colors = _x;
    return this;
  };

  exports.gapExternalBars = function(_x){
    if(!arguments.length) return gapExternalBars;
    gapExternalBars = _x;
    return this;
  };

  exports.gapInternalBars = function(_x){
    if(!arguments.length) return gapInternalBars;
    gapInternalBars = _x;
    return this;
  };

  exports.margin = function(_x) {
    if (!arguments.length) return margin;
    margin = _x;
    return this;
  };

  exports.onClickFunction = function(_x) {
    if (!arguments.length) return onClickFunction;
    onClickFunction = _x;
    return this;
  };

  exports.seriesNames = function(_x) {
    if (!arguments.length) return seriesNames;
    seriesNames = _x;
    return this;
  };

  exports.selectable = function(_x) {
    if (!arguments.length) return selectable;
    selectable = _x;
    return this;
  };

  exports.dataUnit = function(_x) {
    if (!arguments.length) return dataUnit;
    dataUnit = _x;
    return this;
  };

  exports.selection = function(_x){
    if (!arguments.length) return selection;
    selection = _x;
    if(svg) {
      svg.selectAll('rect._selected_').classed('_selected_', false);
      svg.selectAll('rect._notSelected_').classed('_notSelected_', false);
    }
    return this;
  };

  exports.axisXTitle = function(_x) {
    if (!arguments.length) return axisXTitle;
    axisXTitle = _x;
    return this;
  };

  exports.analyticsClass = function(_x){
    if (!arguments.length) return analyticsClass;
    analyticsClass = _x;
    return this;
  };

  return exports;

};
