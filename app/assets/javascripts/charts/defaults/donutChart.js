d3.cloudshapes.donutChartElearn = function module() {
    var margin = { top: 20, right: 20, bottom: 40, left: 20 },
        width = 500,
        height = 500,
        gap = 0,
        color = "#001ff0"
    ease = "exp";
    var padding = { top: 0, right: 0, bottom: 0, left: 0 };
    var outRadius = 0;
    var innRadius = 0;
    var alpha = 0.5;
    var spacing = 12;
    var selectable = true;
    var selection;
    var data = [];
    var labels = [];
    var dataTitle = "";
    var dataUnit = "";
    var showLegend = true;
    var showLabel = false;
    var legendX = 10;
    var legendY = 10;
    var onClickFunction = function() {};
    var svg;
    var colors = d3.scale.category10().range();
    var analyticsClass = '';

    var tip = d3.select("body").append("div").attr("class", "toolTipRowChart");

    function wrap() {
        textWidth = width - legendX - 20;
        var self = d3.select(this),
            textLength = self.node().getComputedTextLength(),
            text = self.text();
        while (textLength > (textWidth - 2 * 0) && text.length > 0) {
            text = text.slice(0, -1);
            self.text(text + '...');
            textLength = self.node().getComputedTextLength();
        }
    }

    var updateData;
    var updateWidth;
    var clearSelection;

    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            var _data = data;

            var colorScale = d3.scale.ordinal()
                .domain(labels)
                .range(colors);


            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

            // Define x and y scale variables.
            var x1 = d3.scale.ordinal()
                .domain(labels.map(function(d, i) { return i; }))
                .rangeRoundBands([0, chartW], 0.1);

            var y1 = d3.scale.linear()
                .domain([0, d3.max(_data, function(d, i) { return d; })])
                .range([chartH, 0]);

            // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("elearn-donutchart", true);
                var container = svg.append("g")
                    .classed("container-group", true);
                var pie_container = container.append("g")
                    .classed("donut-chart-group", true);
                var labelsContainer = container.append("g")
                    .classed("labels-group", true);
                var legendContainer = svg.append("g")
                    .classed("legend-group", true);

            }

            // Transition the width and height of the main SVG and the key 'g' group:
            svg.attr({ width: width, height: height });
            svg.select(".container-group")
                .attr({ transform: "translate(" + (margin.left + (chartW / 2)) + "," + (margin.top + (chartH / 2)) + ")" });

            var pie = d3.layout.pie()
                .value(function(d) { return d; })
                .sort(null);

            var arc = d3.svg.arc()
                .innerRadius(outRadius)
                .outerRadius(innRadius);

            var arcs = pie_container.selectAll(".donut-arc")
                .data(pie(_data));

            arcs.enter().append("g")
                .classed("donut-arc", true)
                .classed(analyticsClass, true);




            var paths = arcs.append("path")
                .attr("fill", function(d, i) {
                    return colorScale(i);
                })
                .attr("d", arc)
                .each(function(d) { this._current = d; })
                .on('mousemove', function(d, i) {
                    var temp = labels[i].split(" ")[0]                    
                    tip.style("left", d3.event.pageX+10+"px");
                    tip.style("top", d3.event.pageY-25+"px");
                    tip.style("display", "inline-block");
                    tip.html("<b>" + labels[i] + "</b><br>"+(d.value) + " " + dataUnit);
                })
                .on('mouseout', function(d, i) {
                    tip.style("display", "none");
                })
                .on("click", function(d, index) {
                    if (selectable) {
                        svg.selectAll(".donut-arc")
                          .classed("_selected_", function(d, i) { return i === index; })
                          .classed("_notSelected_", function(d, i) { return i != index; });
                        svg.selectAll(".labels")
                          .classed("_selected_", function(d, i) { return i === index; })
                          .classed("_notSelected_", function(d, i) { return i != index; });
                        svg.selectAll(".legend-item")
                          .classed("_selected_", function(d, i) { return i === index; })
                          .classed("_notSelected_", function(d, i) { return i != index; });

                        selection = {label: labels[index]};
                        onClickFunction(this, d, labels[index]);
                    }
                });

            var gLabels = [];
            var items = [];

            if (showLegend == true) {
                createLegend();
            }
            if (showLabel == true) {
                createLabels();

            }

            function createLegend() {
                items = legendContainer.selectAll(".legend-item")
                    .data(pie(_data));

                items.enter().append("g")
                    .classed("legend-item", true)
                    .classed("_selected_", true)
                    .attr({ transform: "translate(" + (legendX) + "," + (legendY) + ")" });


                items.append("text").append('tspan').text(function(d, i) {
                        return labels[i]
                    })
                    .attr("x", function(d) {
                        return 10;
                    })
                    .attr("y", function(d, i) {
                        return 20 * i;
                    })
                    .attr('dx', '20').each(wrap)
                    .append('tspan').classed("data-values", true).text(function(d, i) {
                        return " (" + d.value.toLocaleString("es-ES") + " " + dataUnit + ")";
                    });

                items.append("rect")
                    .attr("x", function(d) {
                        return 10;
                    })
                    .attr("y", function(d, i) {
                        return 20 * i - 10;
                    })
                    .attr('width', '10')
                    .attr('height', '10')
                    .attr("fill", function(d, i) {
                        return colorScale(labels[i]);
                    });
            }

            function createLabels() {
                var labels = labelsContainer.selectAll(".label-arcs")
                    .data(pie(_data));

                gLabels = labels.enter().append("g")
                    .classed("labels", true)
                    .classed("_selected_", true)
                    .style("text-anchor", function(d) {
                        var rads = ((d.endAngle - d.startAngle) / 2) + d.startAngle + 10;
                        if ((rads > 7 * Math.PI / 4 && rads < Math.PI / 4) || (rads > 3 * Math.PI / 4 && rads < 5 * Math.PI / 4)) {
                            return "middle";
                        } else if (rads >= Math.PI / 4 && rads <= 3 * Math.PI / 4) {
                            return "start";
                        } else if (rads >= 5 * Math.PI / 4 && rads <= 7 * Math.PI / 4) {
                            return "end";
                        } else {
                            return "middle";
                        }
                    })
                    .style('cursor', 'pointer');



                gLabels.append('text')
                    .classed('labelValue', true)
                    .attr("x", function(d) {
                        var a = d.startAngle + (d.endAngle - d.startAngle) / 2 - Math.PI / 2;
                        d.cx = Math.cos(a) * (outRadius + 75);
                        return d.x = Math.cos(a) * (outRadius + 15);
                    })
                    .attr("y", function(d) {
                        var a = d.startAngle + (d.endAngle - d.startAngle) / 2 - Math.PI / 2;
                        d.cy = Math.sin(a) * (outRadius + 75);
                        return d.y = Math.sin(a) * (outRadius + 15);
                    })
                    .attr('dy', '0.2em')
                    .text(function(d, i) {
                        if ((d.endAngle - d.startAngle) > 0.5) {
                            return d3.format(".0%")(_data[i] / d3.sum(_data));
                        } else {
                            return "";
                        }
                    });

                relax();
            }

            function arcTween(a) {
                var i = d3.interpolate(this._current, a);
                this._current = i(0);
                return function(t) {
                    return arc(i(t));
                };
            }

            function relax() {
                var again = false;
                gLabels.each(function(d, i) {
                    var a = this;
                    var da = d3.select(a);
                    var y1 = da.attr("y");
                    gLabels.each(function(d, i) {
                        var b = this;
                        if (a == b) return;
                        var db = d3.select(b);
                        if (da.attr("text-anchor") != db.attr("text-anchor")) return;
                        var y2 = db.attr("y");
                        deltaY = y1 - y2;
                        if (Math.abs(deltaY) > spacing) return;
                        again = true;
                        var sign = deltaY > 0 ? 1 : -1;
                        var adjust = sign * alpha;
                        da.attr("y", +y1 + adjust);
                        db.attr("y", +y2 - adjust);
                    });
                });
                if (again) { setTimeout(relax, 20); }
            }

            updateData = function() {
                _data = data;

                pie.value(function(d) { return d; }).sort(null);

                path = svg.select(".donut-chart-group").selectAll("path").data(pie(_data));

                path.transition().duration(1000)
                    .attrTween("d", arcTween);



                svg.selectAll('.labels')
                    .data(pie(_data))
                    .style("text-anchor", function(d) {
                        var rads = ((d.endAngle - d.startAngle) / 2) + d.startAngle + 10;
                        if ((rads > 7 * Math.PI / 4 && rads < Math.PI / 4) || (rads > 3 * Math.PI / 4 && rads < 5 * Math.PI / 4)) {
                            return "middle";
                        } else if (rads >= Math.PI / 4 && rads <= 3 * Math.PI / 4) {
                            return "start";
                        } else if (rads >= 5 * Math.PI / 4 && rads <= 7 * Math.PI / 4) {
                            return "end";
                        } else {
                            return "middle";
                        }
                    });


                svg.selectAll('.labelValue')
                    .data(pie(_data))
                    .transition()
                    .duration(1500)
                    .attr("x", function(d) {
                        var a = d.startAngle + (d.endAngle - d.startAngle) / 2 - Math.PI / 2;
                        d.cx = Math.cos(a) * (outRadius + 75);
                        return d.x = Math.cos(a) * (outRadius + 15);
                    })
                    .attr("y", function(d) {
                        var a = d.startAngle + (d.endAngle - d.startAngle) / 2 - Math.PI / 2;
                        d.cy = Math.sin(a) * (outRadius + 75);
                        return d.y = Math.sin(a) * (outRadius + 15);
                    })
                    .text(function(d, i) {
                        if ((d.endAngle - d.startAngle) > 0.5) {
                            return d3.format(".0%")(_data[i] / d3.sum(_data));
                        } else {
                            return "";
                        }

                    });
                // relax();
                items = legendContainer.selectAll(".legend-item")
                    .data(pie(_data));
                child = items.selectAll("tspan.data-values")
                    .data(function(d) { return [d]; });
                child
                    .transition()
                    .duration(1500)
                    .text(function(d, i) {
                        return "(" + d.value.toLocaleString("es-ES") + " " + dataUnit + ")";
                    })


                svg.transition().attr({ width: width, height: height });
            }

            updateWidth = function() {
                chartW = width - margin.left - margin.right;
                x1 = d3.scale.ordinal()
                    .domain(labels.map(function(d, i) { return i; }))
                    .rangeRoundBands([0, chartW], 0.1);

                svg.transition().attr({ width: width, height: height });

                svg.select(".container-group")
                    .transition()
                    .attr({ transform: "translate(" + (margin.left + (chartW) / 2) + "," + (margin.top + (chartH) / 2) + ")" });

                items.selectAll("text").remove();
                items.append("text").append('tspan').text(function(d, i) {
                        return labels[i].toLocaleString("es-ES");
                    })
                    .attr("x", function(d) {
                        return 10;
                    })
                    .attr("y", function(d, i) {
                        return 20 * i;
                    })
                    .attr('dx', '20').each(wrap)
                    .append('tspan').classed("data-values", true).text(function(d, i) {
                        return "(" + d.value.toLocaleString("es-ES") + ")";
                    });

            }

            clearSelection = function() {
                svg.selectAll(".donut-arc").classed("_selected_", true);
                svg.selectAll(".labels").classed("_selected_", true);
                svg.selectAll(".legend-item").classed("_selected_", true);
                onClickFunction(svg);
            }

        });
    }

    exports.cleanFilter = function() {
        if (typeof clearSelection === 'function') clearSelection();
    }

    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };

    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        if (typeof updateWidth === 'function') updateWidth();
        return this;
    };

    exports.onClickFunction = function(value) {
        if (!arguments.length) return onClickFunction;
        onClickFunction = value;
        return this;
    };

    exports.labels = function(_x) {
        if (!arguments.length) return labels;
        labels = _x;
        return this;
    };

    exports.dataTitle = function(_x) {
        if (!arguments.length) return dataTitle;
        dataTitle = _x;
        return this;
    };

    exports.dataUnit = function(_x) {
        if (!arguments.length) return dataUnit;
        dataUnit = _x;
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };

    exports.innRadius = function(_x) {
        if (!arguments.length) return innRadius;
        innRadius = parseInt(_x);
        return this;
    };

    exports.outRadius = function(_x) {
        if (!arguments.length) return outRadius;
        outRadius = parseInt(_x);
        return this;
    };

    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };

    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    exports.showLegend = function(_x) {
        if (!arguments.length) return showLegend;
        showLegend = _x;
        return this;
    };

    exports.legendX = function(_x) {
        if (!arguments.length) return legendX;
        legendX = _x;
        return this;
    };

    exports.legendY = function(_x) {
        if (!arguments.length) return legendY;
        legendY = _x;
        return this;
    };


    exports.showLabel = function(_x) {
        if (!arguments.length) return showLabel;
        showLabel = _x;
        return this;
    };

    exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
    };

    exports.selectable = function(_x) {
        if (!arguments.length) return selectable;
        selectable = _x;
        return this;
    };

    exports.colors = function(_x) {
        if (!arguments.length) return colors;
        colors = _x;
        return this;
    };

    exports.selection = function(_x){
      if (!arguments.length) return selection;
      selection = _x;

      if(svg) {
        svg.selectAll('g._selected_').classed('_selected_', false);
        svg.selectAll('g._notSelected_').classed('_notSelected_', false);
      }
      return this;
    };

    exports.analyticsClass = function(_x){
        if (!arguments.length) return analyticsClass;
        analyticsClass = _x;
        return this;
    };

    return exports;
};
