d3.cloudshapes.multiLevelDonutChart = function module() {

  var margin = { top: 20, right: 20, bottom: 40, left: 40 },
  width = 200,
  height = 200,
  color = "#001ff0";

  var innRadius = 30;
  var singleDonutWidth = 50;
  var gap = 2;

  // data = [[values lvl 1], [values lvl 2], ...]
  var data = [];
  // labels = [[labels lvl 1], [labels lvl 2], ...]
  var labels = [];

  var dataUnit = "";
  var selectable = true;
  var selection;

  var legendX = 10;
  var legendY = 10;

  var showLabels = false;
  var showLegend = true;

  var svg;

  var colors = d3.scale.category10().range();
  var customColors = false;

  var labelsColor = "black";

  var analyticsClass = "";

  var updateData;
  var onClickFunction = function() {};

  var sunburstMode = false;
  // Show arc only if true. Used only if in sunburst mode
  var visible = [];


  // Private vars
  var _pies = {};
  var _arcs = {};
  var chartLabels;

  var tip = d3.select("body").append("div").attr("class", "toolTipRowChart");


  function exports(_selection) {
    _selection.each(function() {
      //compactDataAndLabels();

      var colorScale = d3.scale.ordinal()
      .domain(_.uniq(_.flatten(labels)))
      .range(colors);

      var chartW = width - margin.left - margin.right,
      chartH = height - margin.top - margin.bottom;

      var pie_containers = [];

      // If no SVG exists, create one - and add key groups:
      if (!svg) {
        svg = d3.select(this)
        .append("svg")
        .classed("elearn-multiLevelDonutChart", true);
        var container = svg.append("g")
        .classed("container-group", true);
        var pie_containers = data.map(function(d,i) {
          return container.append("g").classed("donut-chart-group", true);
        });
        var legendContainer = svg.append("g")
        .classed("legend-group", true);
      }


      // Transition the width and height of the main SVG and the key 'g' group:
      svg.attr({ width: width, height: height });

      svg.select(".container-group")
      .attr({ transform: "translate(" + (margin.left + (chartW / 2)) + "," + (margin.top + (chartH / 2)) + ")" });


      data.map(function(levelData, lvlDataIndex) {
        var pie = d3.layout.pie()
        .value(function(d, i) { return d; })
        .sort(null);

        _pies[lvlDataIndex] = pie;

        // ARCS

        var arc = d3.svg.arc()
        .innerRadius(innRadius + lvlDataIndex*(singleDonutWidth + gap/2))
        .outerRadius(innRadius + singleDonutWidth + lvlDataIndex*(singleDonutWidth + gap/2));

        _arcs[lvlDataIndex] = arc;
        var arcs = pie_containers[lvlDataIndex].selectAll(".donut-arc")
        .data(pie(levelData));

        arcs.enter().append("path")
        .classed("donut-arc", true)
        .classed(analyticsClass, true)
        .attr('stroke', '#fff')
        .attr('stroke-width', gap)
        .attr("fill", function(d, i) {
          if(customColors) {
            var col = colors[labels[lvlDataIndex][i]];
            if(col) {
              return col;
            } else {
              colorScale(labels[lvlDataIndex][i]);
            }
          } else {
            return colorScale(labels[lvlDataIndex][i]);
          }
        })
        .attr("opacity", function(d, i) {
          if(sunburstMode) {
            if(!visible[lvlDataIndex][i]) { return 0; }
          } else {
            return 1;
          }
        })
        .attr("d", arc)
        .each(function(d, _i) { this._current = d; })
        .on("mousemove", function(d, i){
          tip.style("left", d3.event.pageX+10+"px");
          tip.style("top", d3.event.pageY-25+"px");
          tip.html((labels[lvlDataIndex][i])+"<br>"+(d.value) + " " + dataUnit);
        })
        .on('mouseover', function(d, i) {
          if(!sunburstMode || (sunburstMode && visible[lvlDataIndex][i])) {
            tip.style("left", d3.event.pageX+10+"px");
            tip.style("top", d3.event.pageY-25+"px");
            tip.style("display", "inline-block");
            tip.html(labels[lvlDataIndex][i] +"<br>"+ d.value + " " + dataUnit);
          } else {
            tip.style("display", "none");
          }
        })
        .on('mouseout', function(d, i) {
          if(!sunburstMode || (sunburstMode && visible[lvlDataIndex][i])) {
            tip.style("display", "none");
          }
        })
        .on("click", function(d, index) {
          if (selectable && (!sunburstMode || (sunburstMode && visible[lvlDataIndex][index]))) {
            svg.selectAll(".donut-arc").classed("_selected_", false);
            svg.selectAll(".donut-arc").classed("_notSelected_", true);

            svg.selectAll(".labels").classed("_selected_", false);
            svg.selectAll(".labels").classed("_notSelected_", true);

            pie_containers[lvlDataIndex].selectAll(".donut-arc").classed("_selected_", function(d, i) { return i === index; });
            pie_containers[lvlDataIndex].selectAll(".donut-arc").classed("_notSelected_", function(d, i) { return i != index; });

            pie_containers[lvlDataIndex].selectAll(".labels")
              .classed("_selected_", function(d, i) { return i === index; })
              .classed("_notSelected_", function(d, i) { return i != index; });

            if(lvlDataIndex == 0) {
              selection = {label: labels[lvlDataIndex][index], subCategory: null};
            } else {
              // Extract inner label
              var sumTotalToIndex = _.sum(_.filter(data[lvlDataIndex], function(elem, i) {
                return i <= index;
              }));
              var sumInnerLvl = 0;
              var innerLvlIndex = -1;
              for(i=0; i<data[0].length; i++) {
                sumInnerLvl += data[0][i];
                if(sumInnerLvl >= sumTotalToIndex) {
                  innerLvlIndex = i;
                  break;
                }
              }
              if(0 <= innerLvlIndex) {
                selection = {label: labels[0][innerLvlIndex], subCategory: labels[lvlDataIndex][index]};
              } else {
                selection = {label: 'none', subCategory: labels[lvlDataIndex][index]};
              }
            }

            onClickFunction(this, lvlDataIndex, index, labels[lvlDataIndex][index]);
          }
        });


        
        // LABELS
        if(showLabels) {
          chartLabels = pie_containers[lvlDataIndex].selectAll(".labels")
          .data(pie(levelData))
          .enter()
          .append("g")
          .classed("labels", true)
          .classed("_selected_", true)
          .style("text-anchor", function(d) {
            var rads = ((d.endAngle - d.startAngle) / 2) + d.startAngle + 10;
            if ((rads > 7 * Math.PI / 4 && rads < Math.PI / 4) || (rads > 3 * Math.PI / 4 && rads < 5 * Math.PI / 4)) {
              return "middle";
            } else if (rads >= Math.PI / 4 && rads <= 3 * Math.PI / 4) {
              return "start";
            } else if (rads >= 5 * Math.PI / 4 && rads <= 7 * Math.PI / 4) {
              return "end";
            } else {
              return "middle";
            }
          })
          .style("fill", labelsColor)
          .style('cursor', 'pointer');

          var labelRadius = innRadius + lvlDataIndex * singleDonutWidth + singleDonutWidth/2;

          chartLabels.append('text')
          .classed('labelValue', true)
          .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
          .attr('dy', '0.2em')
          .text(function(d, i) {
            if(!sunburstMode || (sunburstMode && visible[lvlDataIndex][i])) {
              if ((d.endAngle - d.startAngle) > 0.5) {
                return labels[lvlDataIndex][i].toLocaleString("es-ES");
              } else {
                return "";
              }
            }
          });
        }


        // Legend
        if (showLegend) {
          createLegend();
          showLegend = false;
        }

        function createLegend() {
          leftLegend = legendContainer.selectAll(".left-legend-item").data(_.uniq(labels[0]));

          leftLegend.enter().append("g")
          .classed("left-legend-item", true)
          .attr({ transform: "translate(" + (legendX) + "," + (legendY) + ")" });

          leftLegend.append("text").append('tspan').text(function(d, i) {
            return d.toLocaleString("es-ES");
          })
          .attr({
            x: 10,
            y: function(d,i) { return 20 * i; }
          })
          .attr('dx', '20');

          leftLegend.append("rect")
          .attr({
            x: 10,
            y: function(d,i) { return 20 * i - 10; },
            width: 10,
            height: 10,
            fill: function(d,i) { return colors[d]; }
          });


          rightLegend = legendContainer.selectAll(".right-legend-item").data(_.uniq(labels[1]));

          rightLegend.enter().append("g")
          .classed("right-legend-item", true)
          .attr({ transform: "translate(" + (legendX) + "," + (legendY) + ")" });

          rightLegend.append("text").append('tspan').text(function(d, i) {
            return d.toLocaleString("es-ES");
          })
          .attr({
            x: chartW - 60,
            y: function(d,i) { return 20 * i; }
          })
          .attr('dx', '20');

          rightLegend.append("rect")
          .attr({
            x: chartW - 60,
            y: function(d,i) { return 20 * i - 10; },
            width: 10,
            height: 10,
            fill: function(d,i) {return colors[d]; }
          });
        };
      });



      function compactDataAndLabels() {
        for(j = 0; j < data.length; j++) {
          for(i = 0; i < data[j].length; i++) {
            if(data[j][i] <= 0) {
              data[j].splice(i, 1);
              labels[j].splice(i, 1);
              i =- 1;
            }
          }
        }
      }



      function arcTween(a2, lvlDataIndex) {
        var i = d3.interpolate(this._current, a);
        this._current = i(0);
        return function(t) {
          return arc(i(t));
        };
      }



      updateData = function() {
        _data = data;
        _labels = labels;


        data.map(function(levelData, lvlDataIndex) {
          var pie = _pies[lvlDataIndex];

          arcss = pie_containers[lvlDataIndex].selectAll(".donut-arc")
          .data(pie(levelData));


          arcss.transition()
          .duration(1000)
          .attrTween("d", function(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function(t) {
              return _arcs[lvlDataIndex](i(t));
            };
          })
          .attr("fill", function(d, i) {
            if(customColors) {
              var col = colors[labels[lvlDataIndex][i]];
              if(col) {
                return col;
              } else {
                return colorScale(labels[lvlDataIndex][i]);
              }
            } else {
              return colorScale(labels[lvlDataIndex][i]);
            }
          });


          lab = pie_containers[lvlDataIndex].selectAll('.labelValue')
          .data(pie(levelData));


          lab.transition()
          .duration(1000)
          .attr("transform", function(d) { return "translate(" + _arcs[lvlDataIndex].centroid(d) + ")"; })
          .text(function(d,i) { return labels[lvlDataIndex][i]; })
          .attr("opacity", function(d) {
            if(d.value > 0) {
              return 1;
            } else {
              return 0;
            }
          });

        });
      }


    });
  }


  exports.data = function(_x) {
    if (!arguments.length) return data;
    data = _x;
    if (typeof updateData === 'function') updateData();
    return this;
  };

  exports.width = function(_x) {
    if (!arguments.length) return width;
    width = parseInt(_x);
    if (typeof updateWidth === 'function') updateWidth();
    return this;
  };

  exports.labels = function(_x) {
    if (!arguments.length) return labels;
    labels = _x;
    return this;
  };

  exports.height = function(_x) {
    if (!arguments.length) return height;
    height = parseInt(_x);
    return this;
  };

  exports.innRadius = function(_x) {
    if (!arguments.length) return innRadius;
    innRadius = parseInt(_x);
    return this;
  };

  exports.gap = function(_x) {
    if (!arguments.length) return gap;
    gap = _x;
    return this;
  };

  exports.margin = function(_x) {
    if (!arguments.length) return margin;
    margin = _x;
    return this;
  };

  exports.colors = function(_x) {
    if (!arguments.length) return colors;
    colors = _x;
    return this;
  };

  exports.dataUnit = function(_x) {
    if (!arguments.length) return dataUnit;
    dataUnit = _x;
    return this;
  };

  exports.selectable = function(_x) {
    if (!arguments.length) return selectable;
    selectable = _x;
    return this;
  };

  exports.onClickFunction = function(value) {
    if (!arguments.length) return onClickFunction;
    onClickFunction = value;
    return this;
  };

  exports.singleDonutWidth = function(_x) {
    if (!arguments.length) return singleDonutWidth;
    singleDonutWidth = _x;
    return this;
  };

  exports.sunburstMode = function(_x) {
    if (!arguments.length) return sunburstMode;
    sunburstMode = _x;
    return this;
  };

  exports.visible = function(_x) {
    if (!arguments.length) return visible;
    visible = _x;
    return this;
  };

  exports.showLabels = function(_x) {
    if (!arguments.length) return showLabels;
    showLabels = _x;
    return this;
  };

  exports.showLegend = function(_x) {
    if (!arguments.length) return showLegend;
    showLegend = _x;
    return this;
  };

  exports.customColors = function(_x) {
    if (!arguments.length) return customColors;
    customColors = _x;
    return this;
  };

  exports.selection = function(_x){
    if (!arguments.length) return selection;
    selection = _x;
    if(svg) {
      svg.selectAll('path._selected_').classed('_selected_', false);
      svg.selectAll('path._notSelected_').classed('_notSelected_', false);
    }
    return this;
  };

  exports.analyticsClass = function(_x){
    if (!arguments.length) return analyticsClass;
    analyticsClass = _x;
    return this;
  };


  return exports;

}
