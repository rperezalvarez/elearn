// Stacked tornado chart Module
/////////////////////////////////

// Declare namespace

// Declare component: (this outer function acts as the closure):
d3.cloudshapes.stackedTornadoChart = function module() {
  var margin = {top: 20, right: 10, bottom: 20, left: 100},
  width = 500,
  height = 500,
  color = "#001ff0",
  ease = "exp";

  var colors = d3.scale.category20().range();
  // First array of data corresponds to left side groups, second array corresponds to right side groups. Both arrays must have the same length
  var data = [];

  // Labels array must have the same length of data
  var labels = [];
  var labelsText = [];

  var seriesNames = [];
  var svg;

  var gapBar = 0;

  var maxvalueLeft;
  var maxvalueRight;

  var updateData;
  var dataUnit = "";

  var dataStacks = [];

  var analyticEventName = null;

  var middleGap = 0.1;
  var leftLabelSpacing = 3;
  var rightLabelSpacing = 2;

  var showAxisX = true;
  var showAxisY = true;

  var showLegend = true;
  var isStacked = true;

  var legendX = 10;
  var legendY = 10;

  var xAxisRightTitle = "";
  var xAxisLeftTitle = "";

  var rightDataUnit = "";
  var leftDataUnit = "";

  var axisTitlesHeight = 10;

  var selectable = true;
  var selection;
  var onClickFunction;

  var tip = d3.select("body").append("div")
    .attr("class", "toolTipRowChart");

  // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
  // the above variables.
  function exports(_selection){
    _selection.each(function() {
      var chartW = width - margin.left - margin.right,
          chartH = height - margin.top - margin.bottom;

      if (!svg) {
        svg = d3.select(this)
        .append("svg")
        .classed("elearn-stackedTornadoChart", true)
        .attr("width", width)
        .attr("height", height);
        var container = svg.append("g")
        .classed("container-group", true)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var legendContainer = svg.append("g")
            .classed("legend-group", true);
      }

    function deepcopy(obj) {
      return JSON.parse(JSON.stringify(obj));
    }
    var max_values = [[], []];
    var stacked_data = [];
    var dataStacks = deepcopy(data);
    dataStacks.forEach(function(newData, newDataIndex) {
      // Remap data to make stack group
      stackedData = [];
      newData.forEach(function(array, i) {
        var array = d3.transpose(array);
        var objectArray = []
        array.forEach(function(activity, j) {
          objectArray.push({activity: labels[j]})
          activity.forEach(function(d, k){
            objectArray[j][seriesNames[k]] = d;
          })
        })
        stackedData.push(objectArray)
      });

      // Transpose the data into layers
      var stackLeft = d3.layout.stack()(seriesNames.map(function(serie) {
        return stackedData[0].map(function(d) {
          return {x: d.activity, y: +d[serie]};
        });
      }));

      var stackRight = d3.layout.stack()(seriesNames.map(function(serie) {
        return stackedData[1].map(function(d) {
          return {x:d.activity, y: +d[serie]}
        });
      }));
      stacked_data.push([stackLeft, stackRight]);
      max_values[0].push(d3.max(stackLeft, function(d) {return d3.max(d, function(d) {return d.y0 + d.y;}); }))
      max_values[1].push(d3.max(stackRight, function(d) {return d3.max(d, function(d) {return d.y0 + d.y;}); }))
      // max_values[0].push()

    });
    // Max values for left/right stack to add labels
    var maxValuesLeft = {};
    var maxValuesRight = {};

    labels.forEach(function(l, lindex) {
      maxValuesLeft[l] = [];
      maxValuesRight[l] = [];
    });

    stacked_data.forEach(function(stack, sindex) {
      var len_stack = stack[0].length;
      stack[0][len_stack - 1].forEach(function(datum, dindex) {
        maxValuesLeft[datum.x].push(datum.y + datum.y0);
      });
      stack[1][len_stack - 1].forEach(function(datum) {
        maxValuesRight[datum.x].push(datum.y + datum.y0);
      });
    });

      var maxvalueLeft = d3.max(max_values[0]),
          maxvalueRight = d3.max(max_values[1]),
          minvalueLeft = d3.min(max_values[0]),
          minvalueRight = d3.min(max_values[1]);

      // Define x and y scale variables.
      var xLeft = d3.scale.linear()
          .domain([0, 1.1*maxvalueLeft])
          .range([chartW/2 - chartW*middleGap/2, 0]);

      var xRight = d3.scale.linear()
          .domain([0, 1.1*maxvalueRight])
          .range([chartW / 2 + chartW*middleGap/2, chartW]);

      var y1 = d3.scale.ordinal()
          .domain(labels)
          .rangeRoundBands([chartH, 0], 0.3);

      var y2 = d3.scale.ordinal()
          .domain(labelsText)
          .rangeRoundBands([chartH, 0], 0.3);

      // ----Chart titles----
      // Left title
      container.append("text")
        .attr("dy", "-0.5em")
        .attr("fill", "#000")
        .attr("text-anchor", "end")
        .attr("dominant-baseline", "ideographic")
        .attr("transform", "translate(" + (chartW/2 - chartW*middleGap/2) + ", " + 0 + ")")
        .classed("axis-title", true)
        .text(xAxisLeftTitle);

      // Right title
      container.append("text")
        .attr("dy", "-0.5em")
        .attr("fill", "#000")
        .attr("text-anchor", "start")
        .attr("dominant-baseline", "ideographic")
        .attr("transform", "translate(" + (chartW/2 + chartW*middleGap/2) + ", " + 0 + ")")
        .classed("axis-title", true)
        .text(xAxisRightTitle);

      // ----Define bottom left/right axis----
      var xAxisLeft = d3.svg.axis()
          .scale(xLeft)
          .orient("bottom")
          .tickSize(-chartH)
          .ticks(d3.min([maxvalueLeft, 4]));

      var xAxisRight = d3.svg.axis()
          .scale(xRight)
          .orient("bottom")
          .tickSize(-chartH)
          .ticks(d3.min([maxvalueRight, 4]));

      var barW = (y1.rangeBand() - (labelsText.length - 1)*gapBar)/labelsText.length;
      var colorsStacked = colors.slice(0, 3);
      for(var stackIndex = 0; stackIndex < stacked_data.length; stackIndex++)
      {
        // Select all left bars and bind left side data
        var leftStacked = container.selectAll("#left-stacked-group")
            .data(stacked_data[stackIndex][0])
            .enter().append("g")
            .attr("class", `left-stacked-group ${stackIndex}`)
            .attr("name-label",function(d,i){ return seriesNames[i]})
            .style("fill", function(d,i){ return colorsStacked[i];})
            .attr("transform", function(d,i) {
              return "translate(0," + (stackIndex *(barW + gapBar)) + ")";
            });

        // Left bars ENTER
        leftStacked.selectAll(".rect")
          .data(function(d) {return d; })
          .enter().append("rect")
          // .transition().ease(ease).duration(1000)
          .attr({
            x: function(d) { return xLeft(d.y + d.y0); },
            y: function(d, i) { return y1(d.x); },
            height: barW,
            // width: function(d,i) { return xLeft(d.y0) - xLeft(d.y0 + d.y)}
            width: 0
          })
          .on("mouseover", function(d, i) {
            tip.style("display", "inline-block");

            tip	.html(labels[i] + "<br/><b>" + d3.select(this.parentNode).attr("name-label") + ": </b>" + d.y.toFixed(2) + " " + leftDataUnit)
            .style("left", (d3.event.pageX) + "px")
            .style("top", (d3.event.pageY - 28) + "px");

            if(analyticEventName) {
              ahoy.track(analyticEventName);
            }
          })
          .on("mousemove", function(){
            tip.style("left", d3.event.pageX+10+"px");
            tip.style("top", d3.event.pageY-25+"px");
          })
          .on("mouseout", function(d) {
            tip.style("display", "none");
          })

          .transition()
          .duration(1000)
          .delay(function(d, i) {return i * 50;})
          .attr('y', function(d, i) {return y1(d.x)})
          .attr('width', function(d, i) {return xLeft(d.y0) - xLeft(d.y0 + d.y);})

        // Select all right bars and bind right side data:
        var rightStacked = container.selectAll("#right-stacked-group")
          .data(stacked_data[stackIndex][1])
          .enter().append("g")
          .attr("class", `right-stacked-group ${stackIndex}`)
          .attr("name-label",function(d,i){ return seriesNames[i]})
          .style("fill", function(d,i){return colorsStacked[i]})
          .attr("transform", function(d,i) {
            return "translate(0," + (stackIndex*(barW + gapBar)) + ")";
          });

        var rightStackedBars = rightStacked.selectAll("rect")
          .data(function(d) {return d;})
          .enter().append("rect")
          .attr({
            x: function(d) { return xRight(d.y0);},
            y: function(d, i) { return y1(d.x);},
            height: barW,
            // width: function(d, i) {return xRight(d.y0 + d.y) - xRight(d.y0);}
            width: 0
          })
          .on("mouseover", function(d, i) {
            tip.style("display", "inline-block");

            tip	.html(labels[i] + "<br/><b>" + d3.select(this.parentNode).attr("name-label") + ": </b>" + d.y + " " + rightDataUnit)
            .style("left", (d3.event.pageX) + "px")
            .style("top", (d3.event.pageY - 28) + "px");

            if(analyticEventName) {
              ahoy.track(analyticEventName);
            }
          })
          .on("mousemove", function(){
            tip.style("left", d3.event.pageX+10+"px");
            tip.style("top", d3.event.pageY-25+"px");
          })
          .on("mouseout", function(d) {
            tip.style("display", "none");
          })
          .transition()
          .duration(1000)
          .delay(function (d, i) { return i * 50;})
          .attr('y', function(d, i) { return y1(d.x); })
          .attr('width', function(d, i) {return xRight(d.y0 + d.y) - xRight(d.y0);} )
        }

        labels.forEach(function(label, lindex) {
          labelsText.forEach(function(name, index) {
            var leftLabels = container.append('text')
              .attr("text-anchor", "end")
              .attr("dominant-baseline", "alphabetical")
              .attr("x", function(d) { return xLeft(maxValuesLeft[label][index]+ minvalueLeft*0.1) ;})
              .attr("y", function(d) { return y1(label) + (3*barW / 4);})
              .attr("transform", function(d,i) { return "translate(0," + (index * (barW + gapBar)) + ")";})
              .attr("dx", "-1em")
              .style("fill", "#ababab")
              .text(name)

            var rightLabels = container.append('text')
              .attr("text-anchor", "start")
              .attr("dominant-baseline", "alphabetical")
              .attr("x", function(d) {return xRight(maxValuesRight[label][index] - minvalueRight*0.01);})
              .attr("y", function(d) {return y1(label) + (3*barW / 4);})
              .attr("transform", function(d, i) { return "translate(0," + (index * (barW + gapBar)) + ")";})
              .attr("dx", "1em")
              .style("fill", "#ababab")
              .text(name);
          })
        })

      // CHART X AXIS
      svg.insert("g", ":first-child")
          .attr("class", "x axis left axisHorizontal")
          .attr("transform", "translate("+ margin.left + "," + (chartH + margin.top) + ")")
          .call(xAxisLeft);

      svg.insert("g", ":first-child")
          .attr("class", "x axis right axisHorizontal")
          .attr("transform", "translate(" + margin.left + "," + (chartH + margin.top) + ")")
          .call(xAxisRight);

      // Add labels
      var ticksContainer = svg.append("g")
                            .classed("ticks", true)
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      ticksContainer.selectAll(".tick")
                    .data(labels)
                    .enter()
                    .append("text")

                    .classed("tick", true)
                    .attr("x", function(d,i) {
                      return chartW/2;
                    })
                    .attr("y", function(d,i) {
                      return y1(d) + labelsText.length*barW/2 + gapBar;
                    })
                    .style("text-anchor", "middle")
                    .attr("dominant-baseline", "central")
                    .text(function(d) { return d; });

      createLegend = function () {
        items = legendContainer.selectAll(".legend-item")
            .data(seriesNames);

        items.enter().append("g")
            .classed("legend-item", true)
            .classed("_selected_", true)
            .attr({ transform: "translate(" + (legendX) + "," + (legendY) + ")" })

        items.append("text").append('tspan').text(function(d, i) {
                return seriesNames[i].toLocaleString("es-ES");
            })
            .attr("x", function(d) { return 10; })
            .attr("y", function(d, i) { return 20 * i; })
            .attr('dx', '20')

        items.append("rect")
            .attr("x", function(d) { return 10; })
            .attr("y", function(d, i) { return 20 * i - 10; })
            .attr('width', '10')
            .attr('height', '10')
            .attr("fill", function(d, i) { return colors[i]; });
        };

      if (showLegend) {
        createLegend();
      }
    })
  };
  exports.data = function(_x) {
    if (!arguments.length) return data;
    data = _x;

    if (typeof updateData === 'function') updateData();
    return this;
  };

  exports.onClickFunction = function(value) {
    if (!arguments.length) return onClickFunction;
    onClickFunction = value;
    return this;
  };

  exports.selectable = function(_x){
    if (!arguments.length) return selectable;
    selectable = _x;
    return this;
  }

  exports.labels = function(_x) {
    if (!arguments.length) return labels;
    labels = _x;
    return this;
  };

  exports.labelsText = function(_x) {
    if (!arguments.length) return labelsText;
    labelsText = _x;
    return this;
  };

  exports.orderData = function(_x) {
    if (!arguments.length) return orderData;
    orderData = _x;
    return this;
  };

  exports.showAxisY = function(_x){
    if (!arguments.length) return showAxisY;
    showAxisY = _x;
    return this;
  };

  exports.legendTitle = function(_x){
    if (!arguments.length) return legendTitle;
    legendTitle = _x;
    return this;
  };

  exports.labelRotation = function(_x){
    if (!arguments.length) return labelRotation;
    labelRotation = _x
    return this;
  };

  exports.labelSize = function(_x){
    if (!arguments.length) return labelSize;
    labelSize = _x;
    return this
  };

  exports.legendX = function(_x){
    if (!arguments.length) return legendX;
    legendX = _x;
    return this
  };

  exports.legendY = function(_x) {
      if (!arguments.length) return legendY;
      legendY = _x;
      return this;
  };

  exports.colors = function(_x){
    if(!arguments.length) return colors;
    colors = _x;
    // if (typeof updateData === 'function') updateData();
    return this;
  };


  exports.dataTitle = function(_x) {
    if (!arguments.length) return dataTitle;
    dataTitle = _x;
    return this;
  };

  exports.dataUnit = function(_x) {
    if (!arguments.length) return dataUnit;
    dataUnit = _x;
    return this;
  };


  exports.margin = function(_x) {
    if (!arguments.length) return margin;
    margin = _x;
    return this;
  };

  exports.gapBar = function(_x) {
    if (!arguments.length) return gapBar;
    gapBar = _x;
    return this;
  };

  exports.labelsShort = function(_x){
    if (!arguments.length) return labelsShort;
    labelsShort = _x;
    return this;
  };

  // GETTERS AND SETTERS:
  exports.max_value = function(_x){
    //There is another way to define a internal variable based on others variables??
    if(max_value){
      max_value = d3.max(this.data, function(d){ return d.values[1]; });
    }

    return max_value;
  };

  exports.width = function(_x) {
    if (!arguments.length) return width;
    width = parseInt(_x);
    return this;
  };

  exports.setColors = function(_x) {
    if (!arguments.length) return color;
    colors = _x;
    return this;
  };

  exports.height = function(_x) {
    if (!arguments.length) return height;
    height = parseInt(_x);
    return this;
  };

  exports.ease = function(_x) {
    if (!arguments.length) return ease;
    ease = _x;
    return this;
  };

  exports.margin = function(_x) {
    if (!arguments.length) return margin;
    margin = _x;
    return this;
  };

  exports.seriesNames = function(_x) {
    if (!arguments.length) return seriesNames;
    seriesNames = _x;
    return this;
  };

  exports.showAxisY = function(_x) {
    if (!arguments.length) return showAxisY;
    showAxisY = _x;
    return this;
  };

  exports.showAxisX = function(_x) {
    if (!arguments.length) return showAxisX;
    showAxisX = _x;
    return this;
  };

  exports.middleGap = function(_x) {
    if (!arguments.length) return middleGap;
    middleGap = _x;
    return this;
  };

  exports.xAxisLeftTitle = function(_x) {
    if (!arguments.length) return xAxisLeftTitle;
    xAxisLeftTitle = _x;
    return this;
  };

  exports.xAxisRightTitle = function(_x) {
    if (!arguments.length) return xAxisRightTitle;
    xAxisRightTitle = _x;
    return this;
  };

  exports.leftDataUnit = function(_x) {
    if (!arguments.length) return leftDataUnit;
    leftDataUnit = _x;
    return this;
  };

  exports.rightDataUnit = function(_x) {
    if (!arguments.length) return rightDataUnit;
    rightDataUnit = _x;
    return this;
  };

  exports.selection = function(_x){
    if (!arguments.length) return selection;
    selection = _x;
    return this;
  };

  exports.dataStacks = function(_x){
    if (!arguments.length) return dataStacks;
    dataStacks = _x;
    return this;
  };

  exports.analyticEventName = function(_x) {
    if (!arguments.length) return analyticEventName;
    analyticEventName = _x;
    return this;
  };

  return exports;
};
