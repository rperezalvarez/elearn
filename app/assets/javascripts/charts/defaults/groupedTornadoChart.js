// Grouped tornado chart Module
/////////////////////////////////

// Declare namespace

// Declare component: (this outer function acts as the closure):
d3.cloudshapes.groupedTornadoChart = function module() {
  var margin = {top: 20, right: 10, bottom: 20, left: 100},
  width = 500,
  height = 500,
  color = "#001ff0",
  ease = "exp";

  var colors = d3.scale.category20().range();

  // First array of data corresponds to left side groups, second array corresponds to right side groups. Both arrays must have the same length
  var data = [];

  // Labels array must have the same length of data
  var labels = [];

  var seriesNames = [];
  var svg;

  var gapBar = 0;
  var labelsShort = [];

  var maxvalueLeft;
  var maxvalueRight;

  var updateData;
  var dataUnit = "";

  var analyticEventName = null;

  var middleGap = 0.1;

  var showAxisX = true;
  var showAxisY = true;

  var showLegend = true;

  var isStacked = false;

  var legendX = 10;
  var legendY = 10;

  var xAxisRightTitle = "";
  var xAxisLeftTitle = "";

  var rightDataUnit = "";
  var leftDataUnit = "";

  var axisTitlesHeight = 10;

  var selectable = true;
  var selection;
  var onClickFunction;

  var tip = d3.select("body").append("div")
    .attr("class", "toolTipRowChart");

  // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
  // the above variables.

  function exports(_selection) {
    _selection.each(function() {
      _data = data;
      var chartW = width - margin.left - margin.right,
          chartH = height - margin.top - margin.bottom;

      // Define x and y scale variables.
      var xLeft = d3.scale.linear()
      .domain([1.1*_.max(_.flatten(_data[0])), 0])
      .range([0, chartW/2 - chartW*middleGap/2]);

      var xRight = d3.scale.linear()
      .domain([0, 1.1*_.max(_.flatten(_data[1]))])
      .range([chartW/2 + chartW*middleGap/2, chartW]);

      var y1 = d3.scale.ordinal()
      .domain(labels)
      .rangeRoundBands([chartH, 0], 0.3);

      // If no SVG exists, create one - and add key groups:

      if (!svg) {
        svg = d3.select(this)
        .append("svg")
        .classed("elearn-groupedTornadoChart", true)
        .attr("width", width)
        .attr("height", height);
        var container = svg.append("g")
        .classed("container-group", true)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var legendContainer = svg.append("g")
            .classed("legend-group", true);
      }

      // Define axis
      var xAxisLeft = d3.svg.axis()
      .scale(xLeft)
      .orient("bottom")
      .tickSize(-chartH)
      .ticks(d3.min([maxvalueLeft, 4]));

      container.append("text")
      .attr("dy", "-0.5em")
      .attr("text-anchor", "end")
      .attr("dominant-baseline", "ideographic")
      .attr("transform", "translate(" + (chartW/2 - chartW*middleGap/2) + ", " + 0 + ")")
      .classed("axis-title", true)
      .text(xAxisLeftTitle);

      var xAxisRight = d3.svg.axis()
      .scale(xRight)
      .orient("bottom")
      .tickSize(-chartH)
      .ticks(d3.min([maxvalueRight, 4]));

      container.append("text")
      .attr("dy", "-0.5em")
      .attr("text-anchor", "start")
      .attr("dominant-baseline", "ideographic")
      .attr("transform", "translate(" + (chartW/2 + chartW*middleGap/2) + ", " + 0 + ")")
      .classed("axis-title", true)
      .text(xAxisRightTitle);

      var yAxisLeft = d3.svg.axis()
      .scale(y1)
      .orient("right")
      .tickFormat("");

      var barW = (y1.rangeBand() - (seriesNames.length - 1)*gapBar)/seriesNames.length;

      maxvalueLeft = (d3.max(_.flatten(_data[0])));

      // Select all left bars and bind left side data:
      var leftGroups = container.selectAll(".left-group-bars")
      .data(_data[0])
      .enter().append("g")
      .attr('class', 'left-group-bars')
      .attr("name-label",function(d,i){ return seriesNames[i]})
      .style("fill", function(d,i){ return colors[i]})
      .attr("transform", function(d,i) {
        return "translate(0," + (i*(barW + gapBar)) + ")";
      });

      
      var leftBars = leftGroups.selectAll(".left-bar")
      .data(function(d,i) {return d; });

      // ENTER, UPDATE and EXIT CODE:
      // D3 ENTER code for left bars
      leftBars.enter().append("rect")
      .classed("left-bar", true)
      .attr({
        x: function(d, i) { return xLeft(d); },
        y: function(d, i) { return y1(labels[i]); },
        width: function(d, i) { return Math.abs(xLeft(0) - xLeft(d)); },
        height: barW
      })
      .on("mousemove", function(d, i){
        tip.style("left", d3.event.pageX+10+"px");
        tip.style("top", d3.event.pageY-25+"px");
      })
      .on("mouseover", function(d, i) {
        tip.style("display", "inline-block");

        tip.html(labels[i] + "<br/><b>" + d3.select(this.parentNode).attr("name-label") + ": </b>" + parseFloat(d).toFixed(0) + " " + leftDataUnit)
        .style("left", (d3.event.pageX) + "px")
        .style("top", (d3.event.pageY - 28) + "px");
        if(analyticEventName) {
          ahoy.track(analyticEventName, {course_id: getCourseIdFromUrl()});
        }
      })
      .on("mouseout", function(d) {
        tip.style("display", "none");
      })
      .on("click", function(d,i, sub_i) {
        if(selectable && onClickFunction) {
          svg.selectAll(".right-bar").classed('_selected_', false);
          svg.selectAll(".right-bar").classed('_notSelected_', true);
          svg.selectAll(".left-bar").classed('_selected_', false);
          svg.selectAll(".left-bar").classed('_notSelected_', true);

          d3.select(this).classed("_notSelected_", false);
          d3.select(this).classed("_selected_", true);

          rightB = svg.selectAll(".right-bar").filter(function(d2, i2) {
            return i2 == i + labels.length * sub_i;
          });

          rightB.classed("_notSelected_", false);
          rightB.classed("_selected_", true);

          //selection = {label: labels[i], subIndex: sub_i};
          onClickFunction(this, d, labels[i], sub_i);
        }
      });

      var leftLabels = leftBars.enter().append('text')
      .attr("text-anchor", "end")
      .attr("dominant-baseline", "alphabetical")
      .attr("x", function(d,i) {
          return xLeft(d);
      })
      .attr("y", function(d,i) {
          return y1(labels[i]) + (3*barW / 4);
      })
      .attr("dx", "-1em")
      .text(function (d) { return parseFloat(d).toFixed(0); });

      // D3 EXIT code for left bars
      leftBars.exit().transition().style({opacity: 0}).remove();

      maxvalueRight = d3.max(_.flatten(_data[1]));
      // Select all right bars and bind right side data:

      var rightGroups = container.selectAll(".right-group-bars")
      .data(_data[1])
      .enter().append("g")
      .attr('class', 'right-group-bars')
      .attr("name-label",function(d,i){ return seriesNames[i]})
      .style("fill", function(d,i){ return colors[i]})
      .attr("transform", function(d,i) {
        return "translate(0," + (i*(barW + gapBar)) + ")";
      });

      var rightBars = rightGroups.selectAll(".right-bar")
      .data(function(d,i) {return d; });

      // ENTER, UPDATE and EXIT CODE:
      // D3 ENTER code for right bars
      rightBars.enter().append("rect")
      .classed("right-bar", true)
      .attr({
        x: function(d, i) { return xRight(0); },
        y: function(d, i) { return y1(labels[i]); },
        width: function(d, i) { return Math.abs(xRight(d) - xRight(0)); },
        height: barW
      })
      .on("mousemove", function(d, i){
        tip.style("left", d3.event.pageX+10+"px");
        tip.style("top", d3.event.pageY-25+"px");
      })
      .on("mouseover", function(d, i) {
        tip.style("display", "inline-block");
        tip.html(labels[i] + "<br/><b>" + d3.select(this.parentNode).attr("name-label") + ": </b>" + parseFloat(d).toFixed(0) + " " + rightDataUnit)

        // tip	.html(labels[i] + "<br/><b>" + d3.select(this.parentNode).attr("name-label") + ": </b>" + d.toFixed(1) + " " + rightDataUnit)
        .style("left", (d3.event.pageX) + "px")
        .style("top", (d3.event.pageY - 28) + "px");
        if(analyticEventName) {
          ahoy.track(analyticEventName, {course_id: getCourseIdFromUrl()});
        }
      })
      .on("mouseout", function(d) {
        tip.style("display", "none");
      })
      .on("click", function(d,i, sub_i) {
        if(selectable && onClickFunction) {
          svg.selectAll(".right-bar").classed('_selected_', false);
          svg.selectAll(".right-bar").classed('_notSelected_', true);
          svg.selectAll(".left-bar").classed('_selected_', false);
          svg.selectAll(".left-bar").classed('_notSelected_', true);

          d3.select(this).classed("_notSelected_", false);
          d3.select(this).classed("_selected_", true);

          leftB = svg.selectAll(".left-bar").filter(function(d2, i2) {
            return i2 == i + labels.length * sub_i;
          });

          leftB.classed("_notSelected_", false);
          leftB.classed("_selected_", true);
          onClickFunction(this, d, labels[i], sub_i);
        }
      });

      var rightLabels = rightBars.enter().append('text')
      .attr("text-anchor", "start")
      .attr("dominant-baseline", "alphabetical")
      .attr("x", function(d,i) {
          return xRight(d);
      })
      .attr("y", function(d,i) {
          return y1(labels[i]) + (3*barW / 4);
      })
      .attr("dx", "1em")
      .text(function (d) { return d.toFixed(1); });

      // D3 EXIT code for right bars
      rightBars.exit().transition().style({opacity: 0}).remove();


      // Add labels
      var ticksContainer = svg.append("g")
                            .classed("ticks", true)
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      ticksContainer.selectAll(".tick")
                    .data(labels)
                    .enter()
                    .append("text")

                    .classed("tick", true)
                    .attr("x", function(d,i) {
                      return chartW/2;
                    })
                    .attr("y", function(d,i) {
                      return y1(d) + seriesNames.length*barW/2 + gapBar;
                    })
                    .style("text-anchor", "middle")
                    .attr("dominant-baseline", "central")
                    .text(function(d) { return d;}) ;

      // CHART X AXIS
      svg.insert("g", ":first-child")
          .attr("class", "x axis left axisHorizontal")
          .attr("transform", "translate("+ margin.left + "," + (chartH + margin.top) + ")")
          .call(xAxisLeft);

      svg.insert("g", ":first-child")
          .attr("class", "x axis right axisHorizontal")
          .attr("transform", "translate(" + margin.left + "," + (chartH + margin.top) + ")")
          .call(xAxisRight);

      updateData = function() {
        _data = data;

        y1 = d3.scale.ordinal()
        .domain(labels)
        .rangeRoundBands([chartH, 0], 0.3);

        y2 = d3.scale.ordinal()
        .domain(_data.map(function(d, i) { return i; }))
        .rangeRoundBands([0, chartH/_data[0]], 0.3);

        barW = (y1.rangeBand() - (seriesNames.length - 1)*gapBar)/seriesNames.length;

        // EXIT LEFT GROUP
        var leftG = container.selectAll(".left-group-bars").data(_data[0]);

        leftG.exit().remove();


        // ENTER LEFT GROUP

        leftG.enter().append("g")
        .attr('class', 'left-group-bars')
        .attr("name-label",function(d,i){ return seriesNames[i]})
        .style("fill", function(d,i){ return colors[i]})
        .attr("transform", function(d,i) {
          return "translate(0," + (i*(barW + gapBar)) + ")";
        });

        var leftB = leftG.selectAll(".left-bar")
        .data(function(d,i) {return d; });

        leftB.enter().append("rect")
        .classed("left-bar", true)
        .attr({
          x: function(d, i) { return xLeft(0); },
          y: function(d, i) { return y1(labels[i]); },
          width: function(d, i) { return 0; },
          height: barW
        })
        .on("mousemove", function(d, i){
          tip.style("left", d3.event.pageX+10+"px");
          tip.style("top", d3.event.pageY-25+"px");
        })
        .on("mouseover", function(d, i) {
          tip.style("display", "inline-block");

          tip	.html(labels[i] + "<br/><b>" + d3.select(this.parentNode).attr("name-label") + ": </b>" + d.toFixed(0) + " " + leftDataUnit)
          .style("left", (d3.event.pageX) + "px")
          .style("top", (d3.event.pageY - 28) + "px");
          if(analyticEventName) {
            ahoy.track(analyticEventName, {course_id: getCourseIdFromUrl()});
          }
        })
        .on("mouseout", function(d) {
          tip.style("display", "none");
        })
        .on("click", function(d,i, sub_i) {
          if(selectable && onClickFunction) {
            svg.selectAll(".right-bar").classed('_selected_', false);
            svg.selectAll(".right-bar").classed('_notSelected_', true);
            svg.selectAll(".left-bar").classed('_selected_', false);
            svg.selectAll(".left-bar").classed('_notSelected_', true);

            d3.select(this).classed("_notSelected_", false);
            d3.select(this).classed("_selected_", true);

            rightB = svg.selectAll(".right-bar").filter(function(d2, i2) {
              return i2 == i + labels.length * sub_i;
            });

            rightB.classed("_notSelected_", false);
            rightB.classed("_selected_", true);

            //selection = {label: labels[i], subIndex: sub_i};
            onClickFunction(this, d, labels[i], sub_i);
          }
        });


        // UPDATE LEFT BARS
        leftB.transition().ease(ease).duration(1000)
        .attr({
          x: function(d) { return xLeft(d); },
          y: function(d, i) { return y1(labels[i]); },
          width: function(d) { return Math.abs(xLeft(0) - xLeft(d)); },
          height: barW
        });

        // UPDATE LEFT LABELS
        var leftL = leftG.selectAll('text')
        .data(function(d,i) { return d; });

        leftL.enter()
        .append('text')
        .attr("text-anchor", "end")
        .attr("dominant-baseline", "alphabetical")
        .attr("x", function(d,i) {
            return xLeft(0);
        })
        .attr("y", function(d,i) {
            return y1(labels[i]) + (3*barW / 4);
        })
        .attr("dx", "-1em")
        .text(function (d) { return d; });

        leftL.transition()
        .ease(ease)
        .duration(1000)
        .attr({
          x: function(d) { return xLeft(d); },
          y: function(d, i) { return y1(labels[i]) + (3*barW / 4); }
        })
        .tween("text", function(d) {
          var trans = d3.interpolate(this.textContent, d);
          var prec = parseFloat(d).toFixed(1) + "";
          return function(t) { this.textContent = parseFloat(trans(t)).toFixed(0); };
        });

        container.selectAll(".left-group-bars")
        .transition()
        .ease(ease)
        .duration(1000)
        .attr("transform", function(d,i) {
          return "translate(0," + (i*(barW + gapBar)) + ")";
        });

        // EXIT RIGHT GROUP
        var rightG = container.selectAll(".right-group-bars").data(_data[1]);

        rightG.exit().remove();

        // ENTER RIGHT GROUP
        rightG.enter().append("g")
        .attr('class', 'right-group-bars')
        .attr("name-label",function(d,i){ return seriesNames[i]})
        .style("fill", function(d,i){ return colors[i]})
        .attr("transform", function(d,i) {
          return "translate(0," + (i*(barW + gapBar)) + ")";
        });

        var rightB = rightG.selectAll(".right-bar")
        .data(function(d,i) {return d; });

        rightB.enter().append("rect")
        .classed("right-bar", true)
        .attr({
          x: function(d, i) { return xRight(0); },
          y: function(d, i) { return y1(labels[i]); },
          width: function(d, i) { return 0; },
          height: barW
        })
        .on("mousemove", function(d, i){
          tip.style("left", d3.event.pageX+10+"px");
          tip.style("top", d3.event.pageY-25+"px");
        })
        .on("mouseover", function(d, i) {
          tip.style("display", "inline-block");

          tip	.html(labels[i] + "<br/><b>" + d3.select(this.parentNode).attr("name-label") + ": </b>" + d.toFixed(0) + " " + leftDataUnit)
          .style("left", (d3.event.pageX) + "px")
          .style("top", (d3.event.pageY - 28) + "px");

          if(analyticEventName) {
            ahoy.track(analyticEventName, {course_id: getCourseIdFromUrl()});
          }
        })
        .on("mouseout", function(d) {
          tip.style("display", "none");
        })
        .on("click", function(d,i, sub_i) {
          if(selectable && onClickFunction) {
            svg.selectAll(".right-bar").classed('_selected_', false);
            svg.selectAll(".right-bar").classed('_notSelected_', true);
            svg.selectAll(".left-bar").classed('_selected_', false);
            svg.selectAll(".left-bar").classed('_notSelected_', true);

            d3.select(this).classed("_notSelected_", false);
            d3.select(this).classed("_selected_", true);

            leftBB = svg.selectAll(".left-bar").filter(function(d2, i2) {
              return i2 == i + labels.length * sub_i;
            });

            leftBB.classed("_notSelected_", false);
            leftBB.classed("_selected_", true);

            //selection = {label: labels[i], subIndex: sub_i};
            onClickFunction(this, d, labels[i], sub_i);
          }
        });


        // UPDATE LEFT BARS
        rightB.transition().ease(ease).duration(1000)
        .attr({
          x: function(d) { return xRight(0); },
          y: function(d, i) { return y1(labels[i]); },
          width: function(d) { return Math.abs(xRight(0) - xRight(d)); },
          height: barW
        });

        // UPDATE LEFT LABELS
        var rightL = rightG.selectAll('text')
        .data(function(d,i) { return d; });

        rightL.enter()
        .append('text')
        .attr("text-anchor", "start")
        .attr("dominant-baseline", "alphabetical")
        .attr("x", function(d,i) {
            return xRight(0);
        })
        .attr("y", function(d,i) {
            return y1(labels[i]) + (3*barW / 4);
        })
        .attr("dx", "1em")
        .text(function (d) { return d; });


        rightL.transition()
        .ease(ease)
        .duration(1000)
        .attr({
          x: function(d) { return xRight(d); },
          y: function(d, i) { return y1(labels[i]) + (3*barW / 4); }
        })
        .tween("text", function(d) {
          var trans = d3.interpolate(this.textContent, d);
          var prec = d.toFixed(1) + "";
          return function(t) { this.textContent = trans(t).toFixed(1); };
        });

        container.selectAll(".right-group-bars")
        .transition()
        .ease(ease)
        .duration(1000)
        .attr("transform", function(d,i) {
          return "translate(0," + (i*(barW + gapBar)) + ")";
        });
      };

      createLegend = function () {
          items = legendContainer.selectAll(".legend-item")
              .data(seriesNames);

          items.enter().append("g")
              .classed("legend-item", true)
              .classed("_selected_", true)
              .attr({ transform: "translate(" + (legendX) + "," + (legendY) + ")" })

          items.append("text").append('tspan').text(function(d, i) {
                  return seriesNames[i].toLocaleString("es-ES");
              })

              .attr("x", function(d) {
                  return 10;
              })

              .attr("y", function(d, i) {
                  return 20 * i;
              })

              .attr('dx', '20')

          items.append("rect")
              .attr("x", function(d) {
                  return 10;
              })

              .attr("y", function(d, i) {
                  return 20 * i - 10;
              })

              .attr('width', '10')
              .attr('height', '10')
              .attr("fill", function(d, i) {
                  return colors[i];
              });
          };

      if (showLegend){
        createLegend();
        }

      });
  }

  exports.data = function(_x) {
    if (!arguments.length) return data;
    data = _x;

    if (typeof updateData === 'function') updateData();
    return this;
  };

  exports.onClickFunction = function(value) {
    if (!arguments.length) return onClickFunction;
    onClickFunction = value;
    return this;
  };

  exports.selectable = function(_x){
    if (!arguments.length) return selectable;
    selectable = _x;
    return this;
  }

  exports.labels = function(_x) {
    if (!arguments.length) return labels;
      labels = _x
    return this;
  };

  exports.orderData = function(_x) {
    if (!arguments.length) return orderData;
    orderData = _x;
    return this;
  };

  exports.showAxisY = function(_x){
    if (!arguments.length) return showAxisY;
    showAxisY = _x;
    return this;
  };

  exports.legendTitle = function(_x){
    if (!arguments.length) return legendTitle;
    legendTitle = _x;
    return this;
  };

  exports.labelRotation = function(_x){
    if (!arguments.length) return labelRotation;
    labelRotation = _x
    return this;
  };

  exports.labelSize = function(_x){
    if (!arguments.length) return labelSize;
    labelSize = _x;
    return this
  };

  exports.legendX = function(_x){
    if (!arguments.length) return legendX;
    legendX = _x;
    return this
  };

  exports.legendY = function(_x) {
      if (!arguments.length) return legendY;
      legendY = _x;
      return this;
  };

  exports.colors = function(_x){
    if(!arguments.length) return colors;
    colors = _x;
    // if (typeof updateData === 'function') updateData();
    return this;
  };


  exports.dataTitle = function(_x) {
    if (!arguments.length) return dataTitle;
    dataTitle = _x;
    return this;
  };

  exports.dataUnit = function(_x) {
    if (!arguments.length) return dataUnit;
    dataUnit = _x;
    return this;
  };


  exports.margin = function(_x) {
    if (!arguments.length) return margin;
    margin = _x;
    return this;
  };

  exports.gapBar = function(_x) {
    if (!arguments.length) return gapBar;
    gapBar = _x;
    return this;
  };

  exports.labelsShort = function(_x){
    if (!arguments.length) return labelsShort;
    labelsShort = _x;
    return this;
  };

  // GETTERS AND SETTERS:
  exports.max_value = function(_x){
    //There is another way to define a internal variable based on others variables??
    if(max_value){
      max_value = d3.max(this.data, function(d){ return d.values[1]; });
    }

    return max_value;
  };

  exports.width = function(_x) {
    if (!arguments.length) return width;
    width = parseInt(_x);
    return this;
  };

  exports.setColors = function(_x) {
    if (!arguments.length) return color;
    colors = _x;
    return this;
  };

  exports.height = function(_x) {
    if (!arguments.length) return height;
    height = parseInt(_x);
    return this;
  };

  exports.ease = function(_x) {
    if (!arguments.length) return ease;
    ease = _x;
    return this;
  };

  exports.margin = function(_x) {
    if (!arguments.length) return margin;
    margin = _x;
    return this;
  };
  
  exports.seriesNames = function(_x) {
    if (!arguments.length) return seriesNames;
    seriesNames = _x
    return this;
  };

  exports.showAxisY = function(_x) {
    if (!arguments.length) return showAxisY;
    showAxisY = _x;
    return this;
  };

  exports.showAxisX = function(_x) {
    if (!arguments.length) return showAxisX;
    showAxisX = _x;
    return this;
  };

  exports.middleGap = function(_x) {
    if (!arguments.length) return middleGap;
    middleGap = _x;
    return this;
  };

  exports.xAxisLeftTitle = function(_x) {
    if (!arguments.length) return xAxisLeftTitle;
    xAxisLeftTitle = _x;
    return this;
  };

  exports.xAxisRightTitle = function(_x) {
    if (!arguments.length) return xAxisRightTitle;
    xAxisRightTitle = _x;
    return this;
  };

  exports.leftDataUnit = function(_x) {
    if (!arguments.length) return leftDataUnit;
    leftDataUnit = _x;
    return this;
  };

  exports.rightDataUnit = function(_x) {
    if (!arguments.length) return rightDataUnit;
    rightDataUnit = _x;
    return this;
  };

  exports.selection = function(_x){
    if (!arguments.length) return selection;
    selection = _x;
    return this;
  };

  exports.analyticEventName = function(_x) {
    if (!arguments.length) return analyticEventName;
    analyticEventName = _x;
    return this;
  };

  return exports;
};
