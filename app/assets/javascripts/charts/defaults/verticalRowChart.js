//Reference : http://jsfiddle.net/weXNd/6/
d3.cloudshapes.verticalRowChart = function module() {
  var margin = {top: 40, right: 40, bottom: 50, left: 40};
  var width;
  var height;
  var data = [];
  var dataUnit = "";
  var max_value;
  var svg;
  var ease = "exp";
  var selection;
  var analyticsClass = "";

  var colors = { external:'#87cefa', internal:'#4169e1'}

  var gapExternalBars = 0.5;
  var gapInternalBars = 0.3;

  var showLegend = true;
  var legendX = 0;
  var legendY = 0;

  var onClickFunction;

  var tip = d3.select("body").append("div").attr("class", "toolTipRowChart");


  function exports(_selection) {
    _selection.each(function() {

      var chartW = width - margin.left - margin.right,
          chartH = height - margin.top - margin.bottom;

      var x0 = d3.scale.ordinal()
      .domain(data.map(function(d) {
        return d.label;
      }))
      .rangeRoundBands([0, chartW], gapExternalBars);

      var y0 = d3.scale.linear()
      .domain([0, d3.max(data, function(d) { return d.values[1];})])
      .range([chartH, 0]);

      max_value = d3.max(data, function(d){ return d3.max(d.values); });

      var xAxis = d3.svg.axis()
      .scale(x0)
      .orient("bottom");

      var yAxis = d3.svg.axis()
      .scale(y0)
      .tickSize(-chartW)
      .orient("left")
      .ticks(d3.min([max_value, 5]));

      // If no SVG exists, create one - and add key groups:
      if (!svg) {
        svg = d3.select(this)
        .append("svg")
        .classed("elearn-verticalRowChart", true)
        .attr("width", width)
        .attr("height", height);

        var container = svg.append("g")
        .classed("container-group", true)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var legendContainer = svg.append("g")
            .classed("legend-group", true);
      }

      container.append("g")
          .attr("class", "y axis invisible-axis")
          .call(yAxis);

      container.insert("g")
          .attr("class", "x axis invisible-axis")
          .attr("transform", "translate(0," + chartH + ")")
          .call(xAxis);


      var extBarW = x0.rangeBand() - gapExternalBars;
      var intBarW = extBarW * (1 - gapInternalBars * 2);

      var externalBars = container.selectAll(".external-bar")
      .data(data)
      .enter()
      .append("rect")
      .classed("external-bar", true)
      .classed(analyticsClass, true)
      .attr({
        x: function(d){ return x0(d.label); },
        y: function(d) { return y0(d.values[1]); },
        width: extBarW,
        height: function(d) { return chartH - y0(d.values[1]); },
        fill: colors.external
      })
      .on("mousemove", function(d){
        tip.style("left", d3.event.pageX+10+"px");
        tip.style("top", d3.event.pageY-25+"px");
        tip.style("display", "inline-block");
        d.label < 12 ? tip.html((d.label) + " AM" + "<br>" + (d.values[1]) + " " + dataUnit) : tip.html((d.label) + " PM" + "<br>" + (d.values[1]) + " " + dataUnit);
      })
      .on("mouseout", function(d){
        tip.style("display", "none");
      });

      var internalBars = container.selectAll(".internal-bar")
      .data(data)
      .enter()
      .append("rect")
      .classed("internal-bar", true)
      .classed(analyticsClass, true)
      .attr({
        x: function(d){ return x0(d.label) + extBarW/2 - intBarW/2; },
        y: function(d) { return y0(d.values[0]); },
        width: intBarW,
        height: function(d) { return chartH - y0(d.values[0]); },
        fill: colors.internal
      })
      .on("mousemove", function(d){
        tip.style("left", d3.event.pageX+10+"px");
        tip.style("top", d3.event.pageY-25+"px");
        tip.style("display", "inline-block");
        d.label < 12 ? tip.html((d.label) + " AM" + "<br>" + (d.values[1]) + " " + dataUnit) : tip.html((d.label) + " PM" + "<br>" + (d.values[1]) + " " + dataUnit);
      })
      .on("mouseout", function(d){
        tip.style("display", "none");
      })
      .on("click", function(d, i) {
        svg.selectAll("rect").classed("_selected_", false);
        svg.selectAll("rect").classed("_notSelected_", true);

        d3.select(this).classed("_selected_", true);
        d3.select(this).classed("_notSelected_", false);

        selection = {label: d.label, subCategory: seriesNames[0]};
        onClickFunction(this, d.label, seriesNames[0]);
      });

      externalBars.on("click", function(d, i) {
        svg.selectAll("rect").classed("_selected_", false);
        svg.selectAll("rect").classed("_notSelected_", true);

        d3.select(this).classed("_selected_", true);
        d3.select(this).classed("_notSelected_", false);

        selection = {label: d.label, subCategory: seriesNames[1]};
        onClickFunction(this, d.label, seriesNames[1]);
      });


      updateData = function() {

        externalBars.data(data)
        .transition()
        .ease(ease)
        .duration(1000)
        .attr({
          y: function(d) { return y0(d.values[1]); },
          height: function(d) { return chartH - y0(d.values[1]); }
        });

        internalBars.data(data)
        .transition()
        .ease(ease)
        .duration(1000)
        .attr({
          y: function(d) { return y0(d.values[0]); },
          height: function(d) { return chartH - y0(d.values[0]); }
        });
      }

      if (showLegend){createLegend();}

      function createLegend() {
          items = legendContainer.selectAll(".legend-item")
              .data(seriesNames);

          items.enter().append("g")
              .classed("legend-item", true)
              .classed("_selected_", true)
              .attr({transform: "translate("+ (legendX) + "," + (legendY) + ")"})

          items.append("text").append('tspan').text(function(d, i) {
                  return seriesNames[i].toLocaleString("es-ES");
              })
              .attr("x", function(d, i) {
                  return 90 * i + 25;
              })
              .attr("y", function(d, i) {
                  return 10;
              })

          items.append("rect")
              .attr("x", function(d, i) {
                  return 90 * i + 10 ;
              })
              .attr("y", function(d, i) {
                  return 0;
              })
              .attr('width', '10')
              .attr('height', '10')
              .attr("fill", function(d, i) {
                  return (i == 0 ? colors.internal : colors.external);
              });
            };
    });
  }

  exports.width = function(_x) {
    if (!arguments.length) return width;
    width = parseInt(_x);
    return this;
  };

  exports.height = function(_x) {
    if (!arguments.length) return height;
    height = parseInt(_x);
    return this;
  };

  exports.legendX = function(_x){
    if (!arguments.length) return legendX;
    legendX = _x;
    return this
  }

  exports.legendY = function(_x) {
      if (!arguments.length) return legendY;
      legendY = _x;
      return this;
  };

  exports.data = function(_x) {
    if (!arguments.length) return data;
    data = _x;
    //Understand this function
    if (typeof updateData === 'function') updateData();
    //Return myself as object ?
    return this;
  };

  exports.max_value = function(){
    //There is another way to define a internal variable based on others variables??
    if(max_value){
      max_value = d3.max(this.data, function(d){ return d.values[1]; });
    }
    return max_value;
  };

  exports.colors = function(_x) {
    if (!arguments.length) return colors;
    colors = _x;
    return this;
  };

  exports.dataUnit = function(_x) {
    if (!arguments.length) return dataUnit;
    dataUnit = _x;
    return this;
  };

  exports.margin = function(_x) {
    if (!arguments.length) return margin;
    margin = _x;
    return this;
  };

  exports.onClickFunction = function(_x) {
    if (!arguments.length) return onClickFunction;
    onClickFunction = _x;
    return this;
  };

  exports.seriesNames = function(_x) {
    if (!arguments.length) return seriesNames;
    seriesNames = _x;
    return this;
  };

  exports.selection = function(_x){
    if (!arguments.length) return selection;
    selection = _x;
    if(svg) {
      svg.selectAll('rect._selected_').classed('_selected_', false);
      svg.selectAll('rect._notSelected_').classed('_notSelected_', false);
    }
    return this;
  };

  exports.analyticsClass = function(_x) {
    if (!arguments.length) return analyticsClass;
    analyticsClass = _x;
    return this;
  };

  return exports;

};
