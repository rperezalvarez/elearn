// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery-ui
//= require rails-ujs
//= require js.cookie
//= require jstz
//= require browser_timezone_rails/set_time_zone
//= require bootstrap-toggle
//= require ahoy
//= require i18n
//= require i18n.js
//= require i18n/translations
//= require charts/2nd/color_palette
//= require bootstrap-sprockets
//= require summernote

//= require turbolinks
//= require_tree .


/******************** ANALYTICS ********************/
var clickEventNames = {
  // Lateral menu clicks
  coursesMenuOpt: 'Menu_ChooseCourse',
  notesMenuOpt: 'Menu_ViewNotes',
  goalsMenuOpt: 'Menu_GoalSetting',

  // Courses dashboard selector clicks
  EPDashboardSel: 'Button_EngagementPerformance',
  EEDashboardSel: 'Button_Effectiveness',

  // EP dashboard clicks
  courseTimeUseTemporalFilter0: 'Button_FilterTime7Days',
  courseTimeUseTemporalFilter1: 'Button_FilterTime30Days',
  courseTimeUseTemporalFilter2: 'Button_FilterTimeViewAll',
  
  sessionsViewAll: 'Button_FilterViewAll',
  sessionsChart: 'Graph_TimeInSessions',
  timeUseChart: 'Graph_TimeUse',
  engagementByNumberByActivitiesChart: 'Graph_EngagementByNumberActivities',
  comparationToggleEnabled: 'Button_ComparisonEnabled', // Handled in engagement_performance.js
  comparationToggleDisabled: 'Button_ComparisonDisabled', // Handled in engagement_performance.js

  // EE dashboard clicks
  effectivenessByActivityTypeChart: 'Graph_EffectivenessActivityType',
  effectivenessByDafOfWeekChart: 'Graph_EffectivenessByDayWeek',
  effectivenessByHourOfDayChart: 'Graph_EffectivenessByHourDay',

  // Notes clicks
  downloadAllNotes: 'Button_DownloadAllNotes',
  searchNote: 'Button_SearchNote',
  editNote: 'Button_EditNote',
  downloadNote: 'Button_DownloadNote',
  deleteNote: 'Button_DeleteNote',
  newNote: 'Button_NewNote',

  // Goals clicks
  defineGoal: 'Button_DefineGoal'

};

var hoverEventNames = {
  courseTimeUse: 'Graph_TimeUseOnCourse',
  engagementByActivityTypeChart: 'Graph_EngagementByTypeActivities'
};
/****************************************************/


$(document).on('turbolinks:load', function() {
  // Notes editor setup
  $('[data-provider="summernote"]').each(function(){
    $(this).summernote({
      height: 400,
      toolbar: [
        ['fontsize', ['style', 'fontsize']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['superscript', 'subscript']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['insert', ['link', 'table', 'hr']],
        ['misc', ['undo', 'redo']]
      ]
    });
  });

  // Enable tooltips
  $('[data-toggle="tooltip"]').tooltip();

  $(".chart-info").hover(function(elem) {
    elem.show();
  }); 
  
  
});

$(document).on('click', '[class*=analytics-click]', function() {
  var elemClasses = $(this).attr('class').split(/\s+/);
  var elemClass = _.find(elemClasses, function(className) { return className.includes('analytics-click')});
  
  if(elemClass) {
    var elemId = elemClass.split('-');
    
    if(elemId.length == 3) {
      var eventId = elemId[2];
      var eventName = clickEventNames[eventId]; 
      if(eventName) {
        var courseId = getCourseIdFromUrl();
        
        if(courseId < 0) {
          courseId = parseInt($(this).data().courseId);
        } 

        if(courseId != null && courseId >= 0) {
          ahoy.track(eventName, {course_id: courseId});
        } else {
          ahoy.track(eventName);
        }
  
      } else {
        console.log('Element is not associated to an event:', this)
      }
    } else {
      console.log('Element does not have an event structure:', this);
    }
  } else {
    console.log('Element does not have an event related class:', this);
  }
  
});


function getCourseIdFromUrl() {
  var url = window.location.href;
  var matchCourseUrl = url.match(/\/courses\/\d+\//);

  if(matchCourseUrl != null) {
    return parseInt(matchCourseUrl[0].match(/\d+/)[0]);
  } else {
    return -1;
  }
}
