function DataProcessor(raw_data){
    this.raw_data = raw_data;
    this.cf = crossfilter(raw_data);
    this.data = null;
}

DataProcessor.prototype.reduceInitial = function(){
    return { title: "", subtitle: "", ranges: [0,0,0],measures: [0], markers:[0] };
};

DataProcessor.prototype.reduceAdd = function (p, v) {
    //{"title":"Revenue","subtitle":"US$, in thousands","ranges":[150,225,300],"measures":[220,270],"markers":[250]}
    p.title = v.day_of_week_humanize;
    p.ranges[2] += increame_by_status(v.status);
    p.measures[0] += increame_by_status(v.status)*Math.random()/2;

    return p;
};

DataProcessor.prototype.reduceRemove = function (p, v) {
    p.title = v.day_of_week;
    p.ranges[2] -= increame_by_status(v.status);
    p.measures[0] -= increame_by_status(v.status)*Math.random()/2;

    return p;
};

DataProcessor.prototype.processRawData = function(){
    //process raw data with crossfilter


    //How to use the functions declared above
    return this.data;
};