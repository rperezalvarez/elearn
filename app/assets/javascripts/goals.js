$(document).on('change', 'select#week, select#course', function(e) {
  changeWeek();
});


$(document).on('turbolinks:load', function() {
  if($('select#week').length) {
    changeWeek();
  }
});

function changeWeek() {
  courseSelector = $('#course');
  weekSelector = $('#week');

  courseID = courseSelector.val();
  week = weekSelector.val();

  $.ajax({
    type: "GET",
    url: "/goals/change_week",
    dataType: 'script',
    data: {course_id: courseID, week: week}
  });
}

function drawGoalsChart(div, height, data, dataUnit) {
  // translate labels 
  data.forEach(function(item) {
    item.label = I18n.t(`graphs.resourceType.${item.label}`)
  });

  if($(div).length) {
    var goalsChart = d3.cloudshapes.horizontalRowChart()
    .width($(div).parent().width())
    .height(height)
    .data(data)
    .dataUnit(I18n.t(`graphs.dataUnit.${dataUnit}`))
    .colors({ internal: colorPalette[I18n.t('graphs.colors.Completed')], external: colorPalette[I18n.t('graphs.colors.Goal')] })
    .seriesNames([I18n.t('graphs.colors.Completed'), I18n.t('graphs.colors.Goal')])
    .selectable(false);

    d3.select(div).call(goalsChart);
  }
}
