class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action :set_beginning_of_week
  before_action :set_locale

  def after_sign_in_path_for(resource_or_scope)
    courses_path
  end

  def format_duration(sec)
    "#{(sec/3600).to_s.rjust(2,'0')}:#{(sec%3600.0/60.0).round.to_s.rjust(2,'0')}"
  end


  private

  def set_beginning_of_week
    Date.beginning_of_week = :monday
    DateTime.beginning_of_week = :monday
  end

  def set_locale
    I18n.locale = current_user.locale if current_user
  end

  def default_url_options
    { locale: I18n.locale }
  end


  protected
    def configure_permitted_parameters
       devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :country, :level_education, :gender, :agreement_accepted, :password, :password_confirmation])
    end

    def process_token
      token = params[:token]

      if token.present? && check_token?(token)
        #TODO: Add the case and check that belong to the user.
        sign_out current_user if current_user.present?
        user = User.find(auth_token[:user_id].to_i)
        sign_in(:user, user)
      else
        flash[:error] = "Your token is invalid, please put in contact by email."
        redirect_to new_user_session_path
      end
    end

    def auth_token
      @auth_token ||= JsonWebToken.decode(@token)
    end

    def check_token?(token)
      @token = token
      auth_token && auth_token[:user_id].to_i
    end
end
