class GoalsController < ApplicationController
  #before_action :set_goal, only: [:show, :edit, :update, :destroy]
  #before_action :set_parameters, only: [:index]

  # GET /goals
  # GET /goals.json
  def index
    @courses = current_user.courses
    @course ||= @courses.first

    if @course.present?

      @weeks = @course.weeks
      
      @enrollment = Enrollment.joins(edition: :course).where(courses:{id: @course.id},user_id:current_user.id).first
      @current_week = @enrollment.current_week
      @goal = Goal.create_or_initialize_by_params(current_user,@enrollment,@current_week)
      @goal.build_goal_details unless @goal.persisted?
      
      if @goal.persisted?
        set_charts_data
      else
        set_kpis(@enrollment, @current_week)
      end
    end

  end


  def change_week
    @course = Course.find(params[:course_id])
    @enrollment = Enrollment.joins(edition: :course).where(courses:{id: @course.id},user_id:current_user.id).first
    @current_week  = params[:week].to_i

    @goal = Goal.create_or_initialize_by_params(current_user,@enrollment,@current_week)
    @goal.build_goal_details unless @goal.persisted?
    
    set_kpis(@enrollment, @current_week)

    if @goal.persisted?
      set_charts_data
    end

    respond_to do |format|
      format.js {}
    end
  end

  # GET /goals/1
  # GET /goals/1.json
  def show
  end

  # GET /goals/new
  def new
    @goal = Goal.new
  end

  # GET /goals/1/edit
  def edit
  end

  # POST /goals
  # POST /goals.json
  def create
    @goal = Goal.create_by_params(goal_params.merge({user_id:current_user.id}))

    respond_to do |format|
      if @goal.persisted?
        set_goal_elements
        set_charts_data
        format.js{}
        format.json { render :show, status: :created, location: @goal }
      else
        format.html { render :new }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_goal
      @goal = Goal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def goal_params
      params.require(:goal)
    end

    def set_goal_elements
      @enrollment = @goal.enrollment
      @course = @enrollment.edition.course
      @current_week = @enrollment.current_week
    end

    def set_charts_data
      @activities_chart_data = [
        { 'label': 'Videos', 'values': [@enrollment.videos_watched(@current_week), @goal.goal_videos] },
        { 'label': 'Evaluations', 'values': [@enrollment.exams_done(@current_week), @goal.goal_evaluations]}
      ]
      @time_chart_data = [{ label: 'Time invested', values: [@enrollment.time_invested(@current_week)/(60.0*60.0), @goal.goal_hours] }]

    end

    def set_kpis(enrollment, week)
      videos_watched = enrollment.videos_watched(week)
      exams_done = enrollment.exams_done(week)
      avg_videos = enrollment.avg_videos_watched.round(1)
      avg_exams = enrollment.avg_exams_done.round(1)
      avg_time_week = (enrollment.avg_time_invested_per_week/60.0).round(1)
      avg_time_exam = (enrollment.avg_time_invested_per_week(Resource::CATEGORIES[:evaluations])/60.0).round(1)
      avg_time_videos = (enrollment.avg_time_invested_per_week(Resource::CATEGORIES[:lectures])/60.0).round(1)

      most_effective_day = enrollment.most_effective_day
      
      total_videos_this_week = enrollment.edition.course.resources.where(category: Resource::CATEGORIES[:lectures], module: week-1).distinct.count
      total_evals_this_week = enrollment.edition.course.resources.where(category: Resource::CATEGORIES[:evaluations], module: week-1).distinct.count
      total_time_videos_week = enrollment.edition.course.resources.where(category: Resource::CATEGORIES[:lectures], module: week-1).distinct.sum(:duration)
      total_time_evals_week = enrollment.edition.course.resources.where(category: Resource::CATEGORIES[:evaluations], module: week-1).distinct.sum(:duration)

      avg_completers_kpis = enrollment.avg_completers(week-1)
      
      avg_videos_completers = avg_completers_kpis[0].round(1)
      avg_evals_completers = avg_completers_kpis[1].round(1)
      avg_time_completers = avg_completers_kpis[2].round(1)

      @categorized_kpis = [
        { category: I18n.t("goals.categories.course_performance_previous_week"),
          kpis: [
            { value: videos_watched, msg: I18n.t("goals.statistics.videos_last_week")},
            { value: exams_done, msg: I18n.t("goals.statistics.exams_last_week")}
          ]
        },
        { category: I18n.t("goals.categories.course_performance"),
          kpis: [
            { value: most_effective_day, msg: I18n.t("goals.statistics.most_effective_day")},
            { value: avg_videos, msg: I18n.t("goals.statistics.avg_weekly_videos")},
            { value: avg_exams, msg: I18n.t("goals.statistics.avg_weekly_exams")},
            { value: avg_time_week.to_s + " " + I18n.t('graphs.dataUnit.min'), msg: I18n.t("goals.statistics.avg_weekly_time")},
            { value: avg_time_exam.to_s + " " + I18n.t('graphs.dataUnit.min'), msg: I18n.t("goals.statistics.avg_weekly_time_exams")},
            { value: avg_time_videos.to_s + " " + I18n.t('graphs.dataUnit.min'), msg: I18n.t("goals.statistics.avg_weekly_time_videos")},
          ]
        },
        { category: I18n.t("goals.categories.activities_this_week"),
          kpis: [
            { value: total_videos_this_week, msg: I18n.t("goals.statistics.this_week_videos")},
            { value: total_evals_this_week, msg: I18n.t("goals.statistics.this_week_exams")},
            { value: total_time_videos_week.to_s + " " + I18n.t('graphs.dataUnit.min'), msg: I18n.t("goals.statistics.this_week_videos_time")},
            { value: total_time_evals_week.to_s + " " + I18n.t('graphs.dataUnit.min'), msg: I18n.t("goals.statistics.this_week_exams_time")},
          ]
        },
        { category: I18n.t("goals.categories.other_students_performance"),
          kpis: [
            { value: avg_videos_completers, msg: I18n.t("goals.statistics.avg_videos_completers")},
            { value: avg_evals_completers, msg: I18n.t("goals.statistics.avg_exams_completers")},
            { value: avg_time_completers.to_s + " " + I18n.t('graphs.dataUnit.min'), msg: I18n.t("goals.statistics.avg_time_completers")}
          ]
        }
      ]
    end
end
