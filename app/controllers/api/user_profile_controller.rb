class Api::UserProfileController < Api::ApiController
  before_action :authenticate_request!

  def details
    render json: {name: current_user.name, email: current_user.email}, status: :ok
  end

  def edit
    user = current_user
    if params[:user_name]
      user.name = params[:user_name]
    end

    if params[:email]
      user.email = params[:email]
    end

    if params[:old_password] and params[:new_password]
      if user.valid_password?(params[:old_password])
        user.password = params[:new_password]
        user.password_confirmation = params[:new_password]
      else
        return render json: {error: "Changes couldn't be saved. Password is incorrect."}, status: :unauthorized
      end
    end

    if user.save
      render json: {name: user.name, email: user.email}, status: :ok
    else
      render json: {error: "Changes couldn't be saved. " + user.errors.full_messages.to_s}, status: :internal_server_error
    end

  end

  def destroy
    if params[:password]
      if current_user.valid_password?(params[:password])
        current_user.destroy
        render json: {success: "User deleted successfully"}, status: :ok
      else
        return render json: {error: "Changes couldn't be saved. Password is incorrect."}, status: :unauthorized
      end
    else
      return render json: {error: "Missing params"}, status: :internal_server_error
    end
  end

end
