class Api::SessionsController < Api::ApiController
  before_action :authenticate_request!

  def start
    last_session = Session.from_user(current_user).order(:start).last

    if last_session.present?
      if last_session.duration.nil?
        return render json: last_session.to_json, status: :ok
      end
    end

    new_session = Session.new(user: current_user, start: DateTime.now)

    if new_session.save!
      return render json: new_session.to_json, status: :ok
    else
      return render json: {error: "Could not start a new session"}, status: :internal_server_error
    end

  end


  def get_current
    last_session = Session.from_user(current_user).order(:start).last

    if last_session.present?
      if last_session.duration.nil?
        if last_session.session_resources.present?
          if last_session.session_resources.order(:finish).last.present?
            if (DateTime.now - last_session.session_resources.order(:finish).last.finish.to_datetime).to_f*24*60*60 > AppConfiguration.max_inactive_time
              last_session.close
              return render json: {error: "Could not find an active session"}, status: :internal_server_error
            else
              return render json: last_session.to_json, status: :ok
            end
          else
            return render json: {error: "Could not find an active session"}, status: :internal_server_error
          end
        else
          if (DateTime.now - last_session.start.to_datetime).to_f*24*60*60 > AppConfiguration.max_inactive_time
            last_session.close
            return render json: {error: "Could not find an active session"}, status: :internal_server_error
          else
            return render json: last_session.to_json, status: :ok
          end
        end
      else
        return render json: {error: "Could not find an active session"}, status: :internal_server_error
      end
    else
      return render json: {error: "Could not find an active session"}, status: :internal_server_error
    end

  end


  def close
    current_session = Session.from_user(current_user).order(:start).last

    if current_session.present?
      if current_session.duration.nil?
        if current_session.effective_time < AppConfiguration.min_session_effective_time_for_save
          current_session.destroy!
          return render json: {success: "Session had an effective time less than 5 minutes so it was deleted."}, status: :ok
        else
          current_session.duration = (Time.now - current_session.start).round
          if current_session.save!
            render json: current_session.to_json, status: :ok
          else
            render json: {error: "Could not close the session"}, status: :internal_server_error
          end
        end
      else
        render json: {error: "Session has already been closed"}, status: :unauthorized
      end
    else
      render json: {error: "Session doesn't exists"}, status: :unauthorized
    end

  end

end
