class Api::NoteController < Api::ApiController
  before_action :authenticate_request!

  def save_visited_url

    if params[:url] and params[:active_time] and params[:start] and params[:finish]
      # new_url = VisitedUrl.new(url: params[:url], start: params[:start], finish: params[:finish], user: current_user)
      new_session = Session.new(user: current_user, start: params[:start].to_datetime, duration: params[:active_time])

      if SessionResource.build_from_url(new_session, params[:start].to_datetime, params[:finish].to_datetime, params[:active_time], params[:url])
        render json: {success: ["URL was successfully saved."]}, status: :created
      else
        render json: {errors: ["Couldn't save the URL, try again."]}, status: 404
      end
    else
      render json: {errors: ['Expecting params url, active_time, start and finish.']}, status: 500
    end
  end

  def notes
    @notes = current_user.notes

    if @notes
      render json: {notes: @notes}, status: :created
    else
      render json: {errors: ["The notes couldnt be founded"]}, status: 500
    end
  end

  def get_note
    code = params[:code]
    @note = Note.find_by_code(code)
    if @note
      render json: @note, status: :created
    else
      render json: {errors: @note.errors().to_json}, status: 500
    end
  end

  def save_note
    title = params[:title]
    course_id = params[:course_id]
    body = params[:body]

    @note = Note.new(title:title, course_id: course_id, body:body,user_id:current_user.id)

    if @note.save()
      render json: @note.to_json(only: [:title, :body, :code]), status: :created
    else
      render json: {errors: @note.errors().to_json}, status: 500
    end
  end

end
