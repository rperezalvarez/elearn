class Api::CoursesController < Api::ApiController
  before_action :authenticate_request!

  def index
    @courses = current_user.courses.order(full_name: :asc)

    render json: {courses: @courses}, status: :ok
  end
end
