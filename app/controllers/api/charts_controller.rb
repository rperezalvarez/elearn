class Api::ChartsController < Api::ApiController
  before_action :authenticate_request!

  def get_effective_time
    user_sessions = Session.from_user(current_user).order(:start)

    if user_sessions.present?
      if user_sessions.last.duration.nil?

        session = user_sessions.last

        procrastination_time = session.procrastination_time
        effective_time = session.effective_time

        render json: {effective_time: effective_time, procrastination_time: procrastination_time, session_start: (session.start.to_f*1000).to_i}, status: :ok
      else
        render json: {error: "Could not find an active session"}, status: :unauthorized
      end
    else
      render json: {error: "Could not find an active session"}, status: :unauthorized
    end
  end
end
