class Api::UserAuthenticationController < Api::ApiController

  def register_user
    if params[:auth]
      if params[:auth][:email] and params[:auth][:name] and params[:auth][:country] and params[:auth][:gender] and params[:auth][:level_education] and params[:auth][:password] and params[:auth][:password_confirmation] and params[:auth][:agreement_accepted]
        if params[:auth][:password] != params[:auth][:password_confirmation]
          render json: {errors: ['Password must be the same']}, status: 500
          return
        end
        if !params[:auth][:agreement_accepted]
          render json: {errors: ['You must accept the terms and conditions']}, status: 500
          return
        end
        user = User.find_by_email(params[:auth][:email])
        if user
          render json: {errors: ['Email is already being used by another user']}, status: :unauthorized
        else
          User.create(email: params[:auth][:email], name: params[:auth][:name], country: params[:auth][:country], gender: params[:auth][:gender], level_education: params[:auth][:level_education], password: params[:auth][:password], agreement_accepted: params[:auth][:agreement_accepted])
          render json: {success: ['User signed up successfully']}, status: 201
        end
      else
        render json: {errors: ['Expecting more params']}, status: 500
      end
    else
      render json: {errors: ['Expecting more params']}, status: 500
    end
  end

  def authenticate_user
    user = User.find_for_database_authentication(email: params[:email])
    if user
      if user.valid_password?(params[:password])
        render json: payload(user)
      else
        render json: {errors: ['Invalid Username/Password']}, status: :unauthorized
      end
    else
      render json: {errors: ['Email is not registered']}, status: :unauthorized
    end
  end

  def validate_token
    unless user_id_in_token?
      render json: { errors: ['Token not valid'] }, status: :unauthorized
      return
    end
    user = User.find(auth_token[:user_id])
    render json: user, status: 200
    return
  end

  private

  def payload(user)
    return nil unless user and user.id
    {
      auth_token: JsonWebToken.encode({user_id: user.id}),
      user: {id: user.id, email: user.email}
    }
  end

end
