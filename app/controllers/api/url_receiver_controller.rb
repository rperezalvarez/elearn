class Api::UrlReceiverController < Api::ApiController
  before_action :authenticate_request!

  # Loads other LMS url processors
  Dir[Rails.root.join('lib', 'url_processors', '*.rb')].each { |file| require file }
  APP_DOMAIN = Rails.application.config.app_variables[:domain]

  def save_visited_url

    if params[:url] and params[:active_time] and params[:start] and params[:finish] and params[:session_id]
      # new_url = VisitedUrl.new(url: params[:url], start: params[:start], finish: params[:finish], user: current_user)
      user_sessions = Session.from_user(@current_user).order(:start)
      current_session_id = user_sessions.present? ? user_sessions.last.id : nil
      session = Session.find_by_id(params[:session_id])
      if session.present? and session.duration.nil?
        lastResourceTime = session.session_resources.present? ? session.session_resources.effective.order(:finish).last.finish : session.start
        diferenceMin =  (Time.now - lastResourceTime)

        if diferenceMin <= AppConfiguration.max_inactive_time

          success = false
          # if is_dashboard_time? params[:url]
          #   success = save_as_help_time(session, params[:start].to_datetime, params[:finish].to_datetime, params[:active_time], params[:url])
          # else
          # end
          case params[:url]
          when /#{UrlProcessors::Coursera.base_url}/
            # Url belongs to Coursera
            success = UrlProcessors::Coursera.build_from_url(session, params[:start].to_datetime, params[:finish].to_datetime, params[:active_time], params[:url])
          when /#{APP_DOMAIN}/
            # Url belongs to the dashboard
            success = save_as_help_time(session, params[:start].to_datetime, params[:finish].to_datetime, params[:active_time], params[:url])
          else
            # Url belongs to procrastination
            success = save_as_procrastination_time(session, params[:start].to_datetime, params[:finish].to_datetime, params[:active_time], params[:url])
          end

          if success
            render json: {success: ["URL was successfully saved."], sessionId: current_session_id}, status: :created
          else
            render json: {errors: ["Couldn't save the URL, try again."], sessionId: current_session_id}, status: 404
          end
        else
          @current_user.close_active_study_sessions
          render json: {success: ["URL was not saved, because the study session is already closed"], sessionId: session.id}, status: 204
        end
      else
        # In this case we lie to the plugin about the resouce being saved :(
        render json: {success: ["URL was not saved, because the study session is already closed"], sessionId: nil}, status: 204
      end

    else
      render json: {errors: ['Expecting params url, active_time, start and finish.']}, status: 500
    end
  end




  private

    def save_as_help_time(session, start, finish, active_time, url)
      if session.session_resources.present?
        sr = SessionResource.new()
        sr.start = start
        sr.finish = finish
        sr.active_time = active_time
        sr.session = session
        sr.url = url

        last_effective_session_resource = session.session_resources.joins(:resource).where("resources.course_id IS NOT NULL AND resources.resource_type != ?", Resource::RESOURCE_TYPES[:others][:procrastination]).order(:finish).last
        last_course = last_effective_session_resource.present? ? last_effective_session_resource.resource.course : nil

        if last_course.present?
          help_resource = Resource.find_by(category: Resource::CATEGORIES[:complements], resource_type: Resource::RESOURCE_TYPES[:complements][:help], course: last_course)
          help_resource = Resource.create(category: Resource::CATEGORIES[:complements], resource_type: Resource::RESOURCE_TYPES[:complements][:help], course: last_course) unless help_resource.present?

          sr.resource = help_resource

          return sr.save
        else
          return false
        end
      else
        return false
      end
    end

    def save_as_procrastination_time(session, start, finish, active_time, url)
      if session.session_resources.present?
        sr = SessionResource.new()
        sr.start = start
        sr.finish = finish
        sr.active_time = active_time
        sr.session = session
        sr.url = url

        last_effective_session_resource = session.session_resources.joins(:resource).where("resources.course_id IS NOT NULL AND resources.resource_type != ?", Resource::RESOURCE_TYPES[:others][:procrastination]).order(:finish).last
        last_course = last_effective_session_resource.present? ? last_effective_session_resource.resource.course : nil

        if last_course.present?
          procrastination_resource = Resource.find_by(category: Resource::CATEGORIES[:others], resource_type: Resource::RESOURCE_TYPES[:others][:procrastination], course: last_course)
          procrastination_resource = Resource.create(category: Resource::CATEGORIES[:others], resource_type: Resource::RESOURCE_TYPES[:others][:procrastination], course: last_course) unless procrastination_resource.present?

          sr.resource = procrastination_resource

          return sr.save
        else
          return false
        end
      else
        return false
      end
    end

end
