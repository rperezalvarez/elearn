class DashboardController < ApplicationController
  before_action :authenticate_user!

  # CHECK
  def engagement_performance_data
    # puts 'Time 1: ' + Time.now.strftime('%H:%M:%S.%L').to_s
    @week = params[:week].to_i
    @course = Course.find_by_id(params[:course_id])
    user_enrollment = Enrollment.from_user(current_user).from_course(@course).last
    @date_enrolled = user_enrollment.date_enrolled
    @weeks_on_course = user_enrollment.current_week
    @week_aggregation = params[:week] == 'view all'

    date_start_last_edition = user_enrollment.edition.start

    if @week_aggregation
      @start_date = [@date_enrolled.to_datetime.beginning_of_day, date_start_last_edition].max
      @end_date = Date.today.to_datetime.end_of_day
    else
      @start_date = [(@date_enrolled + 7*(@week-1)).to_datetime.beginning_of_day, date_start_last_edition].max
      @end_date = (@start_date + 6.days).to_datetime.end_of_day
    end

    # Show only sessions from last course edition
    @user_sessions = Session.from_user(current_user).from_course(@course).order(:start)

    @avg_session_time = @user_sessions.present? ? SessionResource.joins(:resource).where("session_resources.session_id IN (?) AND resources.course_id = ?", @user_sessions.map(&:id), @course).sum(:active_time)/(@user_sessions.distinct.count*60.0) : 0
    time_by_day = Session.joins(:resources).where("session_resources.session_id IN (?) AND resources.course_id = ?", @user_sessions.map(&:id), @course).group("sessions.start").sum("session_resources.active_time")
    times = time_by_day.map{ |td| td[1] }
    @avg_day_time = time_by_day.present? ? times.sum/(time_by_day.count * 60.0) : 0

    @sessions = @user_sessions.where(start: @start_date..@end_date).distinct

    data1 = []
    data2 = []
    data = []

    user_resource_statuses = user_enrollment.resource_statuses

    # puts 'Time 2: ' + Time.now.strftime('%H:%M:%S.%L').to_s
    user_data_length = 0
    @sessions.each do |session|
      session_week = (user_enrollment.day_of_course(session.start.to_date)-1)/7 + 1
      day_of_week = @week_aggregation ? session_week : change_first_day_week(session.start)
      day_of_week_humanize = @week_aggregation ? (I18n.t('week').capitalize + ' ' + session_week.to_s) : (I18n.t('date.day_names')[session.start.wday]).capitalize

      session_sr = session.session_resources.joins(:resource).where("resources.course_id = ?", @course)
      data1 = data1 + session_sr.map{|session_resource| {is_comparation: false,
              is_completer: false,
              session_id: session.id,
              resource_id: session_resource.resource.id,
              day_of_week: day_of_week,
              day_of_week_humanize: day_of_week_humanize,
              resource_type: I18n.t("graphs.resourceType.#{session_resource.resource.resource_type.capitalize}"),
              procrastination: session_resource.resource.resource_type == Resource::RESOURCE_TYPES[:others][:procrastination] ? true : false,
              # status: I18n.t("graphs.resourceStatus.#{session_resource.resource.where(resource_type: ['procrastination', 'discussions', 'info'])  ? ResourceStatus::STATUS[:not_started] : user_resource_statuses.from_resource(session_resource.resource).first.status_at_end_of(session)}"),
              # status: I18n.t("graphs.resourceStatus.#{(['procrastination', 'discussions', 'info'].include? session_resource.resource.resource_type)  ? ResourceStatus::STATUS[:not_started] : user_resource_statuses.from_resource(session_resource.resource).first.status_at_end_of(session)}"),              
              status: I18n.t("graphs.resourceStatus.#{([Resource::RESOURCE_TYPES[:evaluations][:exam], Resource::RESOURCE_TYPES[:lectures][:lecture]].include? session_resource.resource.resource_type)  ? user_resource_statuses.from_resource(session_resource.resource).first.status_at_end_of(session) : ResourceStatus::STATUS[:not_started]}"),                            
              time_invested: session_resource.active_time/60.0,
              time_required: session_resource.resource.duration.nil? ? 0 : session_resource.resource.duration,
              user_id: session.user_id
              }}
    end

    # puts 'Time 3: ' + Time.now.strftime('%H:%M:%S.%L').to_s
    @other_students_data = ActivityDetailView.find_by_sql("
      SELECT student_course_state, course_item_type_desc, state, COUNT(*) as activities_number, SUM(total_time_spent) as time_invested
      FROM activity_detail_view
      WHERE course_id_lms = '#{@course.id_lms}'
      AND week = #{@week-1}
      GROUP BY student_course_state, course_item_type_desc, state")

    # puts 'Time 4: ' + Time.now.strftime('%H:%M:%S.%L').to_s

    data2 = @other_students_data.map{|entry| {
            is_comparation: true,
            is_completer: entry.student_course_state == 'not_completed' ? false : true,
            session_id: nil,
            resource_id: nil,
            day_of_week: nil,
            day_of_week_humanize: nil,
            resource_type: I18n.t("graphs.resourceType.#{(Resource::MATCH_RESOURCE_TYPE[entry.course_item_type_desc] || entry.course_item_type_desc).capitalize}") ,
            procrastination: false,
            status: I18n.t("graphs.resourceStatus.#{entry.state.capitalize}"),
            time_invested: entry.time_invested.to_i/60.0,
            number_of_activities: entry.activities_number,
            time_required: 0
            }}

    # puts 'Time 5: ' + Time.now.strftime('%H:%M:%S.%L').to_s

    # Build sessions labels
    if @week_aggregation
      @sessions_labels = (1..@weeks_on_course).map{|d| I18n.t('week').capitalize + ' ' + d.to_s }
    else
      @sessions_labels = (0..6).map{|d| (I18n.t('date.day_names')[(d+1)%7]).capitalize }
    end

    other_users = ActivityDetailView.where(course_id_lms: @course.id_lms, week: @week-1)
    number_of_completers = other_users.where("student_course_state != 'not_completed'").pluck(:id_user_lms).uniq.count
    number_of_no_completers = other_users.where("student_course_state = 'not_completed'").pluck(:id_user_lms).uniq.count

    # puts 'Time 6: ' + Time.now.strftime('%H:%M:%S.%L').to_s
    data = data1 + data2
    respond_to do |format|
      format.json {
        render :json => Oj.dump({ data: data, avg_session_time: Array.new(@sessions_labels.length, @avg_session_time), avg_day_time: Array.new(@sessions_labels.length, @avg_day_time), sessions_labels: @sessions_labels, number_of_other_students: {completers: number_of_completers, no_completers: number_of_no_completers}}, mode: :compat)
      }
    end
  end

  # CHECK
  def effectiveness_efficiency_data

    @course = Course.find_by_id(params[:course_id])
    user_enrollment = Enrollment.from_user(current_user).from_course(@course).last
    @resource_statuses = ResourceStatus.where(enrollment: user_enrollment, resource: @course.resources).where.not(status: ResourceStatus::STATUS[:not_started])

    data = []
    counterCompleted = 0
    counterStarted = 0
    @resource_statuses.each do |resource_status|
      date = resource_status.status == ResourceStatus::STATUS[:completed] ? resource_status.date_completed : resource_status.date_started
      # First we add the started entry
      data << {
        is_comparation: false,
        resource_id: resource_status.resource.id,
        date: date.strftime("%y/%m/%d"),
        day_of_week: change_first_day_week(date),
        day_of_week_humanize: (I18n.t('date.day_names')[date.wday]).capitalize,
        hour: date.hour,
        resource_type: I18n.t("graphs.resourceType.#{resource_status.resource.resource_type.capitalize.pluralize}"),
        status: resource_status.status == ResourceStatus::STATUS[:completed] ? I18n.t('graphs.resourceStatus.Completed') : I18n.t('graphs.resourceStatus.Not_completed')
      }
    end

    respond_to do |format|
      format.json {
        render :json => data.to_json
      }
    end
  end

  # CHECK
  def progress_and_course_time_use
    @course = Course.find_by_id(params[:course_id])
    user_enrollment = Enrollment.from_user(current_user).from_course(@course).last
    @date_enrolled = user_enrollment.date_enrolled


    # Progress data
    progressData = Hash.new()

    progressData['required_lectures'] = @course.resources.where(category: Resource::CATEGORIES[:lectures]).count
    progressData['required_evaluations'] = @course.resources.where(category: Resource::CATEGORIES[:evaluations]).count
    progressData['completed_lectures'] = ResourceStatus.where(enrollment: user_enrollment, resource: @course.resources.where(category: Resource::CATEGORIES[:lectures]), status: ResourceStatus::STATUS[:completed]).count
    progressData['completed_evaluations'] = ResourceStatus.where(enrollment: user_enrollment, resource: @course.resources.where(category: Resource::CATEGORIES[:evaluations]), status: ResourceStatus::STATUS[:completed]).count


    # Time data

    timeData = Hash.new()
    (@date_enrolled..Date.today).each do |date|
      timeData[date.to_s] = Hash.new()
      timeData[date.to_s]['effective'] = 0
      timeData[date.to_s]['procrastination'] = 0
    end

    @sessions = Session.from_user(current_user).from_course(@course).where(start: user_enrollment.date_enrolled.beginning_of_day..Date.today.end_of_day).order(start: :asc).distinct

    @sessions.each do |session|
      session.session_resources.each do |session_resource|
        if session_resource.resource.resource_type == Resource::RESOURCE_TYPES[:others][:procrastination]
          timeData[session.start.to_date.to_s]['procrastination'] += session_resource.active_time/60.0
        else
          timeData[session.start.to_date.to_s]['effective'] += session_resource.active_time/60.0
        end
      end
    end

    respond_to do |format|
      format.json {
        render :json => {progress: progressData, timeUseOverCourse: timeData, originalTimeUse: timeData}.to_json
      }
    end
  end

  # CHECK
  def required_activities
    @week = params[:week].to_i
    @course = Course.find_by_id(params[:course_id])
    @week_aggregation = params[:week] == 'view all'

    user_enrollment = Enrollment.from_user(current_user).from_course(@course).last
    @active_module = user_enrollment.current_week - 1

    data = []

    if @week_aggregation
      data = @course.resources.where(module: 0..@active_module).select(:id, :resource_type, :duration)
    else
      data = @course.resources.where(module: @week - 1).select(:id, :resource_type, :duration)
    end

    respond_to do |format|
      format.json {
        render :json => data.to_json
      }
    end

  end

  private
  def change_first_day_week(datetime)
    #TODO: Use just as reference, look how to configurate rails to use monday as first day of week
    (datetime.wday+6)%7
  end

end
