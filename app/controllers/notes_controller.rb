class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]

  # GET /notes
  # GET /notes.json
  def index
    @notes = Note.from_user(current_user).search(params[:search]).order(created_at: :desc).page params[:page]
  end

  # GET /notes/1
  # GET /notes/1.json
  def show
  end

  def new
    @note = Note.new
  end

  def create
    @note = Note.new(note_params)
    @note.user = current_user

    respond_to do |format|
      if @note.save
        format.html { redirect_to notes_path, notice: 'Note was successfully created.' }
        format.json { render :show, status: :created, location: @note }
      else
        format.html { render :new }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /notes/1/edit
  def edit
  end

  # PATCH/PUT /notes/1
  # PATCH/PUT /notes/1.json
  def update
    respond_to do |format|
      if @note.update(note_params)
        format.html { redirect_to notes_url, notice: 'Note was successfully updated.' }
        format.json { render :show, status: :ok, location: @note }
      else
        format.html { render :edit }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notes/1
  # DELETE /notes/1.json
  def destroy
    @note.destroy
    respond_to do |format|
      format.html { redirect_to notes_url, notice: 'Note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def render_notes
    if params[:note_id]
      @notes = Note.find(params[:id])
    else
      @notes = Note.all.where(user_id: current_user)
    end
  end

  def download
    if params[:id] != ""
      @notes = Note.where(id: params[:id])
    else
      @notes = Note.where(user_id: current_user).order(:created_at)
    end

    html = render_to_string(:template => 'notes/render_notes.html.erb', layout: false).encode("UTF-8") 

    pdf = WickedPdf.new.pdf_from_string(html, :encoding => 'UTF-8') 
    if @notes.length == 1
      title = @notes.first.title
      send_data(pdf, 
          :filename    => "#{title}.pdf", 
          :encoding => "utf8",          
          :disposition => 'attachment')
    else
      title = 'all_notes'
      send_data(pdf,
          :filename => "#{title}.pdf",
          :encoding => "utf8",          
          :disposition => 'attachment')
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def note_params
      params.require(:note).permit(:title, :course_id, :body)
    end
end
