class CoursesController < ApplicationController
  before_action :set_course, except: [:sing_in_by_token, :index]
  before_action :authenticate_user!, except: :sing_in_by_token
  before_action :process_token, only: [:sing_in_by_token]

  # CHECK
  def sing_in_by_token
    redirect_to action: "index"
  end

  # CHECK
  def index
    @courses = Course.from_user(current_user).uniq
  end

  # CHECK
  def engagement_performance
    user_enrollment = Enrollment.from_user(current_user).from_course(@course).last
    @weeks_on_course = user_enrollment.current_week
    @course_duration = @course.duration
    @temporal_options_time_use = ['Last 7 days', 'Last 30 days', 'View all'];
    @active_week = @course_duration + Course::OFF_SET < @weeks_on_course ? (@course_duration + Course::OFF_SET) : @weeks_on_course

    date_start_last_edition = user_enrollment.edition.start
    @date_enrolled = user_enrollment.date_enrolled
    @start_date = [@date_enrolled.to_datetime.beginning_of_day, date_start_last_edition].max
    @end_date = Date.today.to_datetime.end_of_day

    user_sessions = Session.from_user(current_user).from_course(@course).where(start: @start_date..@end_date).distinct

    other_students_data = ActivityDetailView.where("course_id_lms = ?", @course.id_lms).group(:student_course_state).average(:total_time_spent)

    @user_time_in_course = format_duration(SessionResource.joins(:resource).where("session_resources.session_id IN (?) AND resources.course_id = ? AND resources.category != ? ", user_sessions.map(&:id), @course, Resource::CATEGORIES[:others]).sum(:active_time))
    @completers_time_in_course = format_duration(other_students_data['completed'].to_i + other_students_data['completed_without_certificate'].to_i)
    @no_completers_time_in_course = format_duration(other_students_data['not_completed'].to_i)

    @user_study_frequency = format_duration(current_user.study_frequency(@course))
    # @completers_study_frequency = format_duration(0)
    # @no_completers_study_frequency = format_duration(0)
  end

  # CHECK
  def effectiveness_efficiency
    user_enrollment = Enrollment.from_user(current_user).from_course(@course).last
    @weeks_on_course = user_enrollment.current_week

    course_resources = Resource.from_course(@course)
    @time_required = course_resources.group(:resource_type).sum(:duration)

    @user_time_in_course =  course_resources.joins(:sessions)
                                            .where("sessions.user_id = ? AND resources.category != ? ", current_user.id, Resource::CATEGORIES[:others])
                                            .group("resources.resource_type")
                                            .sum("session_resources.active_time")

    @completers_time_in_course = ActivityDetailView.where(course_id_lms: @course.id_lms, student_course_state: 'completed')
                                                   .sum(:total_time_spent).to_int

    @completers_time = ActivityDetailView.where(course_id_lms: @course.id_lms, student_course_state: 'completed', week: @weeks_on_course)
                                          .group(:course_item_type_desc).average(:total_time_spent)

    @not_completers_time = ActivityDetailView.where(course_id_lms: @course.id_lms, student_course_state: 'not_completed', week: @weeks_on_course)
                                              .group(:course_item_type_desc).average(:total_time_spent)

    @resources = @user_time_in_course.map{ |key, value|
      {
        name: key.capitalize.pluralize,
        time_required: format_duration(@time_required[key]*60),
        time_invested: format_duration(value),
        completers_time: @completers_time[key].present? ? format_duration(@completers_time[key].to_int) : format_duration(0),
        not_completers_time: @not_completers_time[key].present? ? format_duration(@not_completers_time[key].to_int) : format_duration(0)
      }
    }
  end


  private

  def set_course
    @course = Course.find(params[:course_id])
  end

end
