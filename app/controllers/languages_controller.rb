class LanguagesController < ApplicationController

  def change_language
    if params[:language]
      if I18n.available_locales.include? params[:language].to_sym and current_user
        current_user.locale = params[:language]
        current_user.save
        I18n.locale = current_user.locale
      end
    end

    respond_to do |format|
      format.js { render js: "location.reload();" }
    end

  end

end
