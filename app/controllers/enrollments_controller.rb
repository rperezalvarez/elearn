class EnrollmentsController < ApplicationController
    before_action :authenticate_user!

    def destroy_course_enrollments
        course = Course.find(params[:course_id])
        enrollments = Enrollment.where(edition: course.editions, user: current_user)

        if enrollments.present?
            enrollments.destroy_all
        end

        redirect_to courses_path
    end
end
