module DashboardHelper

  def dashboard_selector
    case params[:action]
    when 'effectiveness_efficiency'
      content_tag(:div, class: 'btn-group pull-right') do
        concat(link_to I18n.t("buttons.engagement_performance"), course_engagement_performance_path(@course), {class: 'btn btn-primary analytics-click-EPDashboardSel'})
        concat(link_to I18n.t("buttons.effectiveness_efficiency"), '#', class: 'btn btn-primary active')
      end
    when 'engagement_performance'
      content_tag(:div, class: 'btn-group pull-right') do
        concat(link_to I18n.t("buttons.engagement_performance") , '#', class: 'btn btn-primary active')
        concat(link_to I18n.t("buttons.effectiveness_efficiency"), course_effectiveness_efficiency_path(@course), {class: 'btn btn-primary analytics-click-EEDashboardSel'})
      end
    end
  end

end
