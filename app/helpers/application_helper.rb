module ApplicationHelper

  def view_title
    if params[:controller] == "courses"
      if params[:action] == "index"
        I18n.t('sidebar.My courses')
      else
        truncate(Course.find(params[:course_id]).full_name, length: 75, separator: ' ')
      end
    elsif params[:controller] == "notes"
      I18n.t('sidebar.My notes')
    elsif params[:controller] == "goals"
      I18n.t('sidebar.My goals')
    end
  end

  def language_select
    output = ""
    I18n.available_locales.each do |lang|
      output << content_tag(:li, link_to(I18n.t("languages.#{lang.to_s}"), languages_change_language_path(language: lang.to_s), remote: true))
    end
    output.html_safe
  end

  def info_message(text)
    content_tag(:i, "", {class: 'fa fa-info-circle', data: { toggle: 'tooltip', placement: 'right', html: 'true', container: 'body'}, title: text})
  end

end
