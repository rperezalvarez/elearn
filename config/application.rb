require_relative 'boot'

require 'rails/all'
require 'sprockets/es6'
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ChromeExtensionDashboardApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '*.yml').to_s]

    config.i18n.default_locale = :es

    config.i18n.available_locales = [:en, :es]

    # config.time_zone = 'Santiago'

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.autoload_paths += %W(#{config.root}/lib) # add this line
  end
end
