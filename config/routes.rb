Rails.application.routes.draw do

  root 'courses#index'
  get 'languages/change_language'

  
  
  get 'sing_in_by_token' => 'courses#sing_in_by_token'
  
  resources :languages do
    get 'change_language/:language' => 'languages#change_language'
  end
  
  resources :courses do
    post 'delete_enrollments' => 'enrollments#destroy_course_enrollments'
    get 'engagement_performance'
    get 'effectiveness_efficiency'
    get 'get_enrolled_weeks'
    get 'day_of_course'

    get 'engagement_performance_data' => 'dashboard#engagement_performance_data'
    get 'effectiveness_efficiency_data' => 'dashboard#effectiveness_efficiency_data'

    get 'progress_and_course_time_use' => 'dashboard#progress_and_course_time_use'
    get 'required_activities' => 'dashboard#required_activities'
  end

  resources :dashboard, only: :index do
    get 'sing_in_by_token', on: :collection
  end

  resources :notes do
  end

  get 'download_notes' => 'notes#download', :as => 'download_notes'

  get 'dashboard/effectiveness_data', :defaults => { :format => 'json' }
  get 'dashboard/activities_complete_data', :defaults => { :format => 'json' }

  resources :goals, only: [:index,:create] do
    get 'change_week',on: :collection
  end


  namespace :api, :defaults => { :format => 'json' } do
    post 'sign_up' => 'user_authentication#register_user'
    post 'auth_user' => 'user_authentication#authenticate_user'
    get 'validate_token' => 'user_authentication#validate_token'
    get 'urls' => 'url_receiver#index'
    post 'urls' => 'url_receiver#save_visited_url'

    #TODO: Create namespace for notes
    post 'create_note' => 'note#save_note'
    post 'get_note' => 'note#get_note'
    get 'notes' => 'note#notes'
    get 'courses' => 'courses#index'

    post 'get_effective_time' => 'charts#get_effective_time'

    namespace :sessions do
      post 'start', method: :start
      post 'get_current', method: :get_current
      post 'close', method: :close
    end

    namespace :user_profile do
      get 'details', method: :user_details
      post 'edit', method: :edit
      post 'destroy', method: :destroy
    end

  end

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
end
