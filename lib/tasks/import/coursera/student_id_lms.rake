require_relative 'update_membership_importer'

namespace :import do
  namespace :coursera do

    desc 'Import lms id to membership'
    task student_id_lms: :environment do

      ARGV.each { |a| task a.to_sym do ; end }

      file_name = ARGV[1].to_s


      base_path = Rails.root.join('db','csv_data')
      file_path = base_path.join('coursera','user_id_lms',file_name)
      importer = UpdateMembershipImporter.new(file_path)

      puts "Importing #{file_path} - START"
      importer.import_data
      puts "Importing #{file_path} - END"
    end
  end
end
