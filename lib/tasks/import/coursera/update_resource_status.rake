
namespace :import do
  namespace :coursera do

    desc 'Import csv all courses to update resources status'
    task update_resource_status: :environment do

      ARGV.each { |a| task a.to_sym do ; end }

      base_path = Rails.root.join('db','csv_data')

      folder_names = ['aula_constructivista',
                      'gestion_organizaciones',
                      'electrones',
                      'analisis_transporte',
                      'gestion_proyectos',
                      'silicon_valey',
                      'equilibrio',
                      'energia_sustentable',
                      'gestion_empresarial',
                      'python',
                      'demanda_transporte',
                      'web_semantica',
                      'liderazgo_educativo']

      importer = UpdateMembershipImporter.new()

      file_name = ARGV[1].to_s

      folder_names.each do |folder|
        file_path = base_path.join('coursera','courses',folder,file_name)
        importer.set_new_data(file_path)

        importer.import_data
      end

    end
  end
end