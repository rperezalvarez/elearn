require_relative 'data_importer'

class UpdateMembershipImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
        Membership.transaction do
          params = {}
          begin
            lms_coursera = LearningManagementSystem.find_by(name: "Coursera")
            params = update_user_params(row_data)

            # Find by email
            user = User.find_by email: params[:email]
            
            # Find by current IP
            if user.blank?
                user = User.find_by current_sign_in_ip: params[:ip]
            end

            #  Update membership with lms_user_id
            if !user.blank?
                membership = Membership.find_by(user_id: user.id,learning_management_system_id:lms_coursera.id)

                membership.lms_user_id = params[:lms]
                puts "User: #{user.email}"
                puts "Membership :#{membership.lms_user_id}"
                #TODO: Run in the server see how it works
                membership.save!
            else
              puts "User not founded"
            end
            
          rescue Exception => e
            puts e.message
            puts "#{params}"
          end
  
        end
      end

  end

  def update_user_params(row)
    {
        lms: row[0],
        email: row[2],
        ip: row[3]
    }
  end

end