require_relative 'data_importer'

class UpdateStatusImporter < DataImporter

  def import_data(args = {})
    total_records_imported = 0

    @data.each_with_index do |row_data,index|
      ResourceStatus.transaction do
        params = {}
        
        begin
          puts "ROW : #{index}"
          params = resources_status_params(row_data)
          lms_coursera = LearningManagementSystem.find_by(name: "Coursera")

          course = Course.find_by(learning_management_system_id: lms_coursera.id,id_lms: params[:course_id_lms])

          resource = Resource.find_by(course_id: course.id, id_lms: params[:resource_id_lms])

          if resource.present?
            membership = Membership.find_by(learning_management_system_id: lms_coursera.id,lms_user_id:params[:user_id_lms])

            if membership.present?
              editions_ids = course.editions.pluck(:id)

              if editions_ids.present?
                #Find the last student enrollment
                
                last_enrollment = Enrollment.where(user_id:membership.user_id,edition_id:editions_ids).order(:date_enrolled).last
                puts "User ID : #{membership.user_id}"
                puts "Enrollment ID : #{last_enrollment.id}"

                if last_enrollment.present?
                  status = ResourceStatus.where(resource_id:resource.id,enrollment_id:last_enrollment.id).first

                  if params[:course_progress].to_i == 2
                    status.date_completed = params[:execution_date]
                    status.save!
                  end

                  total_records_imported = total_records_imported + 1  
                else
                  puts "Enrollment is not present"
                end  
              else
                puts "Edition not present"
              end  
            else
              puts "Membership not founded"
            end

          else
            puts "The resource required couldn't be found ID LMS : #{params[:resource_id_lms]}"
          end  
          


        rescue Exception => e
          puts e.message
          puts "#{params}"
        end

        
      end
    end

    puts "TOTAL RECORDS : #{total_records_imported}"   
  end

  def resources_status_params(row)
    {
      course_id_lms: row[0],
      resource_id_lms: row[1],
      user_id_lms: row[2],
      course_progress: row[3],
      execution_date: DateTime.parse(row[4])
    }
  end
end