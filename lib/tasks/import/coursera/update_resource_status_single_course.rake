require_relative 'update_status_importer'

namespace :import do
  namespace :coursera do

    desc 'Import csv all courses to update resources status'
    task update_resource_status_single_course: :environment do

      ARGV.each { |a| task a.to_sym do ; end }

      base_path = Rails.root.join('db','csv_data')

      importer = UpdateStatusImporter.new()

      folder_name = ARGV[1].to_s
      file_name = ARGV[2].to_s

      file_path = base_path.join('coursera','courses',folder_name,file_name)
      importer.set_new_data(file_path)

      importer.import_data

    end
  end
end