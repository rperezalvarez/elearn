# This task close all study sessions from all users

active_sessions = Session.where(duration: nil)
active_sessions_count = active_sessions.count

puts "##### Closing #{active_sessions_count} active sessions #####"

active_sessions.each_with_index do |active_session, count|
  puts "Session #{count+1} of #{active_sessions_count}"
  active_session.close
end
