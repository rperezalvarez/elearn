module UrlProcessors
  class Coursera
    @up = {
      # Base
      base_url: "http(s)?:\/\/www.coursera.org*",
      learn: "http(s)?:\/\/www.coursera.org\/learn\/[a-zA-Z0-9-][a-zA-Z0-9_-]*",

      home: "\/home",
      # Home sub-paths
      welcome: "\/welcome",
      assignments: "\/assignments",
      info: "\/info",
      week: "\/week",
      week_number: "\/[0-9][0-9]*",

      lecture: "\/lecture",
      programming: "\/programming",
      exam: "\/exam",
      supplement: "\/supplement",
      # Lecture, programming, exam, supplement sub-path
      resource_code: "\/[a-zA-Z0-9-][a-zA-Z0-9_-]*",
      resource_name: "\/[a-zA-Z0-9-][a-zA-Z0-9_-]*",

      discussions: "\/discussions",
      # Discussions sub-paths
      forums: "\/forums",
      weeks: "\/weeks",
      threads: "\/threads",
      thread_code: "\/[a-zA-Z0-9-][a-zA-Z0-9_-]*",
      forum_code: "\/[a-zA-Z0-9-][a-zA-Z0-9_-]*",

      peer: "\/peer",
      # Peer sub-paths
      give_feedback: "\/give_feedback",
      review: "\/review"
    }

    def self.base_url
      @up[:base_url]
    end

    def self.build_from_url(session, start, finish, active_time, url)
      sr = SessionResource.new()
      sr.start = start
      sr.finish = finish
      sr.active_time = active_time
      sr.session = session
      sr.url = url
      case url

      when /#{@up[:learn]}#{@up[:home]}#{@up[:welcome]}/,
        /#{@up[:learn]}#{@up[:home]}#{@up[:assignments]}/,
        /#{@up[:learn]}#{@up[:home]}#{@up[:info]}/,
        /#{@up[:learn]}#{@up[:home]}#{@up[:week]}#{@up[:week_number]}/

        course = Course.find_by_short_name(SessionResource.get_url_element(url, 2))
        if course
          resource = Resource.where('category = ? AND resource_type = ? AND course_id = ?', Resource::CATEGORIES[:complements], Resource::RESOURCE_TYPES[:complements][:info], course).first
          resource = Resource.create(category: Resource::CATEGORIES[:complements], resource_type: Resource::RESOURCE_TYPES[:complements][:info], course: course) unless resource.present?
          sr.resource = resource
        end

      when /#{@up[:learn]}#{@up[:lecture]}#{@up[:resource_code]}#{@up[:resource_name]}/,
        /#{@up[:learn]}#{@up[:programming]}#{@up[:resource_code]}#{@up[:resource_name]}/,
        /#{@up[:learn]}#{@up[:exam]}#{@up[:resource_code]}#{@up[:resource_name]}/,
        /#{@up[:learn]}#{@up[:supplement]}#{@up[:resource_code]}#{@up[:resource_name]}/

        sr.resource = Resource.find_by_id_lms(SessionResource.get_url_element(url, 4))

      when /#{@up[:learn]}#{@up[:discussions]}#{@up[:forums]}#{@up[:weeks]}#{@up[:week_number]}#{@up[:threads]}#{@up[:thread_code]}/,
        /#{@up[:learn]}#{@up[:discussions]}#{@up[:forums]}#{@up[:forum_code]}/,
        /#{@up[:learn]}#{@up[:peer]}#{@up[:resource_code]}#{@up[:resource_name]}#{@up[:give_feedback]}/,
        /#{@up[:learn]}#{@up[:peer]}#{@up[:resource_code]}#{@up[:resource_name]}#{@up[:review]}/

        course = Course.find_by_short_name(SessionResource.get_url_element(url, 2))
        if course
          resource = Resource.where('category = ? AND resource_type = ? AND course_id = ?', Resource::CATEGORIES[:discussions], Resource::RESOURCE_TYPES[:discussions][:discussions], course).first
          resource = Resource.create(category: Resource::CATEGORIES[:discussions], resource_type: Resource::RESOURCE_TYPES[:discussions][:discussions], course: course) unless resource.present?
          sr.resource = resource

        end
      end

      # If doesn't match any resource, we just through it to the trash without save it
      if !sr.resource
        return true
      end


      # Enroll user in course if his first time
      if sr.resource.course.present? and sr.resource.category != Resource::CATEGORIES[:others]
        user_course_enrollment = Enrollment.from_user(session.user).from_course(sr.resource.course).last

        if user_course_enrollment.nil?
          available_edition = sr.resource.course.editions.real.enrollable.present? ? sr.resource.course.editions.real.enrollable.last : sr.resource.course.editions.not_real.enrollable.last
          if available_edition
            user_course_enrollment = Enrollment.create(user: session.user, edition: available_edition, date_enrolled: Date.today)
          else
            return true
          end

        elsif user_course_enrollment.edition.finish.present?
          if user_course_enrollment.edition.finish < DateTime.now
            available_edition = sr.resource.course.editions.real.enrollable.present? ? sr.resource.course.editions.real.enrollable.last : sr.resource.course.editions.not_real.enrollable.last

            if available_edition
              user_course_enrollment = Enrollment.create(user: session.user, edition: available_edition, date_enrolled: Date.today)
            else
              return true
            end
          end
        end

        # Mark resource status as started if is his first time
        resource_status = user_course_enrollment.resource_statuses.from_resource(sr.resource).last
        resource_status = ResourceStatus.create(enrollment: user_course_enrollment, resource: sr.resource) unless resource_status.present?

        if resource_status.date_started.nil?
          resource_status.date_started = sr.start
          resource_status.save
        end
      end
      sr.save
    end

  end
end
