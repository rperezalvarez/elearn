data_type_opts = ['courses', 'editions', 'resources']
puts "Please enter the type of data you want to import. Available options are: #{data_type_opts.map(&:inspect).join(', ')}"

data_type = STDIN.gets.chomp

if !data_type_opts.include? data_type
  raise "'#{data_type}' is not a valid type"
end

require_relative "importers/#{data_type.singularize}_importer"

base_path = "db/csv_data/"
case data_type
when "courses"
  puts base_path + "courses.csv"
  imp = CourseImporter.new(base_path + "courses.csv")
  imp.import_data

when "editions"
  Dir[base_path + "editions/*"].each do |file|
    puts file
    imp = EditionImporter.new(file)
    imp.import_data
  end

when "resources"
  Dir[base_path + "resources/*"].each do |file|
    puts file
    imp = ResourceImporter.new(file)
    imp.import_data
  end
end
