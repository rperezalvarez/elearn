Course.all.each do |c|
  if c.editions.where(real: false).empty?
    Edition.create!(course: c, start: DateTime.now - 100.years, finish: DateTime.now + 1000.years, dead_line_enrollment_date: DateTime.now + 999.years, real: false)
  end
end
