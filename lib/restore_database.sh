#!/usr/bin/env bash

#In order to work this need to be in the rails app folder

clear
echo "Script to restore database table by table"

echo "Full path of dump location:"
read dump_location

echo "Database password:"
read db_pass

tables=( "learning_management_systems" "courses" "editions" "enrollments" "memberships" "goal_details" "goals"  "notes"
          "resources" "resource_statuses" "sessions" "users" "student_data"
          "session_data" "resource_activity_data" )

echo "--------------Dropping database--------------"
rails db:drop
echo "--------------Creating database--------------"
rails db:create
echo "--------------Migrating schema--------------"
rails db:migrate

echo "--------------Restoring tables--------------"
for table in "${tables[@]}"
do
  PGPASSWORD=$db_pass pg_restore -a -d elearn -v -U postgres -h localhost -t $table $dump_location;
done

# Run different database modifications

rails runner lib/update_pk.rb
rails runner lib/fix_categories.rb
rails runner lib/create_not_real_editions.rb
rails runner lib/create_app_configurations.rb
