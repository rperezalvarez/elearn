require_relative 'data_importer'

class EditionImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
      Edition.transaction do
        params = {}
        begin
          course = Course.find_by_id_lms(row_data[0])
          params = edition_params(row_data,course)
          Edition.find_or_create_by!(params)

        rescue Exception => e
          puts e.message
          puts "#{params}"
        end

      end
    end
  end

  def edition_params(row,course)
    {
      id_edition_lms: row[1],
      course_id: course.id,
      start: DateTime.strptime(row[2], "%Y-%m-%d %H:%M:%S"),
      finish: DateTime.strptime(row[3], "%Y-%m-%d %H:%M:%S"),
      dead_line_enrollment_date: DateTime.strptime(row[4], "%Y-%m-%d %H:%M:%S")
    }
  end

end