require_relative 'course_importer'
require_relative 'edition_importer'
require_relative 'resource_importer'

class ImporterManager

  def initialize(files_paths,directory_params)
    edition_importer = EditionImporter.new
    resource_importer = ResourceImporter.new
    @importers = {edition:edition_importer,resource:resource_importer}

    @files_importers = {}
    @directory_importers = {}

    @files_importers[:course] = CourseImporter.new(files_paths[:course]) if files_paths[:course].present?

    directory_params.each do |importer_type,directory_path|
      @directory_importers[importer_type.to_sym] = directory_path
    end
  end

  def import_all_files
    @files_importers[:course].import_data
  end

  def import_data_of(importer_key,args = {})
    puts "Import data of #{importer_key} initiated"
    @importers[importer_key.to_sym].import_data(args)
    puts "Import data of #{importer_key} finished"
  end

  def import_data_with(importer_key,args = {})
    if args.has_key?(:path)
      importer = @importers[importer_key.to_sym]

      new_data_path = args[:path]
      importer.set_new_data(new_data_path)

      puts "Import data of #{importer_key} initiated"
      importer.import_data(args)
      puts "Import data of #{importer_key} finished"
    end
  end

  def import_all_directory
    @directory_importers.each do |importer_type,path|
      files = Dir.glob(path)
      files.each do |file_name|

        importer = @importers[importer_type.to_sym]
        importer.set_new_data(file_name)
        importer.import_data
      end
    end
  end

  def import_directory(importer_type)
    importer = @importers[importer_type.to_sym]
    path = @directory_importers[importer_type.to_sym]

    files = Dir.glob(path)
    files.each do |file_name|

      puts file_name
      puts importer.class.name
      importer.set_new_data(file_name)
      importer.import_data
    end
  end
end


base_path = Rails.root.join('db','csv_data')
files_paths = {course: base_path.join('courses.csv')}

directory_params = {edition: base_path.join('editions','*'),resource: base_path.join('resources','*')}
manager = ImporterManager.new(files_paths,directory_params)

manager.import_all_files
manager.import_directory('edition')
manager.import_directory('resource')
