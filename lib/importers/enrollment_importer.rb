require_relative 'data_importer'


class EnrollmentImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
      Enrollment.transaction do
        params = {}
        begin
          edition = Edition.find_by_id_edition_lms(row_data[0])
          user = User.find_by_email(row_data[1])

          params = enrollment_params(row_data,edition,user)
          enrollment = Enrollment.find_or_create_by!(params)

        rescue Exception => e
          puts e.message
          puts "#{params}"
        end

      end
    end
  end


  def enrollment_params(row,edition,user)
    {
        user_id: edition.id,
        edition_id: user.id,
        date_enrolled: Date.strptime(row[2], "%d/%m/%Y"),
        date_completed:Date.strptime(row[3], "%d/%m/%Y"),
        finished: transform_finished(row[4])
    }
  end

  def transform_finished(raw_finished)
    true_options = [1,'true',true,'verdedadero']

    true_options.include?(raw_finished)
  end
end