require_relative 'data_importer'

class ResourceStatusImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
      ResourceStatus.transaction do
        params = {}
        begin

          user = User.joins(:memberships).where(membership:{lms_user_id:row_data[0]}).group(:user_id).first
          resource = Resource.find_by_id_lms(row_data[1])

          params = resources_status_params(row_data,user,resource)
          ResourceStatus.find_or_create_by!(params)

        rescue Exception => e
          puts e.message
          puts "#{params}"
        end

      end
    end
  end


  def resources_status_params(row,user,resource)
    {
      user_id: user.id,
      resource_id: resource.id,
      date_started: DateTime.strptime(row[2], "%d/%m/%Y"),
      date_completed: DateTime.strptime(row[3], "%d/%m/%Y"),
      status: row[4]
    }
  end
end