require_relative 'data_importer'

class UserImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
      User.transaction do
        params = {}
        begin
          params = user_params(row_data)
          user = User.find_or_create_by!(params)

        rescue Exception => e
          puts e.message
          puts "#{params}"
        end

      end
    end
  end


  def user_params(row)
    {
        email:row[0],
        name: row[1]
    }
  end
end