require_relative 'data_importer'

class SessionImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
      Session.transaction do
        params = {}
        begin
          membership = Membership.where(lms_user_id:row[0]).first
          user = membership.user

          params = session_params(row_data,user)
          session = Session.find_or_create_by!(params)

          #Params to create session-resource
          resource = Resource.find_by_id_lms(row[4])

          SessionResource.find_or_create_by!(session_resource_params(row,session,resource))

        rescue Exception => e
          puts e.message
          puts "#{params}"
        end

      end
    end
  end


  def session_params(row,user)
    {
      user_id: user.id,
      start: Date.strptime(row[2], "%d/%m/%Y"),
      duration: row[3].to_i
    }
  end

  def session_resource_params(row,session,resource)
    {
      session_id:session.id,
      resource_id:resource.id,
      active_time:row[5],
      url:row[6],
      start: Date.strptime(row[7], "%d/%m/%Y"),
      finish: Date.strptime(row[8], "%d/%m/%Y")
    }
  end
end