require_relative 'data_importer'

class MembershipImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
      Membership.transaction do
        params = {}

        begin
          lms = LearningManagementSystem.find_by_learning_management_system_id(row_data[0])

          params = membership_params(params,lms)
          membership = Membership.find_or_create_by!(params)

          #Before to run , remove confirmation from devise .skip_confirmation!
          email = "user#{index}@notemyprogress.com"
          user = User.find_by_email(email)

          if user.blank?
            user = User.new(name:"user#{index}",email:email)
            user.skip_confirmation!
            user.save!
          end

          membership.user_id = user.id

          membership.save!

        rescue Exception => e
          puts e.message
          puts "#{params}"
        end
      end
    end
  end


  def membership_params(row,lms)
    {
      lms_user_id: row[1],
      learning_management_system_id: lms.learning_management_system_id
    }
  end
end