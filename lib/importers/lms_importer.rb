require_relative 'data_importer'

class LmsImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
      LearningManagementSystem.transaction do
        params = {}
        begin
          params = lms_params(row_data)
          lms = LearningManagementSystem.find_or_create_by!(params)

        rescue Exception => e
          puts e.message
          puts "#{params}"
        end

      end
    end
  end


  def lms_params(row)
    {
        name: row[0],
        root: row[1]
    }
  end
end