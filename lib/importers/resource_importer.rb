require_relative 'data_importer'

class ResourceImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
      Resource.transaction do
        params = {}
        begin
          course = Course.find_by_id_lms(row_data[0])

          params = resource_params(row_data,course)
          resource = Resource.find_or_create_by!(params)

        rescue Exception => e
          puts e.message
          puts "#{params}"
        end

      end
    end
  end

  def resource_params(row,course)
    {
        id_lms: row[1],
        course_id: course.id,
        name: row[2],
        sequence: row[3].to_i,
        section: row[4].to_i,
        module: row[5].to_i,
        resource_type: row[6],
        category: define_category(row[6]),
        duration: row[8].to_i
    }
  end


  def define_category(raw_category)
    category = nil
    case raw_category
         when 'info', 'supplement'
           category = 'complements'
         when 'lecture'
           category = 'lectures'
         when 'exam', 'phased peer'
           category = 'evaluations'
         when 'discussions'
           category = 'discussions'
         else
           category = 'others'
    end

    category
  end
end
