require 'csv'

class StudentBulkImporter

  def initialize(path_file='')
    if path_file != ''
      @path_file = path_file
      @data = CSV.read(path_file, { :headers => true })
    end
  end

  def import_data(args = {})
  end

  def set_files_path(pattern)
    @file_paths = Dir[pattern]
  end

  def process_multiple_files
  end

  def set_new_data(path_file='')
    if path_file != ''
      @path_file = path_file
      @data = CSV.read(path_file, { :headers => true })
    end
  end

  def bulk_import(validation)
    new_records = @data.map do |row|

      StudentDatum.new({id_user_lms:row[0],
                        country:row[1],
                        id_course_lms:row[2],
                        date_completed:DateTime.strptime(row[3], "%Y-%m-%d %H:%M:%S"),
                        date_enrolled:DateTime.strptime(row[5], "%Y-%m-%d %H:%M:%S"),
                        state:define_status(row[4].to_i)})
    end

    StudentDatum.import new_records,validate: validation
  end

  def define_status(int_opt)
    status = nil
    case int_opt
      when 0
        status = 'not_completed'
      when 1
        status = 'completed_without_certificate'
      when 2
        status = 'completed'
    end

    status
  end

end
