require_relative 'student_bulk_importer'
require_relative 'resource_activity_importer'
require_relative 'session_importer'

class SessionBulkManager
  def initialize(files_paths,directory_params)
    student_importer = StudentBulkImporter.new
    resource_importer = ResourceActivityImporter.new
    session_importer = SessionImporter.new

    @importers = {students:student_importer,resources: resource_importer,sessions:session_importer}

    @files_importers = {}
    @directory_importers = {}

    @files_importers[:student] = StudentBulkImporter.new(files_paths[:student]) if files_paths[:student].present?
    @files_importers[:resource] = ResourceActivityImporter.new(files_paths[:resource]) if files_paths[:resource].present?
    @files_importers[:session] = SessionImporter.new(files_paths[:session]) if files_paths[:session].present?

    directory_params.each do |importer_type,directory_path|
      @directory_importers[importer_type.to_sym] = directory_path
    end
  end

  def import_all_files
    puts Benchmark.measure {
      @files_importers[:student].bulk_import(false) if @files_importers[:student].present?
      @files_importers[:resource].bulk_import(false) if @files_importers[:resource].present?
      @files_importers[:session].bulk_import(false) if @files_importers[:session].present?
    }
  end

  def import_all_directory
    @directory_importers.each do |importer_type,path|
      files = Dir.glob(path)
      files.each do |file_name|

        importer = @importers[importer_type.to_sym]
        importer.set_new_data(file_name)
        importer.import_data
      end
    end
  end

  def import_directory(importer_type)

    importer = @importers[importer_type.to_sym]
    path = @directory_importers[importer_type.to_sym]

    files = Dir.glob(path)
    files.each do |file_name|
      puts file_name
      importer.set_new_data(file_name)
      importer.bulk_import(false)
    end
  end

end

base_path = Rails.root.join('db','csv_data')
directory_params = {sessions: base_path.join('sessions','*')}
bulk_manager = SessionBulkManager.new({},directory_params)

puts Benchmark.measure {
  puts "Import SESSIONS - STARTED"
  bulk_manager.import_directory('sessions')
  puts "Import SESSIONS - FINISHED"
}
