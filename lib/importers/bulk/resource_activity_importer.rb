require 'csv'
require 'logger'

class ResourceActivityImporter

  def initialize(path_file='')
    if path_file != ''
      @path_file = path_file
      @data = CSV.read(path_file, { :headers => true })

    end
  end

  def import_data(args = {})
  end

  def set_files_path(pattern)
    @file_paths = Dir[pattern]
  end

  def process_multiple_files
  end

  def set_new_data(path_file='')
    if path_file != ''
      @path_file = path_file
      @data = CSV.read(path_file, { :headers => true })
    end
  end

  def bulk_import(validation)
    puts " RESOURCE BULK INSERTED - STARTED #{DateTime.now.strftime("%Y-%m-%d %H:%M:%S")}"
    puts @path_file.to_s
    logger = create_logger
    amount_file = 0
    new_records = @data.map do |row|
      begin 
        params = {item_id_lms:row['item_id_lms'],
                                   user_id_lms:row['user_id_lms'],
                                   state:define_state(row['state']),
                                   course_progress_ts: parse_date(row['course_progress_ts'],amount_file,logger),
                                   course_item_type_desc:row['course_item_type_desc'],
                                   session_id_lms:row['id_session'],
                                   course_id_lms: row['course_id'],
                                   tine_spent: row['time_spent'].to_d
                                   }

        puts "Processing #{amount_file}" if amount_file%10000 == 0
        amount_file += 1

        ResourceActivityDatum.new(params)
      rescue Exception => e
        logger.info(e.message)
      end 
    end
    begin
      puts new_records.count
      ResourceActivityDatum.import new_records,validate: validation
    rescue Exception => e
      logger.info(e.message)
    end
    puts " RESOURCE BULK INSERTED - FINISHED #{DateTime.now.strftime("%Y-%m-%d %H:%M:%S")}"
  end


  def define_state(int_opt)
    status = nil

    option = int_opt.to_i

    case option
      when 1
        status = 'started'
      when 2
        status = 'completed'
    end

    status
  end

  def parse_date(raw_date,index,logger)
    date = nil
    begin
      date = DateTime.strptime(raw_date, "%Y-%m-%d %H:%M:%S")
    rescue Exception => e
      message = "{INDEX: #{index.to_s}, MESSAGE: #{e.message}, DATA: #{raw_date.to_s}}"
      logger.info(message)
    end

    date
  end

  def create_logger
    file_name = File.basename @path_file
    logger_path = Rails.root.join('lib','importers','bulk','log',"#{DateTime.now.strftime("%Y%m%d%H%M%S")}_resource_act_#{file_name}.log")
    logger = Logger.new logger_path
    logger.level = Logger::INFO

    # logs for program called MainProgram
    logger.progname = 'ResourceActivity'

    return logger
  end

end
