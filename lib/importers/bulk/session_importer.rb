require 'csv'
require 'logger'

class SessionImporter

  def initialize(path_file='')
    if path_file != ''
      @path_file = path_file
      @data = CSV.read(path_file, { :headers => true })

    end
  end

  def import_data(args = {})
  end

  def set_files_path(pattern)
    @file_paths = Dir[pattern]
  end

  def process_multiple_files
  end

  def set_new_data(path_file='')
    if path_file != ''
      @path_file = path_file
      @data = CSV.read(path_file, { :headers => true })
    end
  end

  def bulk_import(validation)
    puts " SESSION BULK INSERTED - STARTED #{DateTime.now.strftime("%Y-%m-%d %H:%M:%S")}"
    puts @path_file.to_s
    logger = create_logger
    amount_file = 0

    new_records = @data.map do |row|
      begin 
        params = { id_session_lms:row['id'],
                   number_session:row['number_session'],
                   user_id_lms: row['user_id_lms'],
                   id_course_lms: row['course_id'],
                   date_ts: parse_date(row['date_ts'],amount_file,logger),
                   duration: row['duration'].to_d
                 }

        puts "Processing #{amount_file}" if amount_file%10000 == 0
        amount_file += 1

        SessionDatum.new(params)
      rescue Exception => e
        logger.info(e.message)
      end 
    end
    begin
      puts new_records.count
      SessionDatum.import new_records,validate: validation
    rescue Exception => e
      logger.info(e.message)
    end
    puts " SESSION BULK INSERTED - FINISHED #{DateTime.now.strftime("%Y-%m-%d %H:%M:%S")}"
  end

  def parse_date(raw_date,index,logger)
    date = nil
    begin
      date = DateTime.strptime(raw_date, "%Y-%m-%d %H:%M:%S")
    rescue Exception => e
      message = "{INDEX: #{index.to_s}, MESSAGE: #{e.message}, DATA: #{raw_date.to_s}}"
      logger.info(message)
    end

    date
  end

  def create_logger
    file_name = File.basename @path_file
    logger_path = Rails.root.join('lib','importers','bulk','log',"#{DateTime.now.strftime("%Y%m%d%H%M%S")}_session_#{file_name}.log")
    logger = Logger.new logger_path
    logger.level = Logger::INFO

    # logs for program called MainProgram
    logger.progname = 'Session'

    return logger
  end

end
