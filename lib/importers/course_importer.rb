require_relative 'data_importer'

class CourseImporter < DataImporter

  def import_data(args = {})
    @data.each_with_index do |row_data,index|
      Course.transaction do
        params = {}
        begin
          lms = LearningManagementSystem.find_by_name("Coursera")
          params = course_params(row_data,lms)
          Course.find_or_create_by!(params)

        rescue Exception => e
          puts e.message
          puts "#{params}"
        end

      end
    end
  end

  def course_params(row,lms)
    {
        id_lms: row[0],
        learning_management_system_id: lms.id,
        full_name: row[1],
        short_name: row[2],
        duration: row[3].to_f
    }
  end
end