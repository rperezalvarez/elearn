module PerformanceCalculations
  module ResourcesActivities

    def video_watched_previous_week(enrollment,goal)
      start_date = goal.start - 1.week
      end_date = goal.finish

      enrollment.resource_statuses.joins(:resource).where(resources:{resource_type:'lecture'},date_started:start_date..end_date,status:'Completed').count


    end

    def exams_done_previous_week(enrollment,goal)
      start_date = goal.start - 1.week
      end_date = goal.finish

      enrollment.resource_statuses.joins(:resource).where(resources:{resource_type:'exam'},date_started:start_date..end_date,status:'Completed').count


    end

    def avg_videos_watch(enrollment)
      ed = enrollment.edition

      start_date = enrollment.date_enrolled.to_datetime
      end_date = ed.finish + Course::OFF_SET.week
      weeks = ((end_date-start_date).to_f.ceil/7.0).round(2)

      total_videos = enrollment.resource_statuses.joins(:resource).where(resources:{resource_type:'lecture'},date_started:start_date..end_date,status:'Completed').count

      (total_videos/weeks.to_f).round(2)

    end

    def avg_exams_done(enrollment)
      ed = enrollment.edition

      start_date = enrollment.date_enrolled.to_datetime
      end_date = ed.finish + Course::OFF_SET.week

      weeks = ((end_date-start_date).to_f.ceil/7.0).round(2)

      total_videos = enrollment.resource_statuses.joins(:resource).where(resources:{resource_type:'exam'},date_started:start_date..end_date,status:'Completed').count

      (total_videos/weeks.to_f).round(2)

    end

  end

end
