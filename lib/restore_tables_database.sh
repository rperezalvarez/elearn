#!/usr/bin/env bash

#In order to work this need to be in the rails app folder

clear
echo "Script to restore database table by table"

echo "Full path of dump location:"
read dump_location

echo "Database password:"
read db_pass

tables=( "learning_management_systems" "courses" "editions" "enrollments" "memberships" "goal_details" "goals"  "notes"
          "resources" "resource_statuses" "sessions" "users" "student_data"
          "session_data" "resource_activity_data" )

for table in "${tables[@]}"
do
  PGPASSWORD=$db_pass pg_restore -a -d elearn -v -U elearn -h localhost -t $table $dump_location;
done
