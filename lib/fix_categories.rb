# Little script to fix database duplicate key issues and
# wrong categories

Resource.all.each do |r|
    new_resource_type = Resource::MATCH_RESOURCE_TYPE[r.resource_type]
    r.resource_type = new_resource_type
    r.category = Resource.CATEGORY_OF_RESOURCE_TYPE(new_resource_type)
    r.save!
end
