require 'test_helper'

class Api::ChartsControllerTest < ActionDispatch::IntegrationTest
  test "should get get_effective_time" do
    get api_charts_get_effective_time_url
    assert_response :success
  end

end
