require 'test_helper'

class Api::UserProfileControllerTest < ActionDispatch::IntegrationTest
  test "should get user_details" do
    get api_user_profile_user_details_url
    assert_response :success
  end

end
