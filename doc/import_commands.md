# Command example 

```
pg_restore -U postgres --dbname=elearn --data-only --table=learning_management_systems 20180219_elearn.dump
```

# Import Order

1. learning_management_systems
2. courses
3. app_configurations
4. users
5. memberships
6. editions
7. resources
8. sessions
9. session_resources
10. editions
11. enrollments
12. resource_statuses
13. goals
14. goal_kpi_groups

# Commands

pg_restore -U postgres --dbname=elearn --data-only --table=learning_management_systems 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=courses 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=app_configurations 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=users 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=memberships 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=editions 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=resources 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=sessions 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=session_resources 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=editions 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=enrollments 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=resource_statuses 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=goals 20180219_elearn.dump
pg_restore -U postgres --dbname=elearn --data-only --table=goal_kpi_groups 20180219_elearn.dump

