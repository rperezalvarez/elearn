# Readme

### Application setup
Build the database:
```sh
$ rake db:create db:migrate
```
Populate the database:
```sh
$ rake db:seed
```
You can use the previous seed logging in with the following credentials:
```sh
username: juan@uc.cl
password: 123456
```


### Build domain model
To build the domain model run:

```sh
$ erd --inheritance --direct --attributes=foreign_keys,primary_keys,timestamps,content --orientation=vertical --filename=Diagram
```

## Import initial data

## Requirements

### New project

1. If the app instance is new, you should run the migration from cero without data:

```sh
$ rake db:migrate:reset
```

2. Load the LMS data

```sh
$ rails db:seed model="learning_management_system"
```

### CSV Structure files

TODO: Describe the structure to use importer

### Dump data from Database
Log in with your postgres user.

```sh
$ sudo -i -u postgres
```

Dump data from database

```sh
$ pg_dump -a -Fc database > database.dump
```


Create a clean database, from your rails app folder write:

```sh
$ rails db:drop
$ rails db:create
$ rails db:migrate
```


With your fresh database, import dump into database using the following command.

```sh
$ pg_restore -d database database.dump
```

Done!

*Note*: Flag ```-Fc``` dumps into a custom format archieve to be used with ```pg_restore```. Flag ```-a``` dumps only data.

### Alternatively you can run the script located in /lib/restore_database.sh

To run it:

```sh
$ chmod +x ./restore_database.sh
$ ./restore_database.sh
```

Follow the instructions and you're done.


### Steps

1. rails runner -e development lib/importers/importer_manager.rb
2. rails runner -e development lib/importers/bulk/student_bulk_manager.rb


### Problems
In case of problem asociated with ```Rails console in production: NameError: uninitialized constant``` run:

```sh
$ spring stop
```

and then you should be running `rails console` again with no problems. 

Este software es compartido bajo licencia Creative Commons, que permite permite a otros distribuir, ‘remezclar’, retocar y crear a partir de la obra de modo no comercial, siempre y cuando den los créditos al Proyecto LALA (586120-EPP-1-2017-1-ES-EPPKA2-CBHE-JP) y al Proyecto Fondecyt (11150231), y licencien sus nuevas creaciones bajo nuestras mismas condiciones.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.


