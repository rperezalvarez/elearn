# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180202153402) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ahoy_events", force: :cascade do |t|
    t.integer "visit_id"
    t.integer "user_id"
    t.string "name"
    t.json "properties"
    t.datetime "time"
    t.index ["name", "time"], name: "index_ahoy_events_on_name_and_time"
    t.index ["user_id", "name"], name: "index_ahoy_events_on_user_id_and_name"
    t.index ["visit_id", "name"], name: "index_ahoy_events_on_visit_id_and_name"
  end

  create_table "app_configurations", force: :cascade do |t|
    t.integer "max_inactive_time", default: 2700, null: false
    t.integer "min_session_effective_time_for_save", default: 300, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string "id_lms", limit: 50
    t.bigint "learning_management_system_id"
    t.text "full_name"
    t.text "short_name"
    t.integer "duration"
    t.string "topic", limit: 50
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["learning_management_system_id"], name: "index_courses_on_learning_management_system_id"
  end

  create_table "editions", force: :cascade do |t|
    t.datetime "start"
    t.datetime "finish"
    t.string "id_edition_lms", limit: 50
    t.bigint "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "dead_line_enrollment_date"
    t.boolean "real", default: true
    t.index ["course_id"], name: "index_editions_on_course_id"
  end

  create_table "enrollments", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "edition_id"
    t.date "date_enrolled"
    t.date "date_completed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0
    t.index ["edition_id"], name: "index_enrollments_on_edition_id"
    t.index ["user_id"], name: "index_enrollments_on_user_id"
  end

  create_table "goal_details", force: :cascade do |t|
    t.string "kind"
    t.datetime "schedule_date"
    t.bigint "goal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "execution_date"
    t.boolean "picked", default: false
    t.index ["goal_id"], name: "index_goal_details_on_goal_id"
  end

  create_table "goal_kpi_groups", force: :cascade do |t|
    t.bigint "goal_id"
    t.integer "videos_last_week", default: 0, null: false
    t.integer "evals_last_week", default: 0, null: false
    t.integer "most_effective_day", default: 0, null: false
    t.decimal "avg_weekly_videos", default: "0.0", null: false
    t.decimal "avg_weekly_exams", default: "0.0", null: false
    t.decimal "avg_weekly_time", default: "0.0", null: false
    t.decimal "avg_weekly_time_videos", default: "0.0", null: false
    t.decimal "avg_weekly_time_exams", default: "0.0", null: false
    t.integer "total_videos_this_week", default: 0, null: false
    t.integer "total_evals_this_week", default: 0, null: false
    t.integer "total_time_videos_week", default: 0, null: false
    t.integer "total_time_evals_week", default: 0, null: false
    t.decimal "avg_videos_completers", default: "0.0", null: false
    t.decimal "avg_evals_completers", default: "0.0", null: false
    t.decimal "avg_time_completers", default: "0.0", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["goal_id"], name: "index_goal_kpi_groups_on_goal_id"
  end

  create_table "goals", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "start"
    t.datetime "finish"
    t.integer "goal_hours"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "recorded_hours"
    t.integer "goal_videos"
    t.integer "recorded_videos"
    t.integer "goal_evaluations"
    t.integer "recorded_evaluations"
    t.integer "week"
    t.bigint "enrollment_id"
    t.index ["enrollment_id"], name: "index_goals_on_enrollment_id"
    t.index ["user_id"], name: "index_goals_on_user_id"
  end

  create_table "learning_management_systems", force: :cascade do |t|
    t.string "name"
    t.string "root"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memberships", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "learning_management_system_id"
    t.string "lms_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["learning_management_system_id"], name: "index_memberships_on_learning_management_system_id"
    t.index ["user_id"], name: "index_memberships_on_user_id"
  end

  create_table "notes", force: :cascade do |t|
    t.string "title", limit: 200
    t.text "body"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "code"
    t.bigint "course_id"
    t.index ["course_id"], name: "index_notes_on_course_id"
    t.index ["user_id"], name: "index_notes_on_user_id"
  end

  create_table "resource_activity_data", force: :cascade do |t|
    t.string "item_id_lms"
    t.string "user_id_lms"
    t.string "state"
    t.datetime "course_progress_ts"
    t.string "course_item_type_desc"
    t.string "session_id_lms"
    t.string "course_id_lms"
    t.bigint "course_id"
    t.decimal "tine_spent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_resource_activity_data_on_course_id"
    t.index ["course_id_lms"], name: "index_resource_activity_data_on_course_id_lms"
    t.index ["course_item_type_desc"], name: "index_resource_activity_data_on_course_item_type_desc"
    t.index ["state"], name: "index_resource_activity_data_on_state"
    t.index ["user_id_lms"], name: "index_resource_activity_data_on_user_id_lms"
  end

  create_table "resource_statuses", force: :cascade do |t|
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "date_completed"
    t.string "status", default: "Not started"
    t.datetime "date_started"
    t.bigint "enrollment_id"
    t.index ["enrollment_id"], name: "index_resource_statuses_on_enrollment_id"
    t.index ["resource_id"], name: "index_resource_statuses_on_resource_id"
  end

  create_table "resources", force: :cascade do |t|
    t.string "id_lms", limit: 50
    t.bigint "course_id"
    t.text "name"
    t.integer "sequence"
    t.integer "section"
    t.integer "module"
    t.string "resource_type", limit: 50
    t.string "category", limit: 30
    t.integer "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_resources_on_course_id"
  end

  create_table "session_data", force: :cascade do |t|
    t.string "id_session_lms"
    t.integer "number_session"
    t.string "user_id_lms"
    t.bigint "student_data_id"
    t.bigint "course_id"
    t.datetime "date_ts"
    t.string "id_course_lms"
    t.decimal "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_session_data_on_course_id"
    t.index ["student_data_id"], name: "index_session_data_on_student_data_id"
  end

  create_table "session_resources", force: :cascade do |t|
    t.integer "active_time"
    t.bigint "session_id"
    t.bigint "resource_id"
    t.text "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "start"
    t.datetime "finish"
    t.index ["resource_id"], name: "index_session_resources_on_resource_id"
    t.index ["session_id"], name: "index_session_resources_on_session_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "start"
    t.integer "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_sessions_on_user_id"
  end

  create_table "student_data", force: :cascade do |t|
    t.string "id_user_lms"
    t.string "country"
    t.string "id_course_lms"
    t.datetime "date_completed"
    t.datetime "date_enrolled"
    t.string "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id_user_lms"], name: "index_student_data_on_id_user_lms"
    t.index ["state"], name: "index_student_data_on_state"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "name", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "gender", default: "Male"
    t.string "level_education", limit: 100
    t.string "country", limit: 100
    t.boolean "agreement_accepted", default: false
    t.string "locale", limit: 2, default: "en"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "visits", force: :cascade do |t|
    t.string "visit_token"
    t.string "visitor_token"
    t.string "ip"
    t.text "user_agent"
    t.text "referrer"
    t.text "landing_page"
    t.integer "user_id"
    t.string "referring_domain"
    t.string "search_keyword"
    t.string "browser"
    t.string "os"
    t.string "device_type"
    t.integer "screen_height"
    t.integer "screen_width"
    t.string "country"
    t.string "region"
    t.string "city"
    t.string "postal_code"
    t.decimal "latitude"
    t.decimal "longitude"
    t.string "utm_source"
    t.string "utm_medium"
    t.string "utm_term"
    t.string "utm_content"
    t.string "utm_campaign"
    t.datetime "started_at"
    t.index ["user_id"], name: "index_visits_on_user_id"
    t.index ["visit_token"], name: "index_visits_on_visit_token", unique: true
  end

  add_foreign_key "courses", "learning_management_systems"
  add_foreign_key "editions", "courses"
  add_foreign_key "enrollments", "editions"
  add_foreign_key "enrollments", "users"
  add_foreign_key "goal_kpi_groups", "goals"
  add_foreign_key "goals", "enrollments"
  add_foreign_key "goals", "users"
  add_foreign_key "memberships", "learning_management_systems"
  add_foreign_key "memberships", "users"
  add_foreign_key "notes", "courses"
  add_foreign_key "notes", "users"
  add_foreign_key "resource_activity_data", "courses"
  add_foreign_key "resource_statuses", "resources"
  add_foreign_key "resources", "courses"
  add_foreign_key "session_data", "courses"
  add_foreign_key "session_data", "student_data", column: "student_data_id"
  add_foreign_key "session_resources", "resources"
  add_foreign_key "session_resources", "sessions"
  add_foreign_key "sessions", "users"
end
