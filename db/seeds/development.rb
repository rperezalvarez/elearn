require 'csv'

chars = [*('a'..'z'),*('A'..'Z'),*('0'..'9')].flatten

# Users
if ENV["model"] == 'user' or (!ENV["model"] and !ENV["modify"])
  puts '--- Users ---'
  user1 = User.create(name: "Juan Smith", email: "juan@uc.cl", password: "123456")
  user2 = User.create(name: "Agustín Donoso", email: "agustin@uc.cl", password: "123456")
  user3 = User.create(name: "Pedro Araos", email: "pedro@uc.cl", password: "123456")

  u4 = User.create(name: "user4", email: "user4@uc.cl", password: "123456")
  u5 =User.create(name: "user5" , email: "user5@uc.cl", password: "123456")
end


# Learning Management Systems
if ENV["model"] == 'learning_management_system' or (!ENV["model"] and !ENV["modify"])
  puts '--- Learning Management Systems ---'
  lms_coursera = LearningManagementSystem.create(name: "Coursera", root: "https://www.coursera.org/")
end

# Memberships
if ENV["model"] == 'membership' or (!ENV["model"] and !ENV["modify"])
  puts '--- Memberships ---'
  User.all.each do |user|
    Membership.create(user: user, learning_management_system: LearningManagementSystem.find_by_name('Coursera'), lms_user_id: Array.new(20) { chars.sample }.join)
  end
end


# Courses
if ENV["model"] == 'course' or (!ENV["model"] and !ENV["modify"])
  puts '--- Courses ---'
  Dir.glob("lib/seeds/courses/*.csv").each do |file_name|
    puts '** Loading courses from: ' + file_name + ' **'
    csv_text = File.read(file_name)
    csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
    csv.each do |row|
      t = Course.new
      t.id_lms = row['id_lms']
      t.full_name = row['full_name']
      t.short_name = row['short_name']
      t.duration = row['duration']
      t.learning_management_system = LearningManagementSystem.find_by_name(row['lms'])
      t.save
      puts "#{t.id_lms}, #{t.full_name} saved"
    end
  end
end

#Editions
#Just one edition for each course
if ENV["model"] == 'edition' or (!ENV["model"] and !ENV["modify"])
  Course.all.each_with_index do |course,index|
    reference_date = DateTime.now
    start_date = reference_date - index.week
    end_date = start_date + 8.week
    Edition.create!(start:start_date,finish:end_date,course_id:course.id)
  end
end

# Resources
if ENV["model"] == 'resource' or (!ENV["model"] and !ENV["modify"])
  puts '--- Resources ---'
  # Create resource for procrastination
  Resource.create(category: 'others', resource_type: 'procrastination')

  # Create other resources
  Dir['lib/seeds/resources/*.csv'].each do |file_name|
    puts '** Loading resources from: ' + file_name + ' **'
    csv_text = File.read(file_name)
    csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
    csv.each do |row|
      t = Resource.new
      course = Course.find_by_id_lms(row['course_id_lms'])
      if(course)
        t.course = course
      else
        next
      end

      case row['type'].to_s
      when 'info', 'supplement'
        t.category = 'complements'
      when 'lecture'
        t.category = 'lectures'
      when 'exam', 'phased peer'
        t.category = 'evaluations'
      when 'discussions'
        t.category = 'discussions'
      else
        t.category = 'others'
      end
      t.id_lms = row['id_resource_lms']
      t.sequence = row['secuencia']
      t.resource_type = row['type']
      t.section = row['section']
      t.module = row['module']
      t.name = row['name']
      t.duration = row['duration']
      t.save
      puts "#{t.id_lms}, #{t.name}, #{t.name} saved"
    end
  end
end

# Enrollments
if ENV["model"] == 'enrollment' or (!ENV["model"] and !ENV["modify"])
  puts '--- Enrollments ---'
  # Enroll first 3 users, non of them finished any course

  courses = Course.all
  testing_users = User.first(3)
  total_users = User.count

  other_users = User.order(created_at: :desc).limit(total_users - testing_users.size)

  courses.each_with_index do |course,index|

    edition = course.last_edition

    testing_users.each_with_index do |user,user_index|
      enrollement_date = (edition.start + user_index.days).to_date
      Enrollment.create!(user_id:user.id,edition_id:edition.id,date_enrolled:enrollement_date,date_completed:nil)
    end

    other_users.each_with_index do |other_user,user_index|
      enrollement_date = (edition.start + user_index.days).to_date
      date_completed = enrollement_date + 60.days
      Enrollment.create!(user_id:other_user.id,edition_id:edition.id,date_enrolled:enrollement_date,date_completed:date_completed)
    end

  end
end

# # Resource Status
# if ENV["complete_resources"] == 'true' or (!ENV["model"] and !ENV["modify"])
#   puts '--- Resource Statuses ---'
#   resource_status = ResourceStatus.all
#   resource_status.each do |resource_status|
#     if resource_status.id % 3 == 0 or resource_status.id % 3 == 1
#       date1 = resource_status.user.enrolleds.from_course(resource_status.resource.course).first.date_enrolled
#       start_date = date1.to_datetime
#       end_date = Date.today.to_time.to_datetime
#       completed_date = Time.at((end_date.to_f - start_date.to_f)*rand + start_date.to_f).to_datetime
#
#       if resource_status.id % 3 == 0
#         resource_status.date_started = completed_date - 1.days - 3.hours
#         if rand < 0.3
#           resource_status.date_completed = completed_date
#         end
#       end
#
#       resource_status.save!
#     end
#   end
# end


# # Emergencia
# if ENV["model"] == 'example' or (!ENV["model"] and !ENV["modify"])
#   puts '--- Comparation data ---'
#   user1 = User.find_by_email("user4@uc.cl")
#   user1.enrolleds.each do |e|
#     e.state = true
#     e.save!
#   end
#
#   user2 = User.find_by_email("user5@uc.cl")
#   user2.enrolleds.each do |e|
#     e.state = false
#     e.save!
#   end
#
#   duration = 35
#   start = Date.today - duration
#   finish = Date.tomorrow
#
#   count = 1
#   base_graduated_student = 30
#   base_no_graduated_student = 20
#
#   Course.all.each do |course|
#
#     resource = course.resources.where(resource_type: 'lecture').first
#
#     (start..finish).each do |date|
#       puts date.to_s
#
#       time_graduated_student = base_graduated_student*(1 + rand(-5..5) * 0.1)*60 #Seconds
#       session = Session.create!(user: user1, start: date, duration: time_graduated_student)
#       sr = SessionResource.create!(session: session, resource: resource, start: date, active_time: time_graduated_student)
#
#       time_no_graduated_student = base_no_graduated_student*(1 + rand(-5..5) * 0.1)*60 #Seconds
#       session = Session.create!(user: user2, start: date, duration: time_no_graduated_student)
#       sr = SessionResource.create!(session: session, resource: resource, start: date, active_time: time_no_graduated_student)
#
#       count += 1
#     end
#
#   end
#
#   puts "Final Count :" + count.to_s
# end




if ENV["model"] == 'juan_resources' or (!ENV["model"] and !ENV["modify"])

  base_low_effectiveness = 4
  base_high_effectiveness = 7
  base_procrastination = 17
  procrastination_resource = Resource.find_by_resource_type('procrastination')
  course = Course.third

  puts '--- Juan and no completers sessions ---'

  users = [User.first, User.find_by_email("user5@uc.cl")]

  users.each do |user|
    # Creates sessions on last edition of courses
    user_enrollment = user.enrollments.joins(:edition).where("editions.course_id = ?", course).last

    effective_resources = Resource.where.not(resource_type: 'procrastination')
                                  .where(course: course)
                                  .joins(:resource_statuses)
                                  .where("resource_statuses.enrollment_id = ?", user_enrollment.id)


    start = user_enrollment.date_enrolled
    finish = Date.today

    (start..finish).each do |date|
      if rand > 0.35
        # Effective resources
        effective_resources.each do |effective_resource|
          r = rand
          if r < 0.1
            puts date.to_s
            # Procrastination
            time_procrastination = base_procrastination*(1 + rand(-7..7) * 0.1)*60 #Seconds
            pr = SessionResource.new(resource: procrastination_resource, start: date.to_datetime, active_time: time_procrastination)

            time_low_effectiveness = base_low_effectiveness*(1 + rand(-5..5) * 0.1)*60 #Seconds
            time_high_effectiveness = base_high_effectiveness*(1 + rand(-5..5) * 0.1)*60 #Seconds
            time_session = (time_low_effectiveness + time_high_effectiveness)/2 # Seconds
            # puts 'Low: ' + time_low_effectiveness.to_s
            # puts 'High: ' + time_high_effectiveness.to_s
            # puts 'Session:' + time_session.to_s
            sr = SessionResource.new(resource: effective_resource, start: date.to_datetime, active_time: time_session)

            resource_status = ResourceStatus.from_user(user).from_edition(user_enrollment.edition).from_resource(effective_resource).first

            if resource_status.status != ResourceStatus::STATUS[:completed]

              if resource_status.status == ResourceStatus::STATUS[:not_started]
                resource_status.date_started = date.to_datetime + (rand*23).hours
              end

              if resource_status.status == ResourceStatus::STATUS[:started] and rand < 0.5
                resource_status.date_completed = resource_status.date_started + (rand*23).hours
              end
              resource_status.save!

              session = Session.create!(user: user, start: date.to_datetime, duration: (time_procrastination+time_session)*rand(1..5)*0.1)
              pr.session = session
              pr.save!
              sr.session = session
              sr.save!

            end
          end
        end

      end
    end

  end


  puts '--- Completers sessions ---'

  user = User.find_by_email("user4@uc.cl")
  user_enrollment = user.enrollments.joins(:edition).where("editions.course_id = ?", course).last

  effective_resources = Resource.where.not(resource_type: 'procrastination')
                                .where(course: course)
                                .joins(:resource_statuses)
                                .where("resource_statuses.enrollment_id = ?", user_enrollment)


  start = user_enrollment.date_enrolled
  finish = Date.today

  (start..finish).each do |date|
    # Effective resources
    effective_resources.each do |effective_resource|
      r = rand
      if r < 0.05
        puts date.to_s
        # Procrastination
        time_procrastination = base_procrastination*(1 + rand(-7..7) * 0.1)*60 #Seconds
        pr = SessionResource.new(resource: procrastination_resource, start: date.to_datetime, active_time: time_procrastination)

        time_low_effectiveness = base_low_effectiveness*(1 + rand(-5..5) * 0.1)*60 #Seconds
        time_high_effectiveness = base_high_effectiveness*(1 + rand(-5..5) * 0.1)*60 #Seconds
        time_session = (time_low_effectiveness + time_high_effectiveness)/2 # Seconds
        # puts 'Low: ' + time_low_effectiveness.to_s
        # puts 'High: ' + time_high_effectiveness.to_s
        # puts 'Session:' + time_session.to_s
        sr = SessionResource.new(resource: effective_resource, start: date.to_datetime, active_time: time_session)

        resource_status = ResourceStatus.from_user(user).from_edition(user_enrollment.edition).from_resource(effective_resource).first

        if resource_status.status != ResourceStatus::STATUS[:completed]
          resource_status.date_started = date.to_datetime + (rand*23).hours
          resource_status.date_completed = resource_status.date_started + rand.hours
          resource_status.save!

          session = Session.create!(user: user, start: date.to_datetime, duration: (time_procrastination+time_session)*rand(1..5)*0.1)
          pr.session = session
          pr.save!
          sr.session = session
          sr.save!
        end

      end
    end
  end

end


# # Modify resources
# if ENV["modify"] == 'resources'
#   puts '--- Resource modification ---'
#   # Create resource for procrastination
#   Resource.create(category: 'others', resource_type: 'procrastination')
#
#   # Create other resources
#   Dir['lib/seeds/resources/*.csv'].each do |file_name|
#     puts '** Loading resources from: ' + file_name + ' **'
#     csv_text = File.read(file_name)
#     csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
#     csv.each do |row|
#       t = Resource.find_by_id_lms(row['id_resource_lms'])
#
#       if t
#         case row['type']
#         when 'info', 'supplement'
#           t.category = 'complements'
#         when 'lecture'
#           t.category = 'lectures'
#         when 'exam', 'phased peer'
#           t.category = 'evaluations'
#         when 'discussions'
#           t.category = 'discussions'
#         else
#           t.category = 'others'
#         end
#
#         t.save
#
#         puts "#{t.id_lms}, #{t.name}, #{t.category} saved"
#       end
#     end
#   end
# end
