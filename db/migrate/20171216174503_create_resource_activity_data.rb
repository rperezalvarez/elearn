class CreateResourceActivityData < ActiveRecord::Migration[5.1]
  def change
    create_table :resource_activity_data do |t|

      t.string :item_id_lms
      t.string :user_id_lms
      t.string :state
      t.datetime :course_progress_ts
      t.string :course_item_type_desc
      t.string :session_id_lms
      t.string :course_id_lms
      t.references :course, foreign_key: true
      t.decimal :tine_spent

      t.timestamps
    end
  end
end
