class ActivitySummary < ActiveRecord::Migration[5.1]
   def up
      self.connection.execute %Q( CREATE OR REPLACE VIEW activity_summary AS
            SELECT DISTINCT resource_activity_data.course_id_lms, 
            student_data.id_user_lms, 
            student_data.state AS student_course_state, 
            resource_activity_data.course_item_type_desc, 
            resource_activity_data.state, 
            FLOOR(extract(epoch from (resource_activity_data.course_progress_ts - student_data.date_enrolled))/604800) AS week
            , sum(resource_activity_data.tine_spent) AS total_time_spent
            FROM resource_activity_data
            JOIN student_data
            ON resource_activity_data.user_id_lms = student_data.id_user_lms
            GROUP BY resource_activity_data.course_id_lms, student_data.id_user_lms, student_data.state, resource_activity_data.course_item_type_desc, resource_activity_data.state, FLOOR(extract(epoch from (resource_activity_data.course_progress_ts - student_data.date_enrolled))/604800)
            HAVING 0 <= FLOOR(extract(epoch from (resource_activity_data.course_progress_ts - student_data.date_enrolled))/604800) AND FLOOR(extract(epoch from (resource_activity_data.course_progress_ts - student_data.date_enrolled))/604800) <= 20;
          )
    end

    def down
      self.connection.execute "DROP VIEW IF EXISTS activity_summary;"
    end
end
