class AddPickedColumnToGoalDetails < ActiveRecord::Migration[5.1]
  def change
    add_column :goal_details, :picked, :boolean,default: false
  end
end
