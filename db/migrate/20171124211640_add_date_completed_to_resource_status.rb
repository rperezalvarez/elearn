class AddDateCompletedToResourceStatus < ActiveRecord::Migration[5.1]
  def change
    add_column :resource_statuses, :date_completed, :datetime
  end
end
