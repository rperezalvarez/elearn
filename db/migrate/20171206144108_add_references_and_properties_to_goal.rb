class AddReferencesAndPropertiesToGoal < ActiveRecord::Migration[5.1]
  def change
    add_reference :goals, :edition, foreign_key: true,  index: true
    remove_column :goals, :current_hours
    add_column :goals, :recorded_hours, :integer
    add_column :goals, :goal_videos, :integer
    add_column :goals, :recorded_videos, :integer
    add_column :goals, :goal_evaluations, :integer
    add_column :goals, :recorded_evaluations, :integer
    add_column :goals, :week, :integer
  end
end
