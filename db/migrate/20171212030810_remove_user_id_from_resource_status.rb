class RemoveUserIdFromResourceStatus < ActiveRecord::Migration[5.1]
  def change
    remove_column :resource_statuses, :user_id
    add_reference :resource_statuses, :enrollment, index: true
  end
end
