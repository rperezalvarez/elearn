class CreateJoinTableEditionsUsers < ActiveRecord::Migration[5.1]
  def change
    create_join_table :editions, :users do |t|
      t.index [:edition_id, :user_id]
      t.index [:user_id, :edition_id]
    end
  end
end
