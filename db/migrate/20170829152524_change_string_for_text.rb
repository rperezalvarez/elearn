class ChangeStringForText < ActiveRecord::Migration[5.1]
  def change
    change_column :courses, :full_name, :text, limit: 2000
    change_column :courses, :short_name, :text, limit: 2000
    change_column :resources, :name, :text, limit: 1000
    change_column :session_resources, :url, :text, limit: 2000  
  end
end
