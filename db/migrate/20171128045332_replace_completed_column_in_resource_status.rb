class ReplaceCompletedColumnInResourceStatus < ActiveRecord::Migration[5.1]
  def up
    remove_column :resource_statuses, :completed
    add_column :resource_statuses, :status, :string, :default => ResourceStatus::STATUS[:not_started]
    add_column :resource_statuses, :date_started, :datetime
  end

  def down
    remove_column :resource_statuses, :date_started
    remove_column :resource_statuses, :status
    add_column :resource_statuses, :completed, :boolean
  end
end
