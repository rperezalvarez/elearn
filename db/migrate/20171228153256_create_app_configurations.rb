class CreateAppConfigurations < ActiveRecord::Migration[5.1]
  def change
    create_table :app_configurations do |t|
      # all times are in seconds
      t.integer :max_inactive_time, null: false, default: 45*60
      t.integer :min_session_effective_time_for_save, null: false, default: 5*60

      t.timestamps
    end
  end
end
