class RemoveJoinTableUserEdition < ActiveRecord::Migration[5.1]
  def change
    drop_table :editions_users
  end
end
