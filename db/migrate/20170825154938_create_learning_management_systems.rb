class CreateLearningManagementSystems < ActiveRecord::Migration[5.1]
  def change
    create_table :learning_management_systems do |t|
      t.string :name
      t.string :root, unique: true

      t.timestamps
    end
  end
end
