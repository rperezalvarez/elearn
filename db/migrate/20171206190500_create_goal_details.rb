class CreateGoalDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :goal_details do |t|
      t.string :kind
      t.datetime :schedule_date
      t.references :goal

      t.timestamps
    end
  end
end
