class AddIndexToImproveReportPerformance < ActiveRecord::Migration[5.1]
  def change
    add_index(:resource_activity_data, :user_id_lms)
    add_index(:resource_activity_data, :course_id_lms)
    add_index(:resource_activity_data, :course_item_type_desc)
    add_index(:resource_activity_data, :state)

    add_index(:student_data, :id_user_lms)
    add_index(:student_data, :state)

  end
end
