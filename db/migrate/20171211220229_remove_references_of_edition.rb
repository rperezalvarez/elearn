class RemoveReferencesOfEdition < ActiveRecord::Migration[5.1]
  def change
    remove_reference :goals, :edition, foreign_key: true,  index: true
    remove_column :editions, :enrolled_finish
  end
end
