class CreateGoals < ActiveRecord::Migration[5.1]
  def change
    create_table :goals do |t|
      t.references  :user, foreign_key: true
      t.timestamp   :start
      t.timestamp   :finish
      t.integer     :goal_hours
      t.integer     :current_hours

      t.timestamps
    end
  end
end
