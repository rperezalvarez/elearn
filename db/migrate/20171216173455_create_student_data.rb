class CreateStudentData < ActiveRecord::Migration[5.1]
  def change
    create_table :student_data do |t|

      t.string :id_user_lms
      t.string :country
      t.string :id_course_lms
      t.datetime :date_completed
      t.datetime :date_enrolled
      t.string :state

      t.timestamps
    end
  end
end
