class CreateEnrollments < ActiveRecord::Migration[5.1]
  def change
    create_table :enrollments do |t|
      t.references :user, foreign_key: true
      t.references :edition, foreign_key: true
      t.date :date_enrolled
      t.date :date_completed
      t.boolean :finished

      t.timestamps
    end
  end
end
