class CreateResources < ActiveRecord::Migration[5.1]
  def change
    create_table :resources do |t|
      t.string      :id_lms, limit: 50
      t.references  :course, foreign_key: true
      t.string      :course_id_lms, limit: 50
      t.string      :name, limit: 1000
      t.integer     :sequence
      t.integer     :section
      t.integer     :module
      t.string      :type, limit: 50
      t.string      :category, limit: 30
      t.integer     :duration

      t.timestamps
    end
  end
end
