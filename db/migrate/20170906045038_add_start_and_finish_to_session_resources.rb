class AddStartAndFinishToSessionResources < ActiveRecord::Migration[5.1]
  def change
    add_column :session_resources, :start, :datetime
    add_column :session_resources, :finish, :datetime
    rename_column :session_resources, :time_spent, :active_time
  end
end
