class AddColsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :gender, :string, default: 'Male'
    add_column :users, :level_education, :string, limit: 100
    add_column :users, :country, :string, limit: 100
  end
end
