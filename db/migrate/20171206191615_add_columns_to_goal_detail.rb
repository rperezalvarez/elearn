class AddColumnsToGoalDetail < ActiveRecord::Migration[5.1]
  def change
    add_column :goal_details, :execution_date, :datetime
  end
end
