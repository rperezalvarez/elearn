class CreateGoalKpiGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :goal_kpi_groups do |t|
      t.references :goal, foreign_key: true

      t.integer :videos_last_week, null: false, default: 0
      t.integer :evals_last_week, null: false, default: 0

      t.integer :most_effective_day, null: false, default: 0
      t.decimal :avg_weekly_videos, null: false, default: 0.0
      t.decimal :avg_weekly_exams, null: false, default: 0.0
      t.decimal :avg_weekly_time, null: false, default: 0.0
      t.decimal :avg_weekly_time_videos, null: false, default: 0.0
      t.decimal :avg_weekly_time_exams, null: false, default: 0.0
      
      t.integer :total_videos_this_week, null: false, default: 0
      t.integer :total_evals_this_week, null: false, default: 0
      t.integer :total_time_videos_week, null: false, default: 0
      t.integer :total_time_evals_week, null: false, default: 0

      t.decimal :avg_videos_completers, null: false, default: 0.0
      t.decimal :avg_evals_completers, null: false, default: 0.0
      t.decimal :avg_time_completers, null: false, default: 0.0

      t.timestamps
    end
  end
end
