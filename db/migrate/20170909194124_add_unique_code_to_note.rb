class AddUniqueCodeToNote < ActiveRecord::Migration[5.1]
  def change
    add_column :notes, :code, :text
  end
end
