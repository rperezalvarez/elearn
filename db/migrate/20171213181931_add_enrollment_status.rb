class AddEnrollmentStatus < ActiveRecord::Migration[5.1]
  def change
    remove_column :enrollments,:finished
    add_column :enrollments, :status, :string
  end
end
