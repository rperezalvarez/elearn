class CreateSessionData < ActiveRecord::Migration[5.1]
  def change
    create_table :session_data do |t|

      t.string :id_session_lms
      t.integer :number_session
      t.string :user_id_lms
      t.references :student_data, foreign_key: true
      t.references :course, foreign_key: true
      t.datetime :date_ts
      t.string :id_course_lms
      t.decimal :duration


      t.timestamps
    end
  end
end
