class AddCourseToNote < ActiveRecord::Migration[5.1]
  def change
    add_reference :notes, :course, foreign_key: true
  end
end
