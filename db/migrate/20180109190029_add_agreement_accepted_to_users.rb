class AddAgreementAcceptedToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :agreement_accepted, :boolean, default: false
  end
end
