class RemoveCourseFromNote < ActiveRecord::Migration[5.1]
  def change
    remove_column :notes, :course_id
  end
end
