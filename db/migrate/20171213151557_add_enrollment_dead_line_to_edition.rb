class AddEnrollmentDeadLineToEdition < ActiveRecord::Migration[5.1]
  def change
    add_column :editions, :dead_line_enrollment_date, :datetime
  end
end
