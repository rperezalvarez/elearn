class CreateSessions < ActiveRecord::Migration[5.1]
  def change
    create_table :sessions do |t|
      t.references  :user, foreign_key: true
      t.datetime    :start
      t.integer     :duration

      t.timestamps
    end
  end
end
