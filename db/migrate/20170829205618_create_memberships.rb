class CreateMemberships < ActiveRecord::Migration[5.1]
  def change
    create_table :memberships do |t|
      t.references :user, foreign_key: true
      t.references :learning_management_system, foreign_key: true
      t.string :lms_user_id

      t.timestamps
    end
  end
end
