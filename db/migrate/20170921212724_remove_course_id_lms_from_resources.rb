class RemoveCourseIdLmsFromResources < ActiveRecord::Migration[5.1]
  def change
    remove_column :resources, :course_id_lms, :string
  end
end
