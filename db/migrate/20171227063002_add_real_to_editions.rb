class AddRealToEditions < ActiveRecord::Migration[5.1]
  def change
    add_column :editions, :real, :boolean, default: true
  end
end
