class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string      :id_lms, limit: 50
      t.references  :learning_management_system, foreign_key: true
      t.string      :full_name, limit: 2000
      t.string      :short_name, limit: 2000
      t.integer     :duration
      t.string      :topic, limit: 50

      t.timestamps
    end
  end
end
