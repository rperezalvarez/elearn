class CreateEditions < ActiveRecord::Migration[5.1]
  def change
    create_table :editions do |t|
      t.timestamp   :start
      t.timestamp   :finish
      t.timestamp   :enrolled_finish
      t.string      :id_edition_lms, limit: 50
      t.references  :course, foreign_key: true

      t.timestamps
    end
  end
end
