class CreateSessionResources < ActiveRecord::Migration[5.1]
  def change
    create_table :session_resources do |t|
      t.integer     :time_spent
      t.references  :session, foreign_key: true
      t.references  :resource, foreign_key: true
      t.string      :url, limit: 2000

      t.timestamps
    end
  end
end
